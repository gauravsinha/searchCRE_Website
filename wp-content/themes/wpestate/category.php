<?php
// Category
// Wp Estate Pack
get_header();
$options=sidebar_orientation();
 if ( 'wpestate_message' == get_post_type() || 'wpestate_invoice' == get_post_type() || 'wpestate_booking' == get_post_type() ){
     exit();
 }
?>

<!-- Google Map Code -->
<?php 
get_template_part('libs/templates/map-template'); 
?> 
<!-- Google Map Code -->



<div id="wrapper" class="<?php print $options['fullwhite']; ?>">  
    <div class="<?php print $options['add_back']; ?>"></div>

    <?php
    print breadcrumb_container($options['full_breadcrumbs'],$options['bread_align'] )
    ?>
    <div id="main" class="row <?php print $options['sidebar_status']; ?>">
    <?php
    print display_breadcrumbs( $options['full_breadcrumbs'] ,$options['bread_align_internal'] )
    ?>



        <!-- begin content--> 
        <div id="post" class="blogborder <?php print $options['grid'].' '.$options['shadow']; ?>"> 
            <div class="inside_post inside_no_border inside_no_bottom">


                <h1 class="entry-title">
                <?php printf(__('Category Archives: %s', 'wpestate'), '<span>' . single_cat_title('', false) . '</span>'); ?>
                </h1>

                    <?php   
                    while (have_posts()) : the_post();
                        include(locate_template('bloglisting.php')); 
                    endwhile;
                    
                    wp_reset_query();
                    ?>
                </div> <!-- end inside post-->
                <?php kriesi_pagination('', $range = 2); ?>       
                <div class="spacer_archive"></div>


        </div>
        <!-- end content-->



       <?php  include(locate_template('customsidebar.php')); ?>


    </div><!-- #main -->    
</div><!-- #wrapper -->
<?php get_footer();?>