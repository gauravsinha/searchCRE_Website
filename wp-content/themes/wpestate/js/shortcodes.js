///  shortcode - recent_posts_pictures
////////////////////////////////////////////////////////////////////////////////////////////////////////////////..
/*(function() {
   tinymce.create('tinymce.plugins.spacerx', {
      init : function(ed, url) {
	        ed.addButton('spacerx', {
            title : 'Spacer', 
            image : url+'/tiny_icons/spacer.png',

         	 onclick : function() {  
                    ed.selection.setContent('[spacerx height="40"][/spacerx]');  
                } 
         });
      },
    createControl : function(n, cm) {  
        return null;  
        },  

    });  
    tinymce.PluginManager.add('advanced_search', tinymce.plugins.advanced_search);
})();
*/


(function() {
   tinymce.create('tinymce.plugins.space_area', {
      init : function(ed, url) {
	        ed.addButton('space_area', {
            title : 'Spacer', 
            image : url+'/tiny_icons/spacer.png',

         	 onclick : function() {  
                    ed.selection.setContent('[space_area height="40"][/space_area]');  
                } 
         });
      },
    createControl : function(n, cm) {  
        return null;  
        },  

    });  
    tinymce.PluginManager.add('space_area', tinymce.plugins.space_area);
})();



(function() {
   tinymce.create('tinymce.plugins.advanced_search', {
      init : function(ed, url) {
	        ed.addButton('advanced_search', {
            title : 'Advanced Search', 
            image : url+'/tiny_icons/advanced_search.png',

         	 onclick : function() {  
                    ed.selection.setContent('[advanced_search][/advanced_search]');  
                } 
         });
      },
    createControl : function(n, cm) {  
        return null;  
        },  

    });  
    tinymce.PluginManager.add('advanced_search', tinymce.plugins.advanced_search);
})();


//
/////  shortcode - recent_posts_pictures
////////////////////////////////////////////////////////////////////////////////////////////////////////////////..



(function() {
   tinymce.create('tinymce.plugins.list_items_by_id', {
      init : function(ed, url) {
	        ed.addButton('list_items_by_id', {
            title : 'List Items by Id', 
            image : url+'/tiny_icons/recent-items-2.png',

         	 onclick : function() {  
                    ed.selection.setContent('[list_items_by_id type="properties or articles" ids="" number="how many items/row" rows="1" link="link to global listing"][/list_items_by_id]');  
                } 
         });
      },
    createControl : function(n, cm) {  
        return null;  
        },  

    });  
    tinymce.PluginManager.add('list_items_by_id', tinymce.plugins.list_items_by_id);
})();



    
/////  shortcode - login form 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////..

(function() {  
    tinymce.create('tinymce.plugins.login_form', {  
        init : function(ed, url) {  
            ed.addButton('login_form', {  
                title : 'Add a login_form',  
                image : url+'/tiny_icons/user-login.png',
                onclick : function() {  
                     ed.selection.setContent('[login_form register_label="register here" register_url="..." ]' + ed.selection.getContent() + '[/login_form]');  
  
                }  
            });  
        },  
        createControl : function(n, cm) {  
            return null;  
        },  
    });  
    tinymce.PluginManager.add('login_form', tinymce.plugins.login_form);  
})();  




///  shortcode - register_form 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////..

(function() {
   tinymce.create('tinymce.plugins.register_form', {
      init : function(ed, url) {
	 ed.addButton('register_form', {
            title : 'Insert Register Form', 
            image : url+'/tiny_icons/register_form.png',

         	 onclick : function() {
                     ed.selection.setContent('[register_form][/register_form]');
                } 
         }); 
      },
    createControl : function(n, cm) {  
        return null;  
        },  

    });  
    tinymce.PluginManager.add('register_form', tinymce.plugins.register_form);
})();




///  shortcode - featured_property 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////..

(function() {
   tinymce.create('tinymce.plugins.featured_property', {
      init : function(ed, url) {
	        ed.addButton('featured_property', {
            title : 'Insert Featured Property', 
            image : url+'/tiny_icons/featured-property.png',

         	 onclick : function() {
                     ed.selection.setContent('[featured_property id="property id" sale_line="sale line goes here"][/featured_property]');
                } 
         }); 
      },
    createControl : function(n, cm) {  
        return null;  
        },  

    });  
    tinymce.PluginManager.add('featured_property', tinymce.plugins.featured_property);
})();


///  shortcode - featured article
////////////////////////////////////////////////////////////////////////////////////////////////////////////////..

(function() {
   tinymce.create('tinymce.plugins.featured_article', {
      init : function(ed, url) {
	        ed.addButton('featured_article', {
            title : 'Insert Featured Article', 
            image : url+'/tiny_icons/featured-article.png',

         	 onclick : function() {
                     ed.selection.setContent('[featured_article id="article id" second_line="featured article" type="1 or 2"][/featured_article]');
                } 
         }); 
      },
    createControl : function(n, cm) {  
        return null;  
        },  

    });  
    tinymce.PluginManager.add('featured_article', tinymce.plugins.featured_article);
})();





///  shortcode - featured agent
////////////////////////////////////////////////////////////////////////////////////////////////////////////////..

(function() {
   tinymce.create('tinymce.plugins.featured_agent', {
      init : function(ed, url) {
	        ed.addButton('featured_agent', {
            title : 'Insert Featured Agent', 
            image : url+'/tiny_icons/featured-agent.png',

         	 onclick : function() {
                     ed.selection.setContent('[featured_agent id="agent id"][/featured_agent]');
                } 
         }); 
      },
    createControl : function(n, cm) {  
        return null;  
        },  

    });  
    tinymce.PluginManager.add('featured_agent', tinymce.plugins.featured_agent);
})();




///  shortcode - google map
////////////////////////////////////////////////////////////////////////////////////////////////////////////////..
/*
(function() {
   tinymce.create('tinymce.plugins.google_map', {
      init : function(ed, url) {
	        ed.addButton('google_map', {
            title : 'Insert A Google Map', 
            image : url+'/tiny_icons/gmap.png',

         	 onclick : function() {  
                     ed.selection.setContent("[google_map iframe=''][/google_map]");
                } 
         }); 
      },
    createControl : function(n, cm) {  
        return null;  
        },  

    });  
    tinymce.PluginManager.add('google_map', tinymce.plugins.google_map);
})();

*/






///  shortcode - regular table
////////////////////////////////////////////////////////////////////////////////////////////////////////////////..

 
(function() {
   tinymce.create('tinymce.plugins.regular_table', {
      init : function(ed, url) {
	        ed.addButton('regular_table', {
            title : 'Insert A Table', 
            image : url+'/tiny_icons/table.png',

         	 onclick : function() {  
 					  ed.selection.setContent('[table head_color="#ffffff" head_background="#D74142"]<table class="regular_table"><thead><tr><th scope="col">Column 1</th><th scope="col">Column 2</th><th scope="col">Column 3</th><th scope="col">Column 4</th></tr></thead><tbody><tr><td>Content</td><td>Content</td><td>Content</td><td>Content</td></tr><tr><td>Content col 2</td><td>Content col 2</td><td>Content col 2</td><td>Content col 2</td></tr><tr><td>Content col </td><td>Content col 3</td><td>Content col 3</td><td>Content col 3</td></tr><tr><td>Content col 4</td><td>Content col 4</td><td>Content col 4</td><td>Content col 4</td></tr></tbody><tfoot><tr><td colspan="4">Table footer</td></tr></tfoot></table>[/table]')  

                } 
         });
      },
    createControl : function(n, cm) {  
        return null;  
        },  

    });  
    tinymce.PluginManager.add('regular_table', tinymce.plugins.regular_table);
})();









///  shortcode - breadcrumbs
////////////////////////////////////////////////////////////////////////////////////////////////////////////////..


(function() {
   tinymce.create('tinymce.plugins.breadcrumbs', {
      init : function(ed, url) {
	        ed.addButton('breadcrumbs', {
            title : 'Insert Breadcrumbs', 
            image : url+'/tiny_icons/breadcrumbs.png',

         	 onclick : function() {  
 					  ed.selection.setContent('[breadcrumbs][/breadcrumbs]')  
                } 
         });
      },
    createControl : function(n, cm) {  
        return null;  
        },  

    });  
    tinymce.PluginManager.add('breadcrumbs', tinymce.plugins.breadcrumbs);
})();




///  shortcode - accordion
////////////////////////////////////////////////////////////////////////////////////////////////////////////////..


(function() {
   tinymce.create('tinymce.plugins.accordion', {
      init : function(ed, url) {
	        ed.addButton('accordion', {
            title : 'Insert Accordion ', 
            image : url+'/tiny_icons/accordeon.png',

         	 onclick : function() {  
 					  ed.selection.setContent('[accordion][toggle color="white,green,blue,purple or black" open="yes" title="Title"]Content[/toggle][toggle color="white,green,blue,purple or black" open="no" title="Title"]Content[/toggle][toggle color="white,green,blue,purple or black" open="no" title="Title"]Content[/toggle][/accordion]')  
                } 
         });
      },
    createControl : function(n, cm) {  
        return null;  
        },  

    });  
    tinymce.PluginManager.add('accordion', tinymce.plugins.accordion);
})();




///  shortcode - toggle
////////////////////////////////////////////////////////////////////////////////////////////////////////////////..


(function() {
   tinymce.create('tinymce.plugins.toggle', {
      init : function(ed, url) {
	        ed.addButton('toggle', {
            title : 'Insert Toggle', 
            image : url+'/tiny_icons/toogle.png',

         	 onclick : function() {  
                        ed.selection.setContent('[toggle color="white,green,blue,purple or black" open="yes/no" title="Toggle  Title"]Toggle Content[/toggle]')  
                } 
         });
      },
    createControl : function(n, cm) {  
        return null;  
        },  

    });  
    tinymce.PluginManager.add('toggle', tinymce.plugins.toggle);
})();



/// slider
////////////////////////////////////////////////////////////////////////////////////////////////////////////////.. 

(function() {
   tinymce.create('tinymce.plugins.slider', {
      init : function(ed, url) {
	        ed.addButton('slider', {
            title : 'Add a slider', 
            image : url+'/tiny_icons/slider.png',

         	 onclick : function() {  
					ed.selection.setContent('[slider width="width in px or percent, for ex. 300px or 50%"][slide link="" type="image or video"]picture url or video shortcode[/slide][slide link="" type="image or video"]picture url or video shortcode[/slide][/slider]');                   } 
         });
      },
    createControl : function(n, cm) {  
        return null;  
        },  

    });  
    tinymce.PluginManager.add('slider', tinymce.plugins.slider);
})();


/// slider 2 -revolutin slider
////////////////////////////////////////////////////////////////////////////////////////////////////////////////.. 

(function() {
   tinymce.create('tinymce.plugins.slider_2', {
      init : function(ed, url) {
	        ed.addButton('slider_2', {
            title : 'Add a slider', 
            image : url+'/tiny_icons/slider_2.png',

         	 onclick : function() {  
					ed.selection.setContent('[slider_2][slide_2 type="image or video"]picture url or video shortcode[/slide_2][slide_2 type="image or video"]picture url or video shortcode[/slide_2][/slider_2]');                   } 
         });
      },
    createControl : function(n, cm) {  
        return null;  
        },  

    });  
    tinymce.PluginManager.add('slider_2', tinymce.plugins.slider_2);
})();



///  three_fourth_column
////////////////////////////////////////////////////////////////////////////////////////////////////////////////..



(function() {
   tinymce.create('tinymce.plugins.three_fourth_column', {
      init : function(ed, url) {
	        ed.addButton('three_fourth_column', {
            title : 'Add a three_fourth column', 
            image : url+'/tiny_icons/three-fourth.png',

         	 onclick : function() {  
		    ed.selection.setContent('[content_three_fourth position="first or last" title="Title here" border="none/agentborder/blogborder/listingborder"]...[/content_three_fourth]');                   } 
         });
      },
    createControl : function(n, cm) {  
        return null;  
        },  

    });  
    tinymce.PluginManager.add('three_fourth_column', tinymce.plugins.three_fourth_column);
})();




///  one_fourth_column
////////////////////////////////////////////////////////////////////////////////////////////////////////////////..



(function() {
   tinymce.create('tinymce.plugins.one_fourth_column', {
      init : function(ed, url) {
	        ed.addButton('one_fourth_column', {
            title : 'Add a one fourth column', 
            image : url+'/tiny_icons/one-fourth.png',

         	 onclick : function() {  
                     ed.selection.setContent('[content_one_fourth position="first or last" title="Title here" border="none/agentborder/blogborder/listingborder"]...[/content_one_fourth]');  
                } 
         });
      },
    createControl : function(n, cm) {  
        return null;  
        },  

    });  
    tinymce.PluginManager.add('one_fourth_column', tinymce.plugins.one_fourth_column);
})();



///  two_third_column
////////////////////////////////////////////////////////////////////////////////////////////////////////////////..



(function() {
   tinymce.create('tinymce.plugins.two_third_column', {
      init : function(ed, url) {
	        ed.addButton('two_third_column', {
            title : 'Add a two third column', 
            image : url+'/tiny_icons/two-third.png',

         	 onclick : function() {  
 					        ed.selection.setContent('[container_two_third position="first or last" title="Title here" border="none/agentborder/blogborder/listingborder"]...[/container_two_third]');  
                } 
         });
      },
    createControl : function(n, cm) {  
        return null;  
        },  

    });  
    tinymce.PluginManager.add('two_third_column', tinymce.plugins.two_third_column);
})();



///  one_third
////////////////////////////////////////////////////////////////////////////////////////////////////////////////..



(function() {
   tinymce.create('tinymce.plugins.one_third_column', {
      init : function(ed, url) {
	        ed.addButton('one_third_column', {
            title : 'Add a one third column', 
            image : url+'/tiny_icons/one-third.png',

         	 onclick : function() {  
 					      ed.selection.setContent('[container_one_third position="first or last" title="Title here" border="none/agentborder/blogborder/listingborder"]...[/container_one_third]');  
                } 
         });
      },
    createControl : function(n, cm) {  
        return null;  
        },  

    });  
    tinymce.PluginManager.add('one_third_column', tinymce.plugins.one_third_column);
})();



///  one_half
////////////////////////////////////////////////////////////////////////////////////////////////////////////////..



(function() {
   tinymce.create('tinymce.plugins.one_half_column', {
      init : function(ed, url) {
	        ed.addButton('one_half_column', {
            title : 'Add a one half column', 
            image : url+'/tiny_icons/one-half.png',

         	 onclick : function() {  
 					      ed.selection.setContent('[container_half position="first or last" title="Title here" border="none/agentborder/blogborder/listingborder"]...[/container_half]');  
                } 
         });
      },
    createControl : function(n, cm) {  
        return null;  
        },  

    });  
    tinymce.PluginManager.add('one_half_column', tinymce.plugins.one_half_column);
})();


















///  shortcode - recent_posts_pictures
////////////////////////////////////////////////////////////////////////////////////////////////////////////////..



(function() {
   tinymce.create('tinymce.plugins.recent_items', {
      init : function(ed, url) {
	        ed.addButton('recent_items', {
            title : 'Recent Items', 
            image : url+'/tiny_icons/recent_posts.png',

         	 onclick : function() {  
                    ed.selection.setContent('[recent_items type="properties or articles" category_ids="" action_ids="" city_ids="" area_ids="" number="how many items/row" rows="1" link="link to global listing"][/recent_items]');  
                } 
         });
      },
    createControl : function(n, cm) {  
        return null;  
        },  

    });  
    tinymce.PluginManager.add('recent_items', tinymce.plugins.recent_items);
})();






///  shortcode - content containers - main containers 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////..


(function() {
   tinymce.create('tinymce.plugins.container_content_main', {
      init : function(ed, url) {
	        ed.addButton('container_content_main', {
            title : 'Insert a Full Width Container', 
            image : url+'/tiny_icons/content_container.png',

         	 onclick : function() {  
 						 ed.selection.setContent('[container_content title="Title Here" border="none/agentborder/blogborder/listingborder"]...[/container_content]');  
                } 
         });
      },
    createControl : function(n, cm) {  
        return null;  
        },  

    });  
    tinymce.PluginManager.add('container_content_main', tinymce.plugins.container_content_main);
})();



///  shortcode - content containers - regular containers 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////..


(function() {
   tinymce.create('tinymce.plugins.container_content_regular', {
      init : function(ed, url) {
	        ed.addButton('container_content_regular', {
            title : 'Insert a Icon Content Box', 
            image : url+'/tiny_icons/regular_container.png',

         	 onclick : function() {  
 						 ed.selection.setContent('[container_box title="Box Title" image="image url" link="link in header" bottom_border="no/yes"]Box Content Here[/container_box]');  
                } 
         });
      },
    createControl : function(n, cm) {  
        return null;  
        },  

    });  
    tinymce.PluginManager.add('container_content_regular', tinymce.plugins.container_content_regular);
})();





///  shortcode - tagline container
////////////////////////////////////////////////////////////////////////////////////////////////////////////////..


(function() {
   tinymce.create('tinymce.plugins.tagline', {
      init : function(ed, url) {
	        ed.addButton('tagline', {
            title : 'Insert a Tagline', 
            image : url+'/tiny_icons/tagline.png',

         	 onclick : function() {  
 					ed.selection.setContent('[tagline_container type="one or two" link="Link"  title="Tagline Title" second_line="Second Line goes here" iconlink="only for type2"][/tagline_container]');  
                } 
         });
      },
    createControl : function(n, cm) {  
        return null;  
        },  

    });  
    tinymce.PluginManager.add('tagline', tinymce.plugins.tagline);
})();

 

















///  shortcode - testimonials
////////////////////////////////////////////////////////////////////////////////////////////////////////////////..

(function() {
   tinymce.create('tinymce.plugins.testimonials', {
      init : function(ed, url) {
	        ed.addButton('testimonials', {
            title : 'Insert Testimonials', 
            image : url+'/tiny_icons/testimonial.png',

         	 onclick : function() {  
                     ed.selection.setContent('[testimonial name="Name Here" who="happy client" imagelinks="link to image" vertical="yes"]"Testimonial Text Here."[/testimonial]');
                     
                } 
         });
      },
    createControl : function(n, cm) {  
        return null;  
        },  

    });  
    tinymce.PluginManager.add('testimonials', tinymce.plugins.testimonials);
})();













///  shortcode - tabs
////////////////////////////////////////////////////////////////////////////////////////////////////////////////..

(function() {
   tinymce.create('tinymce.plugins.tabs', {
      init : function(ed, url) {
	        ed.addButton('tabs', {
            title : 'Insert a Tab', 
            image : url+'/tiny_icons/tabs.png',

         	 onclick : function() {  
                     ed.selection.setContent('[tabs  elements="Tab Name 1,Tab Name 2,Tab Name 3 etc.."][tab id=tab1]Tab Content 1[/tab][tab id=tab2]Tab Content 2[/tab][tab id=tab3]Tab Content 3[/tab][/tabs]');  
                } 
         });
      },
    createControl : function(n, cm) {  
        return null;  
        },  

    });  
    tinymce.PluginManager.add('tabs', tinymce.plugins.tabs);
})();




///  shortcode - checklist
////////////////////////////////////////////////////////////////////////////////////////////////////////////////..

(function() {
   tinymce.create('tinymce.plugins.checklist', {
      init : function(ed, url) {
	        ed.addButton('checklist', {
            title : 'Insert a list', 
            image : url+'/tiny_icons/lists.png',

         	 onclick : function() {  
                     ed.selection.setContent('[check-list title="List Title" type="type1 to  type 10"]<ul>\r<li>List element #1</li>\r<li>List element  #2</li>\r<li>List element  #3</li>\r</ul>[/check-list]');  
                } 
         });
      },
    createControl : function(n, cm) {  
        return null;  
        },  

    });  
    tinymce.PluginManager.add('checklist', tinymce.plugins.checklist);
})();



///  shortcode - highlight
////////////////////////////////////////////////////////////////////////////////////////////////////////////////..

(function() {
   tinymce.create('tinymce.plugins.highlight', {
      init : function(ed, url) {
	        ed.addButton('highlight', {
            title : 'Highlight a piece of text', 
            image : url+'/tiny_icons/highlight.png',

         	 onclick : function() {  
                     ed.selection.setContent('[highlight color="red,yellow or grey"]Text to highlight[/highlight]');  
                } 
         });
      },
    createControl : function(n, cm) {  
        return null;  
        },  

    });  
    tinymce.PluginManager.add('highlight', tinymce.plugins.highlight);
})();



///  shortcode - dropcap
////////////////////////////////////////////////////////////////////////////////////////////////////////////////..
	
	
(function() {
   tinymce.create('tinymce.plugins.dropcap', {
      init : function(ed, url) {
	        ed.addButton('dropcap', {
            title : 'Insert Drop Cap', 
            image : url+'/tiny_icons/dropcap.png',

         	 onclick : function() {  
   					 ed.selection.setContent('[dropcap type="type1 or type2"]Replace with Drop Cap Letter/Words[/dropcap]');  
                } 
         });
      },
    createControl : function(n, cm) {  
        return null;  
        },  

    });  
    tinymce.PluginManager.add('dropcap', tinymce.plugins.dropcap);
})();




///  shortcode - button
////////////////////////////////////////////////////////////////////////////////////////////////////////////////..

    
(function() {
   tinymce.create('tinymce.plugins.wpbutton', {
      init : function(ed, url) {
	        ed.addButton('wpbutton', {
            title : 'Insert Button', 
            image : url+'/tiny_icons/button.png',

         	 onclick : function() {  
  						  ed.selection.setContent('[button color="white,green,blue or purple"  link="" target="" text="Button Text"]');  
                } 
         });
      },
    createControl : function(n, cm) {  
        return null;  
        },  

    });  
    tinymce.PluginManager.add('wpbutton', tinymce.plugins.wpbutton);
})();







///  shortcode - vimeo
////////////////////////////////////////////////////////////////////////////////////////////////////////////////..


(function() {
   tinymce.create('tinymce.plugins.vimeo', {
      init : function(ed, url) {
	        ed.addButton('vimeo', {
            title : 'Insert Vimeo Video', 
            image : url+'/tiny_icons/vimeo_video.png',

         	 onclick : function() {  
                    ed.selection.setContent('[vimeo id="Video ID, something like 52236734" width="600" height="350"]'); 
                } 
         });
      },
    createControl : function(n, cm) {  
        return null;  
        },  

    });  
    tinymce.PluginManager.add('vimeo', tinymce.plugins.vimeo);
})();













///  shortcode - youtube
////////////////////////////////////////////////////////////////////////////////////////////////////////////////..


(function() {
   tinymce.create('tinymce.plugins.youtube', {
      init : function(ed, url) {
	        ed.addButton('youtube', {
            title : 'Insert YouTube Video', 
            image : url+'/tiny_icons/youtube_video.png',

         	 onclick : function() {  
                     ed.selection.setContent('[youtube id="Video ID, something like 9RLX3F3rVmk" width="600" height="350"]');  
                } 
         });
      },
    createControl : function(n, cm) {  
        return null;  
        },  

    });  
    tinymce.PluginManager.add('youtube', tinymce.plugins.youtube);
})();


///  shortcode - spacer
////////////////////////////////////////////////////////////////////////////////////////////////////////////////..


////////////////////////////////////////////////////////////////////////////////////////////////////////////////..
///  shortcode - font awesome
////////////////////////////////////////////////////////////////////////////////////////////////////////////////..

(function() {
   tinymce.create('tinymce.plugins.font_awesome', {
      init : function(ed, url) {
	        ed.addButton('font_awesome', {
                title : 'Font Awesome', 
                image : url+'/tiny_icons/font_awesome.png',

         	 onclick : function() {  
                     fontawesomeMenu();
                     //   ed.selection.setContent('[font_awesome][/font_awesome]');  
                } 
         });
      },
    createControl : function(n, cm) {  
        return null;  
        },  

    });  
    tinymce.PluginManager.add('font_awesome', tinymce.plugins.font_awesome);
})();



var ICONS = ["fa-adjust", 
"fa-anchor", 
"fa-archive", 
"fa-arrows", 
"fa-arrows-h", 
"fa-arrows-v", 
"fa-asterisk", 
"fa-ban", 
"fa-bar-chart-o", 
"fa-barcode", 
"fa-bars", 
"fa-beer", 
"fa-bell", 
"fa-bell-o", 
"fa-bolt", 
"fa-book", 
"fa-bookmark", 
"fa-bookmark-o", 
"fa-briefcase", 
"fa-bug", 
"fa-building-o", 
"fa-bullhorn", 
"fa-bullseye", 
"fa-calendar", 
"fa-calendar-o", 
"fa-camera", 
"fa-camera-retro", 
"fa-caret-square-o-down", 
"fa-caret-square-o-left", 
"fa-caret-square-o-right", 
"fa-caret-square-o-up", 
"fa-certificate", 
"fa-check", 
"fa-check-circle", 
"fa-check-circle-o", 
"fa-check-square", 
"fa-check-square-o", 
"fa-circle", 
"fa-circle-o", 
"fa-clock-o", 
"fa-cloud", 
"fa-cloud-download", 
"fa-cloud-upload", 
"fa-code", 
"fa-code-fork", 
"fa-coffee", 
"fa-cog", 
"fa-cogs", 
"fa-comment", 
"fa-comment-o", 
"fa-comments", 
"fa-comments-o", 
"fa-compass", 
"fa-credit-card", 
"fa-crop", 
"fa-crosshairs", 
"fa-cutlery", 
"fa-dashboard", 
"fa-desktop", 
"fa-dot-circle-o", 
"fa-download", 
"fa-edit", 
"fa-ellipsis-h", 
"fa-ellipsis-v", 
"fa-envelope", 
"fa-envelope-o", 
"fa-eraser", 
"fa-exchange", 
"fa-exclamation", 
"fa-exclamation-circle", 
"fa-exclamation-triangle", 
"fa-external-link", 
"fa-external-link-square", 
"fa-eye", 
"fa-eye-slash", 
"fa-female", 
"fa-fighter-jet", 
"fa-film", 
"fa-filter", 
"fa-fire", 
"fa-fire-extinguisher", 
"fa-flag", 
"fa-flag-checkered", 
"fa-flag-o", 
"fa-flash", 
"fa-flask", 
"fa-folder", 
"fa-folder-o", 
"fa-folder-open", 
"fa-folder-open-o", 
"fa-frown-o", 
"fa-gamepad", 
"fa-gavel", 
"fa-gear", 
"fa-gears", 
"fa-gift", 
"fa-glass", 
"fa-globe", 
"fa-group", 
"fa-hdd-o", 
"fa-headphones", 
"fa-heart", 
"fa-heart-o", 
"fa-home", 
"fa-inbox", 
"fa-info", 
"fa-info-circle", 
"fa-key", 
"fa-keyboard-o", 
"fa-laptop", 
"fa-leaf", 
"fa-legal", 
"fa-lemon-o", 
"fa-level-down", 
"fa-level-up", 
"fa-lightbulb-o", 
"fa-location-arrow", 
"fa-lock", 
"fa-magic", 
"fa-magnet", 
"fa-mail-forward", 
"fa-mail-reply", 
"fa-mail-reply-all", 
"fa-male", 
"fa-map-marker", 
"fa-meh-o", 
"fa-microphone", 
"fa-microphone-slash", 
"fa-minus", 
"fa-minus-circle", 
"fa-minus-square", 
"fa-minus-square-o", 
"fa-mobile", 
"fa-mobile-phone", 
"fa-money", 
"fa-moon-o", 
"fa-music", 
"fa-pencil", 
"fa-pencil-square", 
"fa-pencil-square-o", 
"fa-phone", 
"fa-phone-square", 
"fa-picture-o", 
"fa-plane", 
"fa-plus", 
"fa-plus-circle", 
"fa-plus-square", 
"fa-plus-square-o", 
"fa-power-off", 
"fa-print", 
"fa-puzzle-piece", 
"fa-qrcode", 
"fa-question", 
"fa-question-circle", 
"fa-quote-left", 
"fa-quote-right", 
"fa-random", 
"fa-refresh", 
"fa-reply", 
"fa-reply-all", 
"fa-retweet", 
"fa-road", 
"fa-rocket", 
"fa-rss", 
"fa-rss-square", 
"fa-search", 
"fa-search-minus", 
"fa-search-plus", 
"fa-share", 
"fa-share-square", 
"fa-share-square-o", 
"fa-shield", 
"fa-shopping-cart", 
"fa-sign-in", 
"fa-sign-out", 
"fa-signal", 
"fa-sitemap", 
"fa-smile-o", 
"fa-sort", 
"fa-sort-alpha-asc", 
"fa-sort-alpha-desc", 
"fa-sort-amount-asc", 
"fa-sort-amount-desc", 
"fa-sort-asc", 
"fa-sort-desc", 
"fa-sort-down", 
"fa-sort-numeric-asc", 
"fa-sort-numeric-desc", 
"fa-sort-up", 
"fa-spinner", 
"fa-square", 
"fa-square-o", 
"fa-star", 
"fa-star-half", 
"fa-star-half-empty", 
"fa-star-half-full", 
"fa-star-half-o", 
"fa-star-o", 
"fa-subscript", 
"fa-suitcase", 
"fa-sun-o", 
"fa-superscript", 
"fa-tablet", 
"fa-tachometer", 
"fa-tag", 
"fa-tags", 
"fa-tasks", 
"fa-terminal", 
"fa-thumb-tack", 
"fa-thumbs-down", 
"fa-thumbs-o-down", 
"fa-thumbs-o-up", 
"fa-thumbs-up", 
"fa-ticket", 
"fa-times", 
"fa-times-circle", 
"fa-times-circle-o", 
"fa-tint", 
"fa-toggle-down", 
"fa-toggle-left", 
"fa-toggle-right", 
"fa-toggle-up", 
"fa-trash-o", 
"fa-trophy", 
"fa-truck", 
"fa-umbrella", 
"fa-unlock", 
"fa-unlock-alt", 
"fa-unsorted", 
"fa-upload", 
"fa-user", 
"fa-users", 
"fa-video-camera", 
"fa-volume-down", 
"fa-volume-off", 
"fa-volume-up", 
"fa-warning", 
"fa-wheelchair", 
"fa-wrench", 
"fa-check-square", 
"fa-check-square-o", 
"fa-circle", 
"fa-circle-o", 
"fa-dot-circle-o", 
"fa-minus-square", 
"fa-minus-square-o", 
"fa-plus-square-o", 
"fa-square", 
"fa-square-o", 
"fa-bitcoin", 
"fa-btc", 
"fa-cny", 
"fa-dollar", 
"fa-eur", 
"fa-euro",
"fa-gbp", 
"fa-inr", 
"fa-jpy", 
"fa-krw", 
"fa-money", 
"fa-rmb", 
"fa-rouble", 
"fa-rub", 
"fa-ruble", 
"fa-rupee", 
"fa-try", 
"fa-turkish-lira", 
"fa-usd", 
"fa-won", 
"fa-yen", 
"fa-align-center", 
"fa-align-justify", 
"fa-align-left", 
"fa-align-right", 
"fa-bold", 
"fa-chain", 
"fa-chain-broken", 
"fa-clipboard", 
"fa-columns", 
"fa-copy", 
"fa-cut", 
"fa-dedent", 
"fa-eraser", 
"fa-file", 
"fa-file-o", 
"fa-file-text", 
"fa-file-text-o", 
"fa-files-o", 
"fa-floppy-o", 
"fa-font", 
"fa-indent", 
"fa-italic", 
"fa-link", 
"fa-list", 
"fa-list-alt", 
"fa-list-ol", 
"fa-list-ul", 
"fa-outdent", 
"fa-paperclip", 
"fa-paste", 
"fa-repeat", 
"fa-rotate-left", 
"fa-rotate-right", 
"fa-save", 
"fa-scissors", 
"fa-strikethrough", 
"fa-table", 
"fa-text-height", 
"fa-text-width", 
"fa-th", 
"fa-th-large", 
"fa-th-list", 
"fa-underline", 
"fa-undo", 
"fa-unlink", 
"fa-angle-double-down", 
"fa-angle-double-left", 
"fa-angle-double-right", 
"fa-angle-double-up", 
"fa-angle-down", 
"fa-angle-left", 
"fa-angle-right", 
"fa-angle-up", 
"fa-arrow-circle-down", 
"fa-arrow-circle-left", 
"fa-arrow-circle-o-down", 
"fa-arrow-circle-o-left", 
"fa-arrow-circle-o-right", 
"fa-arrow-circle-o-up", 
"fa-arrow-circle-right", 
"fa-arrow-circle-up", 
"fa-arrow-down", 
"fa-arrow-left", 
"fa-arrow-right", 
"fa-arrow-up", 
"fa-arrows", 
"fa-arrows-alt", 
"fa-arrows-h", 
"fa-arrows-v", 
"fa-caret-down", 
"fa-caret-left", 
"fa-caret-right", 
"fa-caret-square-o-down", 
"fa-caret-square-o-left", 
"fa-caret-square-o-right", 
"fa-caret-square-o-up", 
"fa-caret-up", 
"fa-chevron-circle-down", 
"fa-chevron-circle-left", 
"fa-chevron-circle-right", 
"fa-chevron-circle-up", 
"fa-chevron-down", 
"fa-chevron-left", 
"fa-chevron-right", 
"fa-chevron-up", 
"fa-hand-o-down", 
"fa-hand-o-left", 
"fa-hand-o-right", 
"fa-hand-o-up", 
"fa-long-arrow-down", 
"fa-long-arrow-left", 
"fa-long-arrow-right", 
"fa-long-arrow-up", 
"fa-toggle-down", 
"fa-toggle-left", 
"fa-toggle-right", 
"fa-toggle-up", 
"fa-arrows-alt", 
"fa-backward", 
"fa-compress", 
"fa-eject", 
"fa-expand", 
"fa-fast-backward", 
"fa-fast-forward", 
"fa-forward", 
"fa-pause", 
"fa-play", 
"fa-play-circle", 
"fa-play-circle-o", 
"fa-step-backward", 
"fa-step-forward", 
"fa-stop", 
"fa-youtube-play", 
"fa-adn", 
"fa-android", 
"fa-apple", 
"fa-bitbucket", 
"fa-bitbucket-square", 
"fa-bitcoin", 
"fa-btc", 
"fa-css3", 
"fa-dribbble", 
"fa-dropbox", 
"fa-facebook", 
"fa-facebook-square", 
"fa-flickr", 
"fa-foursquare", 
"fa-github", 
"fa-github-alt", 
"fa-github-square", 
"fa-gittip", 
"fa-google-plus", 
"fa-google-plus-square", 
"fa-html5", 
"fa-instagram", 
"fa-linkedin", 
"fa-linkedin-square", 
"fa-linux", 
"fa-maxcdn", 
"fa-pagelines", 
"fa-pinterest", 
"fa-pinterest-square", 
"fa-renren", 
"fa-skype", 
"fa-stack-exchange", 
"fa-stack-overflow", 
"fa-trello", 
"fa-tumblr", 
"fa-tumblr-square", 
"fa-twitter", 
"fa-twitter-square", 
"fa-vimeo-square", 
"fa-vk", 
"fa-weibo", 
"fa-windows", 
"fa-xing", 
"fa-xing-square", 
"fa-youtube", 
"fa-youtube-play", 
"fa-youtube-square",];
function allfonticons(){
    var return_string='';
    var icon;
    for (var i = 0; i < ICONS.length; i++) {
            icon = ICONS[i];
            //listBox.add(icon(_id) + ' ' + _id, _id);
            return_string = return_string+'<li><i class="fa '+icon+'"></i><span>'+icon+'</span></li>';
    }
 return return_string;
                         

}

function fontawesomeMenu(){

    var iconslist, width;
        iconslist = jQuery('<style>.font-awesome-icons li:hover span { color:#000 !important; cursor: pointer !important } .font-awesome-icons li {margin-bottom: 15px;width: 22%;display: block;margin-right: 3%;float: left;line-height: 15px;}.font-awesome-icons li span{color:#aaa;margin-left: 10px;};.font-awesome-icons li span:before {content:"\a"}</style>\n\
    <div id="thundercodes-form">\n\
    <ul class="font-awesome-icons">'+allfonticons()+'</ul></div>');
        iconslist.appendTo('body').hide();
        tb_show( 'Font awesome Icons', '#TB_inline?inlineId=thundercodes-form' );
        jQuery("#TB_window").css("height","415");
        jQuery("#TB_window").css("overflow-y","auto");
        jQuery("#TB_window").css("overflow-x","hidden");
        jQuery("#TB_ajaxContent").css( "width","auto");
        jQuery("#TB_ajaxContent").css( "height","auto");
          
        jQuery(".font-awesome-icons li").click(function(){
                insertShortcode(jQuery(this).find("i").attr("class"));
        });
		
}

function insertShortcode(daclass){
	tinyMCE.activeEditor.execCommand('mceInsertContent', 0, "[font_awesome icon='"+daclass+"' size='15px']");
	tb_remove();
}