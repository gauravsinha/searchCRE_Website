<?php
// Template Name: Zillow Estimate
// Wp Estate Pack
get_header();
$options        =   sidebar_orientation($post->ID);
$custom_image   =   esc_html( esc_html(get_post_meta($post->ID, 'page_custom_image', true)) );   


// get page border if any
$border_option = esc_html( get_post_meta($post->ID, 'border_option', true) );
$border='';

switch ($border_option){
    case 'agent border':
        $border='agentborder';
        break;
    case 'listing border':
        $border='listingborder ';
        break;
    case 'blog border':
       $border='blogborder';
        break;
    case 'white border':
       $border='none';
        break;
    case 'no border':
       $border='';
        break;
     case '':
        $border='none';
        break;
}

$message                =   '';
$hasError               =   false;
$receiver_email         =   is_email ( get_bloginfo('admin_email') );
$sell_estimate_city     =   '';
$sell_estimate_state    =   '';
$sell_estimate_adr      =   '';

if( isset( $_POST['zill_estimate_state'] )  ){
    if (  trim($_POST['zill_estimate_state']) == '') {
        $hasError = true;
        $error[] = __('The state field is empty !','wpestate');
    } else {
        $sell_estimate_state = esc_html( trim($_POST['zill_estimate_state']) );
    } 
}



if( isset( $_POST['zill_estimate_city'] )  ){
    if (trim($_POST['zill_estimate_city']) == '') {
        $hasError = true;
        $error[] = __('The City field is empty !','wpestate');
    } else {
        $sell_estimate_city = esc_html( trim($_POST['zill_estimate_city']) );
    }
}

if( isset( $_POST['zill_estimate_adr'] )  ){
    if (trim($_POST['zill_estimate_adr']) == '') {
        $hasError = true;
        $error[] = __('Your address field is empty!','wpestate');
    } else {
        $sell_estimate_adr = esc_html( trim ($_POST['zill_estimate_adr'] ) );
    }     
}


$estimates=array();

if (!$hasError) {
   $estimates = call_zillow($sell_estimate_adr,$sell_estimate_city,$sell_estimate_state);
}
else {
    $hasError = true;
}

 
    
$to_print='';
if ($hasError) {
    foreach ($error as $mes) {
        $to_print.=$mes . '<br />';
    }
}


//////////////////////////////////////////////////////////////////////////////////////
//// Call zillow
//////////////////////////////////////////////////////////////////////////////////////
function call_zillow( $sell_estimate_adr,$sell_estimate_city,$sell_estimate_state){
    $key =  esc_html ( get_option('wp_estate_zillow_api_key','') );
    $return_array=array();
   
    
    $addr   =   urlencode ($sell_estimate_adr);
    $city   =   urlencode ($sell_estimate_city);
    $state  =   urlencode ($sell_estimate_state);
    
    $location=$city.','.$state;
     $url="http://www.zillow.com/webservice/GetSearchResults.htm?zws-id=".$key."&address=".$addr."&citystatezip=".$location ; 
    
    $xml = simplexml_load_file($url) 
        or die("Error: Could not connect to Zillow API");

  
 
    if(  $xml->message[0]->code[0] >= 500  ){
         $return_array['suma']=0;
    }else{
        $return_array['suma']=    $xml->response[0]->results[0]->result[0]->zestimate[0]->amount[0];
        $return_array['data']=    $xml->response[0]->results[0]->result[0]->zestimate[0]->{'last-updated'}[0];
            
    }
     return $return_array; 
}
?>


<!-- Google Map Code -->
<?php 
get_template_part('libs/templates/map-template'); 
?> 
<!-- Google Map Code -->



<div id="wrapper" class="<?php print $options['fullwhite']; ?>">  
    <div class="<?php print $options['add_back']; ?>"></div>

    <?php
    print breadcrumb_container($options['full_breadcrumbs'],$options['bread_align'] )
    ?>
    <div id="main" class="row <?php print $options['sidebar_status']; ?>">
    <?php
    print display_breadcrumbs( $options['full_breadcrumbs'] ,$options['bread_align_internal'] )
    ?>

        
        <!-- begin content--> 
        <div id="post" class="is_page <?php print $options['grid'].' ' . $options['shadow'].' '.$border;?>  
            <?php 
             if (is_front_page()) { 
                 print ' inside_no_bottom';
            } 
            ?> 
             "> 
            
            <div class="inside_post inside_no_border <?php
            if (is_front_page()) {
                print 'is_home_page';
            }
            ?>" >
                <?php while (have_posts()) : the_post(); ?>
                    <?php if (esc_html( get_post_meta($post->ID, 'page_show_title', true) ) != 'no') { ?>
                        <h1 class="entry-title"><?php the_title(); ?></h1>
                    <?php } ?>
                    <?php 
                    print $to_print;
                    if (!$hasError){
                        if($estimates['suma']!=0){
                         print '<div class="estimate-result">
                             <img src="'.get_template_directory_uri().'/images/zillow-logo.png" alt="logo"/>
                             <h3> On '.$estimates['data'].' this property is estimated at </h3>
                                <div class="zillow-price">$ '.number_format( intval ( $estimates['suma'] )  ).'</div> 
                                <div class="zillow-details">
                                    Address: '.$sell_estimate_adr.' </br>
                                    City: '.$sell_estimate_city.'</br>
                                    State: '.$sell_estimate_state.'</br>
                                </div>
                             </div>';
                        }else{
                          print '<div class="estimate-result">'.__('We are sorry, but we don\'t have an estimation for this property at this moment!  ','wpestate').'</div>';
                        }
                        the_content();
                    }else{
                        _e('Please fill in the form correctly and try again!','wpestate');
                    }
                    ?>


                    <?php endwhile; // end of the loop.  ?>
             </div><!-- end inside post-->
        </div>
        <!-- end content-->





        <?php  include(locate_template('customsidebar.php')); ?>
        
    </div><!-- #main -->    
</div><!-- #wrapper -->
<?php get_footer(); ?>