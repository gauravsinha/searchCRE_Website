<!-- related posts --> 	
<?php
$counter = 0;
  $collum_class='four'; 
$tags = wp_get_post_tags($post->ID);
if($options['related_no']==4){
   $collum_class='three'; 
}

if ($tags) {
       
        $first_tag = $tags[0]->term_id;
        $args = array(
            'tag__in' => array($first_tag),
            'post__not_in' => array($post->ID),
            'showposts' => $options['related_no']
        );
        wp_reset_query();
        $my_query = new WP_Query($args);
        
        
        if ( $my_query->have_posts() ) { ?>	

          <div class="related_posts blog_bottom_border blog_listing_image"> 
                <h2><?php _e('Related Posts', 'wpestate'); ?></h2>   
              
                    <?php
                    while ($my_query->have_posts()) {
                        $counter++;
                        $class = "";
                        if ($counter == 1) {
                            $class = "alpha";
                        }

                        if ($counter == $options['related_no']) {
                            $class = "omega";
                        }

                       
                         

                        $my_query->the_post(); ?>
                          <div class="columns  <?php print $class.' '.$collum_class ?>"> 
                            <?php if (has_post_thumbnail()) { ?>
                            <figure>
                            <?php 
                              $preview = wp_get_attachment_image_src(get_post_thumbnail_id(), 'property_full');
                                $extra= array(
                                    'data-original'=>$preview[0],
                                    'class'	=> 'lazyload',    
                                );
                            the_post_thumbnail('property_full',$extra);
                            }?>
                                <figcaption class="figcaption-post" data-link="<?php the_permalink(); ?>">
                                     <span class="fig-icon"></span>
                                </figcaption>
                            </figure>  
                            <h3 class="listing_title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                            <p><?php
                            $excerpt = get_the_excerpt();
                            print strip_words($excerpt, 14);?>...</p>
                            <a href="<?php the_permalink();?>" class="blog_link"><span class="blog_plus">+</span> <?php _e('read more','wpestate');?></a>
                        </div>
                        <?php
                       
                     } //end while
                    ?>
                
          </div>		

        <?php } //endif have post
}// end if tags
else{
   print'<div class="single-spacer"></div>';
}
 
wp_reset_query();
?> 

