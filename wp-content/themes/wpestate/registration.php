<?php
/* 
Template Name: Registration
*/
get_header();

global $user_ID;

if ($user_ID!='') {
	echo "<script type='text/javascript'>window.location='". get_bloginfo('url') ."'</script>";
}

$options        =   sidebar_orientation($post->ID);
$custom_image   =   esc_html( esc_html(get_post_meta($post->ID, 'page_custom_image', true)) );   


// get page border if any
$border_option = esc_html( get_post_meta($post->ID, 'border_option', true) );
$border='';

switch ($border_option){
    case 'agent border':
        $border='agentborder';
        break;
    case 'listing border':
        $border='listingborder ';
        break;
    case 'blog border':
       $border='blogborder';
        break;
    case 'white border':
       $border='none';
        break;
    case 'no border':
       $border='';
        break;
     case '':
        $border='none';
        break;
}

?>


<!-- Google Map Code -->
<?php 
//get_template_part('libs/templates/map-template'); 
?>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js"></script>


<script type="text/javascript">
$(document).ready(function() {
	 
	$('#role').attr('checked', false)
	//for phone number_format ;
    $('.number_validation').keypress(function(key) {
		
        if(!key.charCode == 46 && key.charCode < 48 || key.charCode > 57) return false;
    });
	
	
	$('#role').change(function(){
		
        var checked = $(this).attr('checked');
        if (checked) { 
           $('#brokers').show();             
        } else {
            $('#brokers').hide();
        }
    });        

	});
</script>
 
<?php /*
<script src="<?php bloginfo('template_directory');?>/js/reg/jquery.min.js"></script>
	<script src="<?php bloginfo('template_directory');?>/js/reg/jquery.validate.min.js"></script>
	<!-- Bootstrap -->*/?>


<div id="wrapper" class="<?php print $options['fullwhite']; ?>">  
    <div class="<?php print $options['add_back']; ?>"></div>

    <?php
    //print breadcrumb_container($options['full_breadcrumbs'],$options['bread_align'] )
    ?>
    <div id="main" class="row no sidebar">
    <?php
    //print display_breadcrumbs( $options['full_breadcrumbs'] ,$options['bread_align_internal'] )
    ?>

        
        <!-- begin content--> 
        <div id="post" class="is_page twelve columns alpha nomargin">
	
	<?php
	$err = '';
	$success = '';

	global $wpdb, $PasswordHash, $current_user, $user_ID;

	if(isset($_POST['task']) && $_POST['task'] == 'register' ) {
		//print_r($_POST);die;
		
		$pwd1 = $wpdb->escape(trim($_POST['pwd1']));
		$pwd2 = $wpdb->escape(trim($_POST['pwd2']));
		$email = $wpdb->escape(trim($_POST['email']));
		$username = $wpdb->escape(trim($_POST['username']));
		 $role = $wpdb->escape(trim($_POST['role']));
		 $img=$wpdb->escape(trim($_POST['user_img']));
		 $imgurl=$wpdb->escape(trim($_POST['user_img_url']));
		 
		$firstname = $wpdb->escape(trim($_POST['firstname']));
		$lastname = $wpdb->escape(trim($_POST['lastname']));
		$userphone = $wpdb->escape(trim($_POST['userphone']));
		$usermobile = $wpdb->escape(trim($_POST['usermobile']));
		$userskype = $wpdb->escape(trim($_POST['userskype']));
		$usercity = $wpdb->escape(trim($_POST['usercity']));
		$usercompany = $wpdb->escape(trim($_POST['usercompany']));
		$userposition = $wpdb->escape(trim($_POST['userposition']));
		$about_me = $wpdb->escape(trim($_POST['about_me']));
		 
		//print_r($_POST);die;
		if($username=='') {
			$erruser = 'Please enter user name.';
		} else if(username_exists($username) ) {
			$erruser = 'user name already exist.';
		}else if($email=='') {
			$erremail = 'Please enter email address.';
		}else if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$erremail = 'Invalid email address.';
		} else if(email_exists($email)) {
			$erremail = 'Email already exist.';
		}elseif($pwd1==''){
			$errpwd = 'Please enter Password.';		
	}else if($pwd1 <> $pwd2 ){
			$errpwd2 = 'Password do not match.';		
		} else {
			if($role!=''){
				$post_information = array(
				'post_title' => wp_strip_all_tags( $_POST['username'] ),
				'post_type' => 'estate_agent',
				'post_status' => 'publish'
				);
			
				$post_id=wp_insert_post( $post_information );
				update_post_meta($post_id,'_thumbnail_id',$img);
				update_post_meta($post_id, 'agent_email',  $email);
				update_post_meta($post_id, 'agent_phone',  $userphone);
				update_post_meta($post_id, 'agent_skype',  $userskype);
				update_post_meta($post_id, 'agent_position',  $userposition);
				update_post_meta($post_id, 'agent_mobile',  $usermobile);
				update_post_meta($post_id, 'agent_city',  $usercity);
				update_post_meta($post_id, 'agent_company',  $usercompany);

			}
	if($role !=''){
					$rl="contributor";
				} else {
					$rl="subscriber";
				}
				$user_id = wp_insert_user( array ('user_login'=>apply_filters('pre_user_user_login', $username),'user_pass' => apply_filters('pre_user_user_pass', $pwd1),'user_email' => apply_filters('pre_user_user_email', $email), 'role' => $rl,'user_agent_id' => $post_id));
			if( is_wp_error($user_id) ) {
				$err = 'Error on user creation.';
			} else {
					update_user_meta($user_id, 'custom_picture', $imgurl);
					update_user_meta( $user_id, 'first_name', $firstname ) ;
					update_user_meta( $user_id, 'last_name',  $secondname) ;
					update_user_meta( $user_id, 'phone' , $userphone) ;
					update_user_meta( $user_id, 'mobile' , $usermobile) ;
					update_user_meta( $user_id, 'description' , $about_me) ;
					update_user_meta( $user_id, 'skype' , $userskype) ;
					update_user_meta( $user_id, 'title', $userposition) ;
					update_user_meta( $user_id, 'city', $usercity) ;
					update_user_meta( $user_id, 'company', $usercompany) ;

				//echo $user_id;die;
				do_action('user_register', $user_id);
				
				$success = 'You have successfully registered';
				echo "<script type='text/javascript'>window.location='". get_bloginfo('url') ."/?page_id=6902'</script>";
				?>
				
				<?php 
			}
			
		}
		
	}
	?>

        <!--display error/success message-->
	<div id="message">
		
		<?php 
			if(! empty($err) ) :
				echo '<p class="error">'.$err.'';
			endif;
		?>
		<?php 
			if(! empty($success) ) :
				echo '<p class="error">'.$success.'';
			endif;
		?>
	</div>
<?php
global $current_user;
$user_roles = $current_user->roles[0];
//$user_roles = $current_user->roles;
//print_r($user_roles);
	
?>
	<form method="post" id="frm" style="padding-bottom:100px;">
	<h3 style="border-bottom:1px #1791D2 solid;margin-bottom:40px;color:#1791D2;">Registration</h3>
	<div class="six columns alpha nomargin">
		
		
		
		
		<p style="clear:both;padding-top:20px;"><label>User Name</label></p>
		<p><input type="text" value="<?php echo $username;?>" name="username" id="username" class="reg_text" /><?php if(!empty(erruser)){
			echo "<span class='error'>". $erruser."</span>";
		} ?><br/></p>
		
		<p style="clear:both;padding-top:0px;"><label>Email</label></p>
		<p><input type="text" value="<?php echo $email;?>" name="email" id="email" class="reg_text" /><span style="color:#FF4676;" id="user_emailErr"><?php if(!empty(erremail)){
			echo "<span class='error'>".$erremail."</span>";
		} ?></span><br/></p>
		
			<p><label>Password</label></p>
		<p><input type="password" value="" name="pwd1" id="pwd1" class="reg_text" /><?php if(!empty(errpwd)){
			echo "<span class='error'>". $errpwd."</span>";
		} ?></p>
		<p style="clear:both;padding-top:10px;"><label>Confirm Password</label></p>
		<p><input type="password" value="" name="pwd2" id="pwd2" class="reg_text" /><?php if(!empty(errpwd2)){
			echo "<span class='error'>". $errpwd2."</span>";
		} ?></p>
		<p style="clear:both;padding-top:15px;padding-bottom:15px;">
		<input type="checkbox" value="contributer" name="role" id="role" style="margin-top:5px !important;display:block;float:left;width:15px;height:15px;margin:0px;brder:1px #ccc solid;" /> 
		<label style="font-size:16px;padding-left:10px;float:left;">Register as Broker</label></p>
		<div id="brokers" style="display:none;">
		   
                <div class="profile_div" id="profile-div">
                    <div class="featured_agent_image" id="profile-image" style="background-image: url(<?php print $user_custom_picture;?>);"   >
                        <input type="hidden" id="profile-image_id" name="user_img" value="">
						<input type="hidden" id="profile-image_url" name="user_img_url" value="">
                    </div>  

                    <div id="upload-container" style="width:auto;clear:none;padding-left:40px;padding-top:20px;">                 
                        <div id="aaiu-upload-container">                 
                            <a id="aaiu-uploader" class="aaiu_button" href="#"><?php _e('Upload Profile Image','wpestate');?></a>
                           
                            <div id="aaiu-upload-imagelist">
                                <ul id="aaiu-ul-list" class="aaiu-upload-list"></ul>
                            </div>
							
                        </div>  
						 <span class="upload_explain"><?php _e('*recommended size: at least 160 px tall & wide','wpestate');?></span>
                    </div>
                   
                </div>
		<p style="clear:both;"><label>First Name</label></p>
		<p><input type="text" value="" name="firstname" id="firstname" class="reg_text" /><br/></p>
		
		<p style="clear:both;"><label>Last Name</label></p>
		<p><input type="text" value="" name="lastname" id="lastname" class="reg_text" /><br/></p>
		
		<p style="clear:both;"><label>Phone</label></p>
		<p><input type="text" value="" name="userphone" id="userphone" class="number_validation Phone reg_text" /><br/></p>
		
		<p style="clear:both;"><label>Mobile</label></p>
		<p><input type="text" value="" name="usermobile" id="usermobile" class="number_validation Phone reg_text" /><br/></p>
		
		<p style="clear:both;"><label>Skype</label></p>
		<p><input type="text" value="" name="userskype" id="userskype" class="reg_text" /><br/></p>
		
		<p style="clear:both;"><label>City</label></p>
		<p><input type="text" value="" name="usercity" id="usercity" class="reg_text" /><br/></p>
		
		<p style="clear:both;"><label>Company</label></p>
		<p><input type="text" value="" name="usercompany" id="usercompany" class="reg_text" /><br/></p>
		
		<p style="clear:both;"><label>Position</label></p>
		<p><input type="text" value="" name="userposition" id="userposition" class="reg_text" /><br/></p>
		
		<p style="clear:both;"><label>About me</label></p>
		<p><textarea id="about_me" name="about_me"></textarea><br/></p>
				
		</div>
		<p>
		<?php /*<p><?php if($sucess != "") { echo $sucess; } ?> <?php if($err != "") { echo $err; } ?></p>*/?>
		<button type="submit" name="btnregister" class="button" style="width:99%;height:40px;margin-top:10px;background:#005375;color:#fff;border:none;font-size:15px;">SUBMIT</button>
		<input type="hidden" name="task" value="register" />
		</p><br/><br/><br/>
	 </div>	
	</form>

</div></div></div>

<?php get_footer() ?>

<script type="text/javascript">
$(document).ready(function() {
	//for phone number_format ;
    $('.number_validation').keypress(function(key) {
		
        if(!key.charCode == 46 && key.charCode < 48 || key.charCode > 57) return false;
    });
	});
</script>
<script type="text/javascript">
$(document).ready(function() {
	
$('#frm').on("submit",function(){
	var imgurl=$('#profile-image').attr("data-profileurl");
	
	$('#profile-image_url').val(imgurl);
});
});
</script>