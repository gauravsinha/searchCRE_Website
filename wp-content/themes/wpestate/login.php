<?php
/*
Template Name: User Login
*/
global $user_ID;

if (!$user_ID) {

	if($_POST['log']!=''){
		//We shall SQL escape all inputs
		$username = $wpdb->escape($_REQUEST['username']);
		$password = $wpdb->escape($_REQUEST['password']);
		$remember = $wpdb->escape($_REQUEST['rememberme']);
	
		if($remember) $remember = "true";
		else $remember = "false";

		if (filter_var($username, FILTER_VALIDATE_EMAIL)) { //Invalid Email

	        $user = get_user_by('email', $username);

	    } else {

	    $user = get_user_by('login', $username);

	    }
		//print_r($user);die;
		$login_data = array();
		$login_data['user_login'] = $user->data->user_login;
		$login_data['user_password'] = $password;
		$login_data['remember'] = $remember;
		$user_verify = wp_signon( $login_data, false ); 
		//wp_signon is a wordpress function which authenticates a user. It accepts user info parameters as an array.
		//print_r($user_verify);
		if ( is_wp_error($user_verify) ) 
		{
			$err ="invalid email or password";
		   //$err= "<span class='error'>Invalid username or password. Please try again!</span>".$user_verify->get_error_message();
		   //exit();
		 } else 
		 {	
			echo "<script type='text/javascript'>window.location='". get_bloginfo('url') ."'</script>";
			exit();
		  }
	}  

get_header();
$options        =   sidebar_orientation($post->ID);
$custom_image   =   esc_html( esc_html(get_post_meta($post->ID, 'page_custom_image', true)) );   


// get page border if any
$border_option = esc_html( get_post_meta($post->ID, 'border_option', true) );
$border='';

switch ($border_option){
    case 'agent border':
        $border='agentborder';
        break;
    case 'listing border':
        $border='listingborder ';
        break;
    case 'blog border':
       $border='blogborder';
        break;
    case 'white border':
       $border='none';
        break;
    case 'no border':
       $border='';
        break;
     case '':
        $border='none';
        break;
}

?>


<!-- Google Map Code -->
 <?php 
//get_template_part('libs/templates/map-template'); 
?>
<script src="<?php bloginfo('template_directory');?>/js/reg/jquery.min.js"></script>
	<script src="<?php bloginfo('template_directory');?>/js/reg/jquery.validate.min.js"></script>
	<!-- Bootstrap -->
<script>
  
  // When the browser is ready...
$( document ).ready(function() {
  
    // Setup form validation on the #register-form element
    $("#wp_login_form").validate({
    
        // Specify the validation rules
        rules: {
            username: {
				required:true,
				email:true
			},
            password: "required",
            pwd1: {
                required: true
                
            },
			pwd2: {
				equalTo:"#pwd1",
                required: true
                
            }
        },
        
        // Specify the validation error messages
        messages: {
			username:{
				required:"Please enter email",
				email:"please enter valid email"
			},
            password: "Please enter your password",
            pwd2: {
				required: "Please enter password",
				equalTo:"Password not matching"
			}, 
            pwd1: {
                required: "Please enter password",
                minlength: "Your password must be at least 5 characters long"
            }
        },
        
        submitHandler: function(form) {
            form.submit();
        }
    });

  });
  
  </script>

 
<!-- <script src="http://code.jquery.com/jquery-1.4.4.js"></script> --> <!-- Release the comments if you are not using jQuery already in your theme -->
<div id="wrapper" class="<?php print $options['fullwhite']; ?>">  
    <div class="<?php print $options['add_back']; ?>"></div>

    <?php
    //print breadcrumb_container($options['full_breadcrumbs'],$options['bread_align'] )
    ?>
    <div id="main" class="row no sidebar">
    <?php
    //print display_breadcrumbs( $options['full_breadcrumbs'] ,$options['bread_align_internal'] )
    ?>

        
        <!-- begin content--> 
        <div id="post" class="is_page twelve" style="padding-bottom:70px;">
<div id="result"></div> <!-- To hold validation results -->
<div id="message">
		<?php 
			if(! empty($err) ) :
				echo '<p class="error">'.$err.'';
			endif;
		?>
<form id="wp_login_form" action="" method="post" style="padding-bottom:100px;">
<h3 style="border-bottom:1px #1791D2 solid;margin-bottom:40px;color:#1791D2;">Login</h3>
<p><label>Email </label></p>
<p><input type="text" name="username" class="text" value="" style="width:400px;border:1px #ccc solid;"/></p>
<p><label>Password</label></p>
<p><input type="password" name="password" class="text" value=""  style="width:400px;border:1px #ccc solid;background:#fafafa;"/></p>
<p style="clear:both;padding-top:10px;"><input name="rememberme" type="checkbox" value="forever"  style="display:block;float:left;width:15px;height:15px;margin:0px;brder:1px #ccc solid;" />
<label style="font-size:16px;padding-left:10px;">Remember me</label></p>
<input name="log" type="hidden" value="log" />
<br />
<input type="submit" id="submitbtn" name="submit" value="LOGIN" style="width:414px;height:40px;margin-top:10px;background:#005375;color:#fff;border:none;" />
<br/><br/>
<a href="<?php echo get_home_url(); ?>?page_id=6906" style="text-decoration:underline;">Forgot password?</a><br/><br/>
Don't have an account  <a href="<?php echo get_home_url(); ?>?page_id=6904" style="font-weight:bold;text-decoration:underline;"> Register Here</a>
</form>


</div>
</div>
</div>
<?php

get_footer();
	
}
else {
	echo "<script type='text/javascript'>window.location='". get_bloginfo('url') ."'</script>";
}
?>