<?php
if (!is_active_sidebar('first-footer-widget-area') && !is_active_sidebar('second-footer-widget-area') && !is_active_sidebar('third-footer-widget-area') && !is_active_sidebar('fourth-footer-widget-area')
)
return;
?>


<?php if (is_active_sidebar('first-footer-widget-area')) : ?>
    <div id="first" class="widget-area">
        <div class="logo-footer">
            <a href="<?php echo home_url(); ?>">
                    <?php
                    $logo = esc_html( get_option('wp_estate_footer_logo_image', '') );
                    if ($logo != '') {
                    ?>
                    <img src="<?php print $logo; ?>" alt="logo"/>	
                    <?php } else { ?>
                        <img src="<?php print get_template_directory_uri(); ?>/images/logo-footer.png" alt="logo"/>
                    <?php } ?>
            </a>
        </div>

        <ul class="xoxo">
            <?php dynamic_sidebar('first-footer-widget-area'); ?>
        </ul>
    </div><!-- #first .widget-area -->
<?php endif; ?>

    
    
<?php if (is_active_sidebar('second-footer-widget-area')) : ?>
    <div id="second" class="widget-area">
        <ul class="xoxo">
        <?php dynamic_sidebar('second-footer-widget-area'); ?>
        </ul>
    </div><!-- #second .widget-area -->
<?php endif; ?>

    
    
    
<?php if (is_active_sidebar('third-footer-widget-area')) : ?>
    <div id="third" class="widget-area">
        <ul class="xoxo">
        <?php dynamic_sidebar('third-footer-widget-area'); ?>
        </ul>
    </div><!-- #third .widget-area -->
<?php endif; ?>
    
    
    

<?php if (is_active_sidebar('fourth-footer-widget-area')) : ?>
    <div id="fourth" class="widget-area last">
        <ul class="xoxo">
        <?php dynamic_sidebar('fourth-footer-widget-area'); ?>
        </ul>
    </div><!-- #fourth .widget-area -->
<?php endif; ?>


