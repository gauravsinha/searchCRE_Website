<?php
// Template Name: User Dashboard Favorite
// Wp Estate Pack
if ( !is_user_logged_in() ) {   
     wp_redirect(  home_url() );exit;
} 



global $current_user;
get_currentuserinfo();     
$paid_submission_status         =   esc_html ( get_option('wp_estate_paid_submission','') );
$price_submission               =   floatval( get_option('wp_estate_price_submission','') );
$submission_curency_status      =   esc_html( get_option('wp_estate_submission_curency','') );


$userID                     =   $current_user->ID;
$user_option                =   'favorites'.$userID;
$curent_fav                 =   get_option($user_option);
    
$currency       =   esc_html( get_option('wp_estate_currency_symbol', '') );
$where_currency =   esc_html( get_option('wp_estate_where_currency_symbol', '') );

get_header();
$options=sidebar_orientation($post->ID);
get_template_part('libs/templates/map-template'); 
?> 
<!-- Google Map Code -->



<div id="wrapper" class="fullwhite">  
    <div class="<?php print $options['add_back']; ?>"></div>

     <?php
    print breadcrumb_container('yes',$options['bread_align'] )
    ?>
    <div id="main" class="row">
    <?php
    print display_breadcrumbs( 'yes' ,$options['bread_align_internal'] )
    ?>




        <!-- begin content--> 
        <div id="post" class=" noborder twelve columns alpha noshadow favoritediv"> 
            <div class="inside_post inside_no_border submit_area">
               <?php if (esc_html( get_post_meta($post->ID, 'page_show_title', true) ) != 'no') { ?>
                    <h1 class="entry-title-agent-page"><?php the_title(); ?></h1>
               <?php } ?>
                  
               <?php get_template_part('libs/templates/user_menu'); 
               print '<div class="user_profile_div" >';
               
               if( !empty($curent_fav)){
                    $args = array(
                        'post_type' => 'estate_property',
                        'post_status'       => 'publish',
                        'posts_per_page' => -1 ,
                        'post__in' => $curent_fav 
                    );
               
               
                    $prop_selection         =   new WP_Query($args);
                    $counter                =   0;
                    $options['related_no']  =   4;
                    $show_compare           =   0;
                    $rental_module_status   =   esc_html ( get_option('wp_estate_enable_rental_module','') );
                    
                    while ($prop_selection->have_posts()): $prop_selection->the_post(); 
                        if( $rental_module_status == 'yes'){
                            include(locate_template('prop-listing-booking.php'));
                        }else{
                            include(locate_template('prop-listing.php'));
                        }

                    endwhile;  
               }else{
                   print '<h4>'.__('You don\'t have any favorite properties yet!','wpestate').'</h4>';
               }
                   
               
                 
               print '</div>';
               ?>            
            </div> <!-- end inside post-->
        </div>
        <!-- end content-->

       <?php //   include(locate_template('customsidebar.php')); ?>

    </div><!-- #main -->    
</div><!-- #wrapper -->
<?php get_footer(); ?>