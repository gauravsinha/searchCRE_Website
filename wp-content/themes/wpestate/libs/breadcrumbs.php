<?php
///////////////////////////////////////////////////////////////////////////////////////////
////// function to determine the sidebar orientation options
///////////////////////////////////////////////////////////////////////////////////////////

function sidebar_orientation($postid=''){
   
    if($postid!=''){       
        $sidebar_name   =  esc_html( get_post_meta($postid, 'sidebar_select', true) );
        $sidebar_status =  esc_html( get_post_meta($postid, 'sidebar_option', true) );
    }else{
        $sidebar_name   = esc_html( get_option('wp_estate_blog_sidebar_name', '') );
        $sidebar_status = esc_html( get_option('wp_estate_blog_sidebar', '') );
    }

    if($sidebar_name=='') $sidebar_name ='primary-widget-area';
    if($sidebar_status=='') $sidebar_status ='right';
    if ($sidebar_status == 'none')  $sidebar_status = '';
    
    
    $return_values['sidebar_name']=$sidebar_name;
    $return_values['sidebar_status']=$sidebar_status;
    $return_values['related_no']=3;
    $return_values['bread_align'] = "";
    $return_values['shadow'] = '';
    $return_values['add_back'] = '';
    $return_values['bread_align'] = '';
    $return_values['bread_align_internal'] = "";
    $return_values['footer_align'] = "";
    $return_values['grid'] = '';
    $return_values['side'] = '';
    $return_values['fullwhite'] = '';
    $return_values['full_breadcrumbs'] = '';
   
    if ($sidebar_status == 'left') {
        $return_values['shadow'] = 'shadowonright';
        $return_values['add_back'] = 'whiteonright';
        $return_values['bread_align'] = "goright";
        $return_values['bread_align_internal'] = "breadright-internal";
        $return_values['footer_align'] = "goright";
        $return_values['grid'] = "nine columns omega onright ";
        $return_values['side'] = "alpha";
        
    } else if ($sidebar_status == 'right') {
        $return_values['shadow'] = 'shadowonleft';
        $return_values['add_back'] = 'whiteonleft';
        $return_values['grid'] = "nine columns alpha";
        $return_values['side'] = "omega";       
    } else {
        $return_values['shadow']='noshadow';
        $return_values['grid'] = "twelve columns alpha";
        $return_values['fullwhite'] = 'fullwhite';
        $return_values['full_breadcrumbs'] = 'yes';
        $return_values['related_no'] = 4;
    }
    return $return_values;
}
// end sidebar orientation



///////////////////////////////////////////////////////////////////////////////////////////
////// function to display breadcrumbs according to sidebar orientation
///////////////////////////////////////////////////////////////////////////////////////////

function breadcrumb_container($full_breadcrumbs,$bread_align,$postid=''){
    global $post;
    
    if( is_search() ){
       print '  <div id="breadcrumbs_container">
                    <div class="breadcrumbs ">
                        <a href="'.home_url().'">'.__('Home','wpestate').'<span class="bread_arrows">&raquo;</span>'.__('Search Results','wpestate').'</a>
                    </div>
                </div>';
       return;
    }
        
    $category       =   get_the_term_list($postid, 'property_category', '', ', ', '');
    $custom_image   =   '';
    $rev_slider     =   '';
    if( isset($post->ID) ){
        $custom_image   =   esc_html( esc_html(get_post_meta($post->ID, 'page_custom_image', true)) );  
        $rev_slider     =   esc_html( esc_html(get_post_meta($post->ID, 'rev_slider', true)) );
    }
    $home_small_map_status      =   esc_html ( get_option('wp_estate_home_small_map','') );
   
    if ($category==''){
           $category=get_the_category_list(', ',$postid);
       }
    
    if ($full_breadcrumbs != 'yes') {
        $return_string='<div id="breadcrumbs_wrapper" class=" '. $bread_align.'"></div>';    
    } else {
       $return_string='
                 <div id="breadcrumbs_container">
                 <div class="breadcrumbs ">
                 <a href="'.home_url().'">'.__('Home','wpestate').'</a>';
                     if (is_archive()) {
                        if( is_category() || is_tax() ){
                           $return_string.= '<span class="bread_arrows">&raquo;</span><span class="bread_selected">'. single_cat_title('', false).'</span>';
                        } else{
                           $return_string.= '<span class="bread_arrows">&raquo;</span><span class="bread_selected">Archives</span>';
                        }
                    }else{
                        if( $category!=''){
                           $return_string.='<span class="bread_arrows">&raquo;</span>'.$category;
                        }
                        if(!is_front_page()){
                           $return_string.='<span class="bread_arrows">&raquo;</span><span class="bread_selected">'.get_the_title().'</span>';   
                        }
                    }
                  if(!is_front_page() && get_post_type()!='estate_property' || is_tax() || is_search() ){
                      if( $custom_image =='' && $rev_slider==''){
                        $return_string.='<div class="map_opener" id="openmap">'.__('enlarge map','wpestate').'</div>';
                      }
                  }
                  
                  if( is_front_page() && $home_small_map_status=='yes' ){
                      $return_string.='<div class="map_opener" id="openmap">'.__('enlarge map','wpestate').'</div>';
                  }
                  
                  $return_string.='</div>
              </div>';
    }
    return $return_string;
}




function display_breadcrumbs( $full_breadcrumbs,$bread_align_internal,$postid='' ){
        global $post;
       
        if( is_search() ){
           print '<div class="breadcrumbs-internal '.$bread_align_internal.'">
                  <a href="'.home_url().'">'.__('Home','wpestate').'<span class="bread_arrows">&raquo;</span>'.__('Search Results','wpestate').'</a></div>';
           return;
        }
        
        $category        =   get_the_term_list($postid, 'property_category', '', ', ', '');
        $custom_image   =   '';
        $rev_slider     =   '';
        if( isset($post->ID) ){
            $custom_image   =   esc_html( esc_html(get_post_meta($post->ID, 'page_custom_image', true)) );  
            $rev_slider     =   esc_html( esc_html(get_post_meta($post->ID, 'rev_slider', true)) );
        }
       $home_small_map_status      =   esc_html ( get_option('wp_estate_home_small_map','') );
   
       if ($category==''){
           $category=get_the_category_list(', ',$postid);
       }
       
       $return_string='';
       if ($full_breadcrumbs != 'yes') {
        $return_string='
                <div class="breadcrumbs-internal '.$bread_align_internal.'">
                    <a href="'.home_url().'">'.__('Home','wpestate').'</a>';
                    if (is_archive()) {
                        if( is_category() || is_tax() ){
                           $return_string.= '<span class="bread_arrows">&raquo;</span><span class="bread_selected">'. single_cat_title('', false).'</span>';
                        } else{
                           $return_string.= '<span class="bread_arrows">&raquo;</span><span class="bread_selected">Archives</span>';
                        }
                    }else{
                        if( $category!=''){
                           $return_string.='<span class="bread_arrows">&raquo;</span>'.$category;
                        }
                        if( !is_front_page() ){
                           $return_string.='<span class="bread_arrows">&raquo;</span><span class="bread_selected">'.get_the_title().'</span>';   
                        }
                    }
                    
                 if(!is_front_page()&& get_post_type()!='estate_property' || is_tax() || is_search() ){
                      if( $custom_image ==''&& $rev_slider==''){
                        $return_string.='<div class="map_opener" id="openmap">'.__('enlarge map','wpestate').'</div>';
                      }
                  }
                  
                  if( is_front_page() && $home_small_map_status=='yes' ){
                      $return_string.='<div class="map_opener" id="openmap">'.__('enlarge map','wpestate').'</div>';
                  }
                  
                 $return_string.='</div>';
    }
     return $return_string;
}


///////////////////////////////////////////////////////////////////////////////////////////
/////// Display footer breadcrumbs
///////////////////////////////////////////////////////////////////////////////////////////

function display_footer_breadcrumbs($postid=''){
    
        if( is_search() ){
          print ' <a href="'.home_url().'">'.__('Home','wpestate').'<span class="bread_arrows">&raquo;</span>'.__('Search Results','wpestate').'</a>';
          return;
        }
    
        $category='';
        $return_string='';
        $category=get_the_term_list($postid, 'property_category', '', ', ', '');
        if ($category==''){
           $category=get_the_category_list(', ',$postid);
        }
       
        $return_string='<a href="'.home_url().'">'.__('Home','wpestate').'</a>';
        if (is_archive()) {
            if( is_category() || is_tax() ){
               $return_string.= '<span class="bread_arrows">&raquo;</span><span class="bread_selected">'. single_cat_title('', false).'</span>';
            } else{
               $return_string.= '<span class="bread_arrows">&raquo;</span><span class="bread_selected">Archives</span>';
            }
        }else{
            if( $category!=''){
               $return_string.='<span class="bread_arrows">&raquo;</span>'.$category;
            }
            if(!is_front_page()){
               $return_string.='<span class="bread_arrows">&raquo;</span><span class="bread_selected">'.get_the_title().'</span>';   
            }
        }
       return $return_string;
}

?>
