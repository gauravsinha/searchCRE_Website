<?php
if ($color_scheme == 'yes') {

    $background_color               = esc_html( get_option('wp_estate_background_color', '') );
    $content_back_color             = esc_html( get_option('wp_estate_content_back_color', '') );
    $header_color                   = esc_html( get_option('wp_estate_header_color', '') );
    $breadcrumbs_back_color         = esc_html(get_option('wp_estate_breadcrumbs_back_color', '') );
    $breadcrumbs_font_color         = esc_html(get_option('wp_estate_breadcrumbs_font_color', '') );
    $font_color                     = esc_html(get_option('wp_estate_font_color', '') );
    $link_color                     = esc_html(get_option('wp_estate_link_color', '') );
    $headings_color                 = esc_html(get_option('wp_estate_headings_color', '') );
    $comments_font_color            = esc_html(get_option('wp_estate_comments_font_color', '') );
    $coment_back_color              = esc_html(get_option('wp_estate_coment_back_color', '') );
    $footer_back_color              = esc_html(get_option('wp_estate_footer_back_color', '') );
    $footer_font_color              = esc_html(get_option('wp_estate_footer_font_color', '') );
    $footer_copy_color              = esc_html(get_option('wp_estate_footer_copy_color', '') );
    $sidebar_widget_color           = esc_html(get_option('wp_estate_sidebar_widget_color', '') );
    $sidebar_heading_color          = esc_html ( get_option('wp_estate_sidebar_heading_color','') );
    $sidebar_heading_boxed_color    = esc_html ( get_option('wp_estate_sidebar_heading_boxed_color','') );
    $menu_font_color                = esc_html(get_option('wp_estate_menu_font_color', '') );
    $menu_hover_back_color          = esc_html(get_option('wp_estate_menu_hover_back_color', '') );
    $menu_hover_font_color          = esc_html(get_option('wp_estate_menu_hover_font_color', '') );
    $agent_color                    = esc_html(get_option('wp_estate_agent_color', '') );
    $listings_color                 = esc_html(get_option('wp_estate_listings_color', '') );
    $blog_color                     = esc_html(get_option('wp_estate_blog_color', '') );
    $dotted_line                    = esc_html(get_option('wp_estate_dotted_line', '') );
    $sidebar2_font_color            = esc_html(get_option('wp_estate_sidebar2_font_color', '') );
    $footer_top_band                = esc_html ( get_option('wp_estate_footer_top_band','') );
    $top_bar_back                   = esc_html ( get_option('wp_estate_top_bar_back','') );
    $top_bar_font                   = esc_html ( get_option('wp_estate_top_bar_font','') );
    $adv_search_back_color          = esc_html ( get_option('wp_estate_adv_search_back_color ','') );
    $adv_search_font_color          = esc_html ( get_option('wp_estate_adv_search_font_color','') );

   
/// Custom Colors
///////////////////////////////////////////////////////////////////////////////////////////////////////////

    
if ($background_color != '') {
print'#page {background-color: #' . $background_color . ';}';        

} // end $background_color

if ($content_back_color != '') {
print '.whiteonleft,.shadowonleft,.shadowonright,.fullwhite,.noshadow,.whiteonright,.compare_item:nth-child(odd),.dottedline,.dottedlineblog{background-color: #' . $content_back_color . ';}
.featured_article:before, .bottom-post:before,.bottom-estate_property:before, .blog_bottom_border:before{ background-color: #' . $content_back_color . ';}';
}// end $content_back_color

if ($header_color != '') {
print' #branding,#access ul ul,.header_control,.geolocation-button{background-color: #' . $header_color . ' }'; 
} // end $header_color

if ($breadcrumbs_back_color != '') {
print' #breadcrumbs_wrapper,.breadcrumbs-internal,.footer_breadcrumbs_insider ,.footer_breadcrumbs, .breadcrumbs, #breadcrumbs_container{background-color: #'. $breadcrumbs_back_color . ';}';
} // end $breadcrumbs_back_color #f5f5f5

if ($breadcrumbs_font_color != '') {
print'
.breadcrumbs-internal a,.breadcrumbs a,.footer_breadcrumbs_insider a,.bread_arrows, #openmap{color: #' . $breadcrumbs_font_color . ';}        
.bread_selected{color: #' . $breadcrumbs_font_color . '!important;}';
} // end $breadcrumbs_font_color #a3a3a3

if ($font_color != '') {
print'p,column, .columns,.testimonial-container p, .article_property_type a, .article_property_type, .propcol,.recent_post_p,.pagination a ,#new_post label{ color: #' . $font_color . '!important;}';
} // end $font_color #a0a5a8

if ($link_color != '') {
print' a ,.agent_listing_link a,.my_other a, .blog_category a{color: #' . $link_color . ';}';
} // end $link_color

if ($headings_color != '') {
print'h1, h2, h3, h4, h5, h6,.agent_listing_prop_details h2 a,.featured_property h2 a,.testimonial-container h3, .featured_article h2 a,.featured_agent_details h2 a,.featured_agent h2,.iconcol h3 a,.agent_listing_details h2 a,.widget-area-sidebar h3, .blog_listing h2 a, .related_posts h4 a,  .agent_listing_prop_details h3 a,.related_listings h3 a,.agent_listing_details h3,.article_container h3 a,.featured_article h1 a,.featured_property h1 a,.agent_listing_details h3 a,.compare_item h3 a, .listing_title,.listing_title a{color: #' . $headings_color . ';}';
} // end $headings_color #1a171b

if ($comments_font_color != '') {
print' #respond p,.agent_detail,.agent_title,.testimonial-container p,.testimonial-author, .featured_secondline,.agent_content p,.agent_content,.agent_listing_details,.contact_detail,.featured_article_secondline{color: #' . $comments_font_color . ';}';
} // end $comments_font_color

if ($coment_back_color != '') {
print'.agent_listing, #respond,.featured_agent,.featured_article_title,.testimonial-container,.featured_property,.featured_article_secondline,.agent_listing-prop,.agent_bottom_border:before,.dottedline-agent{background-color: #'.$coment_back_color .';}';
} // end $coment_back_color

if ($footer_back_color != '') {
print'#colophon { background-color: #' . $footer_back_color . ';}';
} // end $footer_back_color


if ($footer_font_color != '') {
print'#colophon p, #colophon .textwidget, .widget-area a, .widget-title-footer,#colophon .contact_sidebar p, .widget_latest_price, .widget_latest_internal .listing_name{color:#'. $footer_font_color . '!important;}';
} // end $footer_font_color

if ($footer_copy_color != '') {
print'#site-generator {color: #' . $footer_copy_color . ';}';
} // end $footer_copy_color


if ($sidebar_widget_color != '') {
print'.mortgage_calculator_div ,.widget_search,.featured_sidebar_intern,.widget_search input[type=text],
input[type=password],input[type=email],input[type=url],input[type=number],#submit-form,
.advanced_search_sidebar,.zillow_widget,.loginwd_sidebar{background-color: #' . $sidebar_widget_color . ';}
.dsidx-widget-search, .dsidx-widget-search table,.dsidx-widget-search .dsidx-widget table tbody tr:nth-child(even),.dsidx-widget-listings,.dsidx-widget-single-listing{background-color: #' . $sidebar_widget_color . '!important;}';
} // end $sidebar_widget_color


if($sidebar_heading_color!=''){
    print '.widget-area-sidebar .widget-title-sidebar  {color:#'.$sidebar_heading_color.';}';
}



if($sidebar_heading_boxed_color!=''){
   print '.featured_sidebar .featured_title a,.widget_search input[type=text],.mortgage_calculator_div h2 {color:#'.$sidebar_heading_boxed_color.';}
    .advanced_search_sidebar h3,.zillow_widget h3,.dsidx-widget-listings h3,.dsidx-widget-search h3,.dsidx-widget-single-listing .widget-title a{color:#'.$sidebar_heading_boxed_color.'!important;}'; 
}

if ($sidebar2_font_color != '') {
print'#primary .widget-container, #primary .widget-container a:not(.featured_title_link),#primary .contact_sidebar p,#primary .featured_second_line,#primary .dsidx-widget table label,#primary .mortgage_calculator_div label,#primary .dsidx-widget-single-listing-detail,#primary .dsidx-widget-single-listing-price,#primary .dsidx-widget-single-listing-detail-description,.loginwd_sidebar{color: #' . $sidebar2_font_color . '!important;} ';
} // end $sidebar2_font_color



if ($menu_font_color != '') {
print' #access a,#access ul ul a{ color: #' . $menu_font_color . ';}';
} // end $menu_font_color

if ($menu_hover_back_color != '') {
print'.sub-menu li:hover {background-color: #' . $menu_hover_back_color . ';}
#access ul ul a,#access ul ul :hover > a {border-bottom: 1px solid #' . $menu_hover_back_color . ';}
#access .current-menu-item >a,#access .menu li:hover>a, #access .menu li:hover>a:active, #access .menu li:hover>a:focus,#access .current-menu-item >a, #access .current-menu-parent>a, #access .current-menu-ancestor>a{color: #' . $menu_hover_back_color . ';} 
.sub-menu { border-top: 1px solid #' . $menu_hover_back_color . '; }  ';
} // end $menu_hover_back_color

if ($menu_hover_font_color != '') {
print'.sub-menu li:hover, #access ul ul :hover > a {color: #' . $menu_hover_font_color . ';}';
} // end $menu_hover_font_color

if ($agent_color != '') {
print'.agentborder,.agent_listing-prop{border-right: 10px solid #' . $agent_color . ';}
.shadowonright.agentborder{ border-left: 10px solid #' . $agent_color . ';}
.agentborder a:hover, .agent_listing-prop a:hover,.agent_listing-prop h3 a:hover{ color:#'.$agent_color.';}
.agent_bottom_border:after{ border-top: 1px dashed #'.$agent_color.';}
.featured_agent_image_hover{background-color: #'.$agent_color.';border: 10px solid #'.$agent_color.';}  
.agentbut:hover,.user_tab_menu .user_tab_active,.tooglesubmit,#google_capture, #form_submit_1, #update_profile, #change_pass ,.icon-fav-on-remove,#google_capture:hover, #form_submit_1:hover, #update_profile:hover, #change_pass:hover,.submit_container_header {background-color: #'.$agent_color.';}
 .submitcontent,.submit_container{border: 1px solid #'.$agent_color.';}
 .user_tab_menu{border-bottom: 1px solid  #'.$agent_color.';}  ';
} // end $agent_color

if ($listings_color != '') {
print'.listingborder { border-right: 10px solid #' . $listings_color . ';}
.shadowonright.listingborder{ border-left: 10px solid #' . $listings_color . ';      }        
.sixprop h4,.under-title,.property_price,.listingborder a:hover{color:#'.$listings_color.';}
    .compare-action:hover{color:#'.$listings_color.'!important;}
.bottom-estate_property:after{border-top: 1px dashed #'.$listings_color.';}
#post figcaption,.widget_latest_internal figcaption {background-color:#'.$listings_color.';border: 10px solid #'.$listings_color.';} 
.listingbut:hover {background-color:#'.$listings_color.';}    
';

} // end $listings_color


if ($blog_color != '') {
print'.blogborder{border-right: 10px solid #'.$blog_color.';}      
.shadowonright.blogborder{border-left: 10px solid #' . $blog_color . ';}
.blogborder a:hover,.blogborder h3 a:hover{color:#'.$blog_color.';}
.current-img img{border:3px solid #'.$blog_color.';}
.blog_bottom_border:after,.featured_article:after{ border-top: 1px dashed #'.$blog_color.';}
.figcaption-post {background: #'.$blog_color.'!important;border: 10px solid #'.$blog_color.'!important;}   
.listingblog:hover{background-color: #'.$blog_color.';}
';
} // end $blog_color


if ($dotted_line != '') {
print'.dottedline,.dottedlineblog,.inside_post,.dasboard-prop-listing-line { border-bottom: 1px dashed #' . $dotted_line . ';}        
.blog_listing,.agent_listing-prop,.related_listings,.agent_listing,.listing_filters,.prop-compare,.featured_agent,.article_container,.testimonial-container,.boder_icon,.featured_article,.comment,.related_posts{border-bottom: 1px dashed #' . $dotted_line . ';}
.dottedline-agent{border-top:1px dashed #' . $dotted_line . ';}';
} // end $dotted_line

if($footer_top_band!=''){
 print'
     .footer_band{background-color:#'.$footer_top_band.';}';
}

if($top_bar_back!=''){
    print'.top-user-menu-wrapper{background-color: #'.$top_bar_back.';}';
}    

if($top_bar_font!=''){
    print'.top-user-menu-wrapper,.top-user-menu-wrapper a{color:#'.$top_bar_font.';}';
}
 
if ($adv_search_back_color != '') {
    print'.adv_extra_options,#adv-search-header-4,#adv-search-header-3,#adv-search-header-contact-3,#adv-search-header-2 {background-color: #' . $adv_search_back_color . ';}';        
} 

if ($adv_search_font_color != '') {
    print'.adv_extra_options,#adv-search-header-4,#adv-search-header-3,#adv-search-header-contact-3,#adv-search-header-2 {color: #' . $adv_search_font_color . ';}';        
} 


///////////////////////////////////////////////////////////////////////////////////////////////////////////
// End colors

    
} //////////// end if color scheme
/////// Custom css


?>


