<?php

/////////////////////////////////////////////////////////////////////////////////
///show booking reservation form
////////////////////////////////////////////////////////////////////////////////

if (!function_exists('show_booking_reservation')){
    function show_booking_reservation(){
        if ( !is_user_logged_in() ){
            $front_end_login                =   esc_html( get_option('wp_estate_front_end_login','') );  
            print '<div class="book_warn"><strong><a href="'.$front_end_login.'">'.__('Login','wpestate').'</a>'.__(' to message the owner or to book this property!','wpestate').'</strong></div>';
            return;
        }
        
        global $post;
        global $current_user;
        get_currentuserinfo();
        $userID                         =   $current_user->ID;
        $user_login                     =   $current_user->user_login;
        $post_id    =   $post->ID;
        $prop_id    =   $post->ID;  

   //    $reservation_array = get_booking_dates($prop_id);
 //      print_r($reservation_array);
        print '<div class="booking_reservation_form">';
        
        print '<div class="booking_action_tabs">
                    
                    <div class="booking_action " id="book_contact_owner">'.__('Contact Owner','wpestate').'</div>
               </div>';
        
        print '<div class="booking_form_request" id="booking_form_request">
                    <div id="booking_form_request_mess"></div>
                    <p class="half_form">
                        <label for="booking_from_date">'.__('From Date','wpestate').'</label>
                        <input type="text" id="booking_from_date" size="40" name="booking_from_date" value="">
                    </p>
                    <p class="half_form rightinput">
                        <label for="booking_to_date">'. __('To Date','wpestate').'</label>
                        <input type="text" id="booking_to_date" size="40" name="booking_to_date" value="">
                    </p>
                        
                    <p class="full_form">';
        
                        $max_guest = get_post_meta($post_id,'guest_no',true);
                        print '
                        <label for="booking_guest_no">'. __('Guests','wpestate').' ('.__('Max guests no: ','wpestate').$max_guest.')</label>
                        <select id="booking_guest_no"  name="booking_guest_no"  class="cd-select" >
                            <option value="1">1 '.__('Guest','wpestate').'</option>';
                            for ($i = 2; $i <= $max_guest; $i++) {
                                print '<option value="'.$i.'">'.$i.' '.__('Guests','wpestate').'</option>';
                            }
                    print'
                        </select>    
                    </p>

                    
                    
                    <input type="hidden" id="property_id" name="property_id" value="'.$post_id.'" />
                    
                    
                    <p class="full_form comentp" id="add_costs_here">
                       <label for="event_description">'.__('Comments','wpestate').'</label>
                       <textarea id="event_description" tabindex="3" name="event_description" cols="50" rows="6"></textarea>
                    </p>
                     <button type="submit" id="submit_booking_front" class="btn vernil small">'.__('Send Request','wpestate').'</button>
                    '. wp_nonce_field( 'booking_ajax_nonce', 'security-register-booking_front' ).'
               </div>';
        
                print '<div class="booking_form_contact" id="booking_form_contact">';
                
                $agent_id       = get_user_meta( $post->post_author,'user_agent_id',true);
                $thumb_id       = get_post_thumbnail_id($agent_id);
                $preview_agent  = wp_get_attachment_image_src($thumb_id, 'agent_picture_thumb');
            
                $agent_skype    = esc_html( get_post_meta($agent_id, 'agent_skype', true) );
                $agent_phone    = esc_html( get_post_meta($agent_id, 'agent_phone', true) );
                $agent_mobile   = esc_html( get_post_meta($agent_id, 'agent_mobile', true) );
                $agent_email    = esc_html( get_post_meta($agent_id, 'agent_email', true) );
                $agent_pitch    = esc_html( get_post_meta($agent_id, 'agent_pitch', true) );
                $agent_posit    = esc_html( get_post_meta($agent_id, 'agent_position', true) );
                $link           = get_permalink($agent_id);
                $name           = get_the_title($agent_id);

                $did_he_book    = user_booked_this($userID,$prop_id);
                $contact_class  = '';
                
                if($did_he_book){
                }else{
                //    $contact_class="no_name_contact";
                }
                    if( $preview_agent[0]==''){
                        $user_custom_picture=get_template_directory_uri().'/images/default-user.png';
                    }else{
                        $user_custom_picture=$preview_agent[0];
                    }
                    print '
                    <div class="featured_agent_image" data-agentlink="'. $link .'" style="background-image: url(\'' . $user_custom_picture . '\');">
                        <div class="featured_agent_image_hover">
                           <span>'.$name.'</span>
                        </div> 
                    </div>';
                
                
                print '
                   <div class="agent_listing_prop_details '.$contact_class.' " id="prop_inq">
                       <h3><a href="' . $link . '">' .  $name  . '</a></h3>

                       <div class="agent_title">'.$agent_posit .'</div>';
                       if( $did_he_book ){
                            if ($agent_phone) {
                               print '<div class="agent_detail"><a href="tel:' . $agent_phone . '">'.$agent_phone.'</a></div>';
                            }
                            if ($agent_mobile) {
                                print '<div class="agent_detail"><a href="tel:'.$agent_mobile.'">'.$agent_mobile.'</a></div>';
                            }

                            if ($agent_email) {
                                print '<div class="agent_detail"><a href="mailto:' . $agent_email . '">' . $agent_email . '</a></div>';
                            }

                            if ($agent_skype) {
                                print '<div class="agent_detail">'.__('Skype','wpestate').': ' . $agent_skype . '</div>';
                            }
                       }else{
                           _e('Detailed contact information will be available after a booking is confirmed!','wpestate');
                       }
                
                       print '<input name="agent_id" type="hidden"  id="agent_id" value="'.$agent_id.'">';
                       print '<input name="prop_id" type="hidden"  id="agent_property_id" value="'.$prop_id.'">';
                       print'
                      
                      
                </div>';

                print '
                    <p class="full_form" id="booking_contact"></p>

                     <p class="full_form">
                        <label for="booking_mes_subject">'.__('Subject','wpestate').'</label>
                        <input type="text" id="booking_mes_subject" size="40" name="booking_mes_subject" value="" style="width:246px;display:block">
                    </p>
                    
                    <p class="full_form">
                       <label for="booking_mes_mess">'.__('Message','wpestate').'</label>
                       <textarea id="booking_mes_mess" tabindex="3" name="booking_mes_mess" cols="50" rows="6"></textarea>
                    </p>
                     <button type="submit" id="submit_mess_front" class="btn vernil small">'.__('Send Message','wpestate').'</button>
                    '. wp_nonce_field( 'mess_ajax_nonce_front', 'security-register-mess-front' ).'
                

               </div>';
        
        
        print '</div>';
        
        
    }
}



/////////////////////////////////////////////////////////////////////////////////
/// check user vs property status
////////////////////////////////////////////////////////////////////////////////

function user_booked_this($userid,$propid){
    $all_my_post=array();
    $args = array(
          'post_type'         => 'wpestate_booking',
          'post_status'       => 'publish',
          'posts_per_page'    => -1,
          'author'           =>  $userid ,
          'meta_query' => array(
              array(
                      'key' => 'booking_id',
                      'value' => $propid,
                      'compare' => '='
              ),
              array(
                      'key'     => 'booking_status',
                      'value'   => 'confirmed',
                      'compare' => '='
              )
      )


      );
      $prop_selection = new WP_Query($args);
      if ($prop_selection->have_posts()){
          return true;
      }else{
          return false;
      }
    
}



/////////////////////////////////////////////////////////////////////////////////
/// check user vs agent id 
////////////////////////////////////////////////////////////////////////////////

function user_booked_from_agent($userid,$agent_id){
  
    $all_my_post=array();
    $args = array(
          'post_type'         => 'wpestate_booking',
          'post_status'       => 'publish',
          'posts_per_page'    => -1,
          'author'           =>  $userid ,
          'meta_query' => array(
                array(
                      'key'     => 'booking_status',
                      'value'   => 'confirmed',
                      'compare' => '='
                )
      )


      );

      $prop_selection = new WP_Query($args);
    // print_r($prop_selection);
      if ($prop_selection->have_posts()){
            while ($prop_selection->have_posts()): $prop_selection->the_post(); 
              
                $prop_id = intval  ( get_post_meta(get_the_ID(), 'booking_id', true) );
               // print'check for '.$prop_id.'</br>';
                if( wpse119881_get_author ($prop_id) === $agent_id  ){
                    return true;
                }
            
            endwhile; // end of the loop.    
            return false;
      }else{
          return false;
      }
    
}


/////////////////////////////////////////////////////////////////////////////////
/// show booking calendar
////////////////////////////////////////////////////////////////////////////////





if( !function_exists("show_booking_calendar")):
    function show_booking_calendar(){
        global $post;
        $post_id=$post->ID;
        print '<div class="all-calendars">
        <div id="calendar-prev"><i class="fa fa-angle-left"></i></div>
        <div id="calendar-next"><i class="fa fa-angle-right"></i></div> 
        ';
       // $reservation_array = get_booking_dates($post_id);
        $reservation_array  = get_post_meta($post_id, 'booking_dates',true  ); 
        
        if(!is_array($reservation_array)){
            $reservation_array=array();
        }
      //  print_r($reservation_array);
        get_calendar_custom2 ($reservation_array,true,true);
        print '</div>';
        print '<div class="calendar-legend">
                <div class="legend-calendar-today"></div><span>'.__('Today','wpestate').'</span>
                <div class="legend-calendar-booked"></div><span>'.__('Dates Booked','wpestate').'</span>
              </div>';
    }
endif;      





/////////////////////////////////////////////////////////////////////////////////
/// get calendar reservation dates
////////////////////////////////////////////////////////////////////////////////




function get_calendar_custom2($reservation_array,$initial = true, $echo = true) {
	global $wpdb, $m, $monthnum, $year, $wp_locale, $posts;
        $daywithpost =array();
	


	// week_begins = 0 stands for Sunday
	
        
     $time_now  = current_time('timestamp');
     $now=date('Y-m-d');
     $date = new DateTime();
    
        $thismonth = gmdate('m', $time_now);
        $thisyear  = gmdate('Y', $time_now);
                
	$unixmonth = mktime(0, 0 , 0, $thismonth, 1, $thisyear);
	$last_day = date('t', $unixmonth);

	$month_no=1;
        while ($month_no<12){
            
            draw_month($month_no,$reservation_array, $unixmonth, $daywithpost,$thismonth,$thisyear,$last_day);
            
            $date->modify( 'first day of next month' );
            $thismonth=$date->format( 'm' );
            $thisyear  = $date->format( 'Y' );
            $unixmonth = mktime(0, 0 , 0, $thismonth, 1, $thisyear);
            $month_no++;
        }
       
	
	

}




function    draw_month($month_no,$reservation_array, $unixmonth, $daywithpost,$thismonth,$thisyear,$last_day){
        global $wpdb, $m, $monthnum, $year, $wp_locale, $posts;
        $week_begins = intval(get_option('start_of_week'));
        
        $initial=true;
        $echo=true;
        
        $table_style='';
        if( $month_no>2 ){
               $table_style='style="display:none;"';
        }
        
        $calendar_output = '<div class="booking-calendar-wrapper" data-mno="'.$month_no.'" '.$table_style.'><table class="wp-calendar booking-calendar">
	<caption> '.$thismonth.'/'.$thisyear.' </caption>
	<thead>
	<tr>';

	$myweek = array();

	for ( $wdcount=0; $wdcount<=6; $wdcount++ ) {
		$myweek[] = $wp_locale->get_weekday(($wdcount+$week_begins)%7);
	}

	foreach ( $myweek as $wd ) {
		$day_name = (true == $initial) ? $wp_locale->get_weekday_initial($wd) : $wp_locale->get_weekday_abbrev($wd);
		$wd = esc_attr($wd);
		$calendar_output .= "\n\t\t<th scope=\"col\" title=\"$wd\">$day_name</th>";
	}

	$calendar_output .= '
	</tr>
	</thead>

	<tfoot>
	<tr>';


      //  $calendar_output .= "\n\t\t".'<td colspan="3" id="prev" class="pad">&nbsp;</td>';
	//$calendar_output .= "\n\t\t".'<td class="pad">&nbsp;</td>';
     //   $calendar_output .= "\n\t\t".'<td colspan="3" id="next" class="pad">&nbsp;</td>';
	$calendar_output .= '
	</tr>
	</tfoot>
        <tbody>
	<tr>';

        
        
        

	
	// See how much we should pad in the beginning
	$pad = calendar_week_mod(date('w', $unixmonth)-$week_begins);
	if ( 0 != $pad )
		$calendar_output .= "\n\t\t".'<td colspan="'. esc_attr($pad) .'" class="pad">&nbsp;</td>';

	$daysinmonth = intval(date('t', $unixmonth));
	for ( $day = 1; $day <= $daysinmonth; ++$day ) {
                $timestamp = strtotime( $day.'-'.$thismonth.'-'.$thisyear).' | ';
		if ( isset($newrow) && $newrow ){
                    $calendar_output .= "\n\t</tr>\n\t<tr>\n\t\t";
                }
                
		$newrow = false;

		if ( $day == gmdate('j', current_time('timestamp')) && $thismonth == gmdate('m', current_time('timestamp')) && $thisyear == gmdate('Y', current_time('timestamp')) ){
                    $calendar_output .= '<td class="calendar-today">';
                }
		else if(in_array ($timestamp,$reservation_array) ){
                    $calendar_output .= '<td class="calendar-reserved">';
                }
                else{
                    $calendar_output .= '<td class="calendar-free">';
                }
                
                
                
                
                
                
		if ( in_array($day, $daywithpost) ) // any posts today?
				$calendar_output .= '<a href="' . get_day_link( $thisyear, $thismonth, $day ) . '" title="' . esc_attr( $ak_titles_for_day[ $day ] ) . "\">$day</a>";
		else
			$calendar_output .= $day;
		$calendar_output .= '</td>';

		if ( 6 == calendar_week_mod(date('w', mktime(0, 0 , 0, $thismonth, $day, $thisyear))-$week_begins) )
			$newrow = true;
	}

	$pad = 7 - calendar_week_mod(date('w', mktime(0, 0 , 0, $thismonth, $day, $thisyear))-$week_begins);
	if ( $pad != 0 && $pad != 7 )
		$calendar_output .= "\n\t\t".'<td class="pad" colspan="'. esc_attr($pad) .'">&nbsp;</td>';

	$calendar_output .= "\n\t</tr>\n\t</tbody>\n\t</table></div>";

	

	if ( $echo )
		echo apply_filters( 'get_calendar',  $calendar_output );
	else
		return apply_filters( 'get_calendar',  $calendar_output );
}














/////////////////////////////////////////////////////////////////////////////////
/// display booking calendar
////////////////////////////////////////////////////////////////////////////////



function get_calendar_custom($initial = true, $echo = true) {
	global $wpdb, $m, $monthnum, $year, $wp_locale, $posts;

	$cache = array();
	$key = md5( $m . $monthnum . $year );
	if ( $cache = wp_cache_get( 'get_calendar', 'calendar' ) ) {
		if ( is_array($cache) && isset( $cache[ $key ] ) ) {
			if ( $echo ) {
				echo apply_filters( 'get_calendar',  $cache[$key] );
				return;
			} else {
				return apply_filters( 'get_calendar',  $cache[$key] );
			}
		}
	}

	if ( !is_array($cache) )
		$cache = array();

	// Quick check. If we have no posts at all, abort!
	if ( !$posts ) {
		$gotsome = $wpdb->get_var("SELECT 1 as test FROM $wpdb->posts WHERE post_type = 'wpestate_booking' AND post_status = 'publish' LIMIT 1");
		if ( !$gotsome ) {
			$cache[ $key ] = '';
			wp_cache_set( 'get_calendar', $cache, 'calendar' );
			return;
		}
	}

	if ( isset($_GET['w']) )
		$w = ''.intval($_GET['w']);

	// week_begins = 0 stands for Sunday
	$week_begins = intval(get_option('start_of_week'));

	// Let's figure out when we are
	if ( !empty($monthnum) && !empty($year) ) {
		$thismonth = ''.zeroise(intval($monthnum), 2);
		$thisyear = ''.intval($year);
	} elseif ( !empty($w) ) {
		// We need to get the month from MySQL
		$thisyear = ''.intval(substr($m, 0, 4));
		$d = (($w - 1) * 7) + 6; //it seems MySQL's weeks disagree with PHP's
		$thismonth = $wpdb->get_var("SELECT DATE_FORMAT((DATE_ADD('{$thisyear}0101', INTERVAL $d DAY) ), '%m')");
	} elseif ( !empty($m) ) {
		$thisyear = ''.intval(substr($m, 0, 4));
		if ( strlen($m) < 6 )
				$thismonth = '01';
		else
				$thismonth = ''.zeroise(intval(substr($m, 4, 2)), 2);
	} else {
		$thisyear = gmdate('Y', current_time('timestamp'));
		$thismonth = gmdate('m', current_time('timestamp'));
	}

	$unixmonth = mktime(0, 0 , 0, $thismonth, 1, $thisyear);
	$last_day = date('t', $unixmonth);

	// Get the next and previous month and year with at least one post
	$previous = $wpdb->get_row("SELECT MONTH(post_date) AS month, YEAR(post_date) AS year
		FROM $wpdb->posts
		WHERE post_date < '$thisyear-$thismonth-01'
		AND post_type = 'post' AND post_status = 'publish'
			ORDER BY post_date DESC
			LIMIT 1");
	$next = $wpdb->get_row("SELECT MONTH(post_date) AS month, YEAR(post_date) AS year
		FROM $wpdb->posts
		WHERE post_date > '$thisyear-$thismonth-{$last_day} 23:59:59'
		AND post_type = 'post' AND post_status = 'publish'
			ORDER BY post_date ASC
			LIMIT 1");

	/* translators: Calendar caption: 1: month name, 2: 4-digit year */
	$calendar_caption = _x('%1$s %2$s', 'calendar caption');
	$calendar_output = '<table id="wp-calendar">
	<caption>' . sprintf($calendar_caption, $wp_locale->get_month($thismonth), date('Y', $unixmonth)) . '</caption>
	<thead>
	<tr>';

	$myweek = array();

	for ( $wdcount=0; $wdcount<=6; $wdcount++ ) {
		$myweek[] = $wp_locale->get_weekday(($wdcount+$week_begins)%7);
	}

	foreach ( $myweek as $wd ) {
		$day_name = (true == $initial) ? $wp_locale->get_weekday_initial($wd) : $wp_locale->get_weekday_abbrev($wd);
		$wd = esc_attr($wd);
		$calendar_output .= "\n\t\t<th scope=\"col\" title=\"$wd\">$day_name</th>";
	}

	$calendar_output .= '
	</tr>
	</thead>

	<tfoot>
	<tr>';

	if ( $previous ) {
		$calendar_output .= "\n\t\t".'<td colspan="3" id="prev"><a href="' . get_month_link($previous->year, $previous->month) . '" title="' . esc_attr( sprintf(__('View posts for %1$s %2$s'), $wp_locale->get_month($previous->month), date('Y', mktime(0, 0 , 0, $previous->month, 1, $previous->year)))) . '">&laquo; ' . $wp_locale->get_month_abbrev($wp_locale->get_month($previous->month)) . '</a></td>';
	} else {
		$calendar_output .= "\n\t\t".'<td colspan="3" id="prev" class="pad">&nbsp;</td>';
	}

	$calendar_output .= "\n\t\t".'<td class="pad">&nbsp;</td>';

	if ( $next ) {
		$calendar_output .= "\n\t\t".'<td colspan="3" id="next"><a href="' . get_month_link($next->year, $next->month) . '" title="' . esc_attr( sprintf(__('View posts for %1$s %2$s'), $wp_locale->get_month($next->month), date('Y', mktime(0, 0 , 0, $next->month, 1, $next->year))) ) . '">' . $wp_locale->get_month_abbrev($wp_locale->get_month($next->month)) . ' &raquo;</a></td>';
	} else {
		$calendar_output .= "\n\t\t".'<td colspan="3" id="next" class="pad">&nbsp;</td>';
	}

	$calendar_output .= '
	</tr>
	</tfoot>

	<tbody>
	<tr>';

	// Get days with posts
	$dayswithposts = $wpdb->get_results("SELECT DISTINCT DAYOFMONTH(post_date)
		FROM $wpdb->posts WHERE post_date >= '{$thisyear}-{$thismonth}-01 00:00:00'
		AND post_type = 'post' AND post_status = 'publish'
		AND post_date <= '{$thisyear}-{$thismonth}-{$last_day} 23:59:59'", ARRAY_N);
	if ( $dayswithposts ) {
		foreach ( (array) $dayswithposts as $daywith ) {
			$daywithpost[] = $daywith[0];
		}
	} else {
		$daywithpost = array();
	}

	if (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false || stripos($_SERVER['HTTP_USER_AGENT'], 'camino') !== false || stripos($_SERVER['HTTP_USER_AGENT'], 'safari') !== false)
		$ak_title_separator = "\n";
	else
		$ak_title_separator = ', ';

	$ak_titles_for_day = array();
	$ak_post_titles = $wpdb->get_results("SELECT ID, post_title, DAYOFMONTH(post_date) as dom "
		."FROM $wpdb->posts "
		."WHERE post_date >= '{$thisyear}-{$thismonth}-01 00:00:00' "
		."AND post_date <= '{$thisyear}-{$thismonth}-{$last_day} 23:59:59' "
		."AND post_type = 'post' AND post_status = 'publish'"
	);
	if ( $ak_post_titles ) {
		foreach ( (array) $ak_post_titles as $ak_post_title ) {

				/** This filter is documented in wp-includes/post-template.php */
				$post_title = esc_attr( apply_filters( 'the_title', $ak_post_title->post_title, $ak_post_title->ID ) );

				if ( empty($ak_titles_for_day['day_'.$ak_post_title->dom]) )
					$ak_titles_for_day['day_'.$ak_post_title->dom] = '';
				if ( empty($ak_titles_for_day["$ak_post_title->dom"]) ) // first one
					$ak_titles_for_day["$ak_post_title->dom"] = $post_title;
				else
					$ak_titles_for_day["$ak_post_title->dom"] .= $ak_title_separator . $post_title;
		}
	}

	// See how much we should pad in the beginning
	$pad = calendar_week_mod(date('w', $unixmonth)-$week_begins);
	if ( 0 != $pad )
		$calendar_output .= "\n\t\t".'<td colspan="'. esc_attr($pad) .'" class="pad">&nbsp;</td>';

	$daysinmonth = intval(date('t', $unixmonth));
	for ( $day = 1; $day <= $daysinmonth; ++$day ) {
		if ( isset($newrow) && $newrow )
			$calendar_output .= "\n\t</tr>\n\t<tr>\n\t\t";
		$newrow = false;

		if ( $day == gmdate('j', current_time('timestamp')) && $thismonth == gmdate('m', current_time('timestamp')) && $thisyear == gmdate('Y', current_time('timestamp')) )
			$calendar_output .= '<td id="today">';
		else
			$calendar_output .= '<td>';

		if ( in_array($day, $daywithpost) ) // any posts today?
				$calendar_output .= '<a href="' . get_day_link( $thisyear, $thismonth, $day ) . '" title="' . esc_attr( $ak_titles_for_day[ $day ] ) . "\">$day</a>";
		else
			$calendar_output .= $day;
		$calendar_output .= '</td>';

		if ( 6 == calendar_week_mod(date('w', mktime(0, 0 , 0, $thismonth, $day, $thisyear))-$week_begins) )
			$newrow = true;
	}

	$pad = 7 - calendar_week_mod(date('w', mktime(0, 0 , 0, $thismonth, $day, $thisyear))-$week_begins);
	if ( $pad != 0 && $pad != 7 )
		$calendar_output .= "\n\t\t".'<td class="pad" colspan="'. esc_attr($pad) .'">&nbsp;</td>';

	$calendar_output .= "\n\t</tr>\n\t</tbody>\n\t</table>";

	$cache[ $key ] = $calendar_output;
	wp_cache_set( 'get_calendar', $cache, 'calendar' );

	if ( $echo )
		echo apply_filters( 'get_calendar',  $calendar_output );
	else
		return apply_filters( 'get_calendar',  $calendar_output );

}
?>