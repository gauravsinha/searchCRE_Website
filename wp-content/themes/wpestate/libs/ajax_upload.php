<?php

add_action('wp_ajax_me_upload', 'me_upload');
add_action('wp_ajax_aaiu_delete', 'me_delete_file');

add_action('wp_ajax_nopriv_me_upload',  'me_upload');
add_action('wp_ajax_nopriv_aaiu_delete', 'me_delete_file');

function me_delete_file()
{
    $attach_id = $_POST['attach_id'];
    wp_delete_attachment($attach_id, true);
    exit;
}

function me_upload()
{
   // check_ajax_referer('aaiu_allow', 'nonce');

    $file = array(
        'name' => $_FILES['aaiu_upload_file']['name'],
        'type' => $_FILES['aaiu_upload_file']['type'],
        'tmp_name' => $_FILES['aaiu_upload_file']['tmp_name'],
        'error' => $_FILES['aaiu_upload_file']['error'],
        'size' => $_FILES['aaiu_upload_file']['size']
    );
    $file = fileupload_process($file);
       

}  
    
    
    
function fileupload_process($file){
$attachment = handle_file($file);

if (is_array($attachment)) {
    $html = getHTML($attachment);

    $response = array(
        'success' => true,
        'html' => $html,
        'attach'=> $attachment['id']
    );

    echo json_encode($response);
    exit;
}

$response = array('success' => false);
echo json_encode($response);
exit;
}
    
    
    
    
    
    
    
    function handle_file($upload_data)
    {

        $return = false;
        $uploaded_file = wp_handle_upload($upload_data, array('test_form' => false));

        if (isset($uploaded_file['file'])) {
            $file_loc = $uploaded_file['file'];
            $file_name = basename($upload_data['name']);
            $file_type = wp_check_filetype($file_name);

            $attachment = array(
                'post_mime_type' => $file_type['type'],
                'post_title' => preg_replace('/\.[^.]+$/', '', basename($file_name)),
                'post_content' => '',
                'post_status' => 'inherit'
            );

            $attach_id = wp_insert_attachment($attachment, $file_loc);
            $attach_data = wp_generate_attachment_metadata($attach_id, $file_loc);
            wp_update_attachment_metadata($attach_id, $attach_data);

            $return = array('data' => $attach_data, 'id' => $attach_id);

            return $return;
        }

        return $return;
    }
    
    
    function getHTML($attachment)
    {

        $attach_id = $attachment['id'];
        $file = explode('/', $attachment['data']['file']);
        $file = array_slice($file, 0, count($file) - 1);
        $path = implode('/', $file);
        $image = $attachment['data']['sizes']['thumbnail']['file'];
        $post = get_post($attach_id);
        $dir = wp_upload_dir();
        $path = $dir['baseurl'] . '/' . $path;

        $html = '';
       // $html .= '<li class="aaiu-uploaded-files">';
        //$html .= sprintf('<img src="%s" name="' . $post->post_title . '" />', $path . '/' . $image);
        //$html .= sprintf('<br /><a href="#" class="action-delete" data-upload_id="%d">%s</a></span>', $attach_id, __('Delete'));
        //$html .= sprintf('<input type="hidden" name="aaiu_image_id[]" value="%d" />', $attach_id);
        //$html .= '</li>';
        global $current_user;
        get_currentuserinfo();
        $userID         =   $current_user->ID;
      
        //if( is_page_template('user_dashboard_profile.php') ){
          //  update_user_meta($userID, 'custom_picture', $path.'/'.$image);
       // }
   
       
        $html .= $path.'/'.$image; 
        return $html;
    }
 
?>