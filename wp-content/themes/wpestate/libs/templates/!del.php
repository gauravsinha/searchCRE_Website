  <div class="adv_search_internal advz1"> 
            <select id="adv_actions_2_mobile"  name="filter_search_action[]"  class="cd-select" >
                <option value="all"><?php _e('All Listings','wpestate'); ?></option>
                <?php print $actions_select; ?>
            </select>    
        </div>

        <div class="adv_search_internal advz2"> 
            <select id="adv_categ_2_mobile"  name="filter_search_type[]" class="cd-select" >
                <option value="all"><?php _e('All Types','wpestate'); ?></option>
                <?php print $categ_select; ?>
            </select>  
        </div>

        <div class="adv_search_internal advz3 advanced_city_div_mobile">
            <select id="advanced_city_2_mobile" name="advanced_city" class="cd-select" >
                 <option value="all"><?php _e('All Cities','wpestate'); ?></option>
                <?php echo $select_city ;?>
            </select>
        </div>

        <div class="adv_search_internal advz4 advanced_area_div_mobile">
            <select id="advanced_area_2_mobile" name="advanced_area"  class="cd-select">
                <option value="all"  data-parentcity="*" ><?php _e('All Areas','wpestate'); ?></option>
                <?php echo $select_area; ?>
            </select>
        </div>

        <div class="adv_search_internal ">
            <input type="text" id="adv_rooms" name="advanced_rooms" value="<?php _e('Type Rooms No.','wpestate');?>"      class="advanced_select">
        </div>

        <div class="adv_search_internal "> 
            <input type="text" id="adv_bath"  name="advanced_bath"  value="<?php _e('Type Bathrooms No.','wpestate');?>"  class="advanced_select">
        </div>

        <div class="adv_search_internal ">
            <input type="text" id="price_low" name="price_low"  class="advanced_select" value="<?php _e('Type Min. Price','wpestate');?>"/>
        </div>

        <div class="adv_search_internal ">    
            <input type="text" id="price_max" name="price_max"  class="advanced_select" value="<?php _e('Type Max. Price','wpestate');?>"/>
        </div>    