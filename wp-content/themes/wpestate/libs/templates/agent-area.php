<?php

$agent_id   = intval( get_post_meta($post->ID, 'property_agent', true) );
$prop_id    = $post->ID;  
if(is_user_logged_in()){
if ($agent_id!=0){                        
        $args = array(
            'post_type' => 'estate_agent',
            'p' => $agent_id
        );

        $agent_selection = new WP_Query($args);
        $thumb_id       = '';
        $preview_img    = '';
        $agent_skype    = '';
        $agent_phone    = '';
        $agent_mobile   = '';
        $agent_email    = '';
        $agent_pitch    = '';
        $link           = '';
        $name           = 'No agent';

        if( $agent_selection->have_posts() ){
               print'<div class="agent_listing-prop agent_bottom_border" >';
               while ($agent_selection->have_posts()): $agent_selection->the_post();
                   $thumb_id       = get_post_thumbnail_id($post->ID);
                   $preview        = wp_get_attachment_image_src(get_post_thumbnail_id(), 'agent_picture_thumb');
                   $preview_img    = $preview[0];
                   $agent_skype    = esc_html( get_post_meta($post->ID, 'agent_skype', true) );
                   $agent_phone    = esc_html( get_post_meta($post->ID, 'agent_phone', true) );
                   $agent_mobile   = esc_html( get_post_meta($post->ID, 'agent_mobile', true) );
                   $agent_email    = esc_html( get_post_meta($post->ID, 'agent_email', true) );
                   $agent_pitch    = esc_html( get_post_meta($post->ID, 'agent_pitch', true) );
                   $agent_posit    = esc_html( get_post_meta($post->ID, 'agent_position', true) );
                   $link           = get_permalink();
                   $name           = get_the_title();



                   print '
                   <div class="featured_agent_image" data-agentlink="'. $link .'" style="background-image: url(\'' . $preview_img . '\');">
                       <div class="featured_agent_image_hover">
                           <span>'.$name.'</span>
                       </div> 
                   </div>
                   <div class="agent_listing_prop_details" id="prop_inq">
                       <h3><a href="' . $link . '">' .  $name  . '</a></h3>
                       <div class="agent_title">'.$agent_posit .'</div>';
                       if ($agent_phone) {
                           print '<div class="agent_detail"><a href="tel:' . $agent_phone . '">'.$agent_phone.'</a></div>';
                       }
                       if ($agent_mobile) {
                           print '<div class="agent_detail"><a href="tel:'.$agent_mobile.'">'.$agent_mobile.'</a></div>';
                       }

                       if ($agent_email) {
                           print '<div class="agent_detail"><a href="mailto:' . $agent_email . '">' . $agent_email . '</a></div>';
                       }

                       if ($agent_skype) {
                           print '<div class="agent_detail">'.__('Skype','wpestate').': ' . $agent_skype . '</div>';
                       }
                       print '<input name="prop_id" type="hidden"  id="agent_property_id" value="'.$prop_id.'">';
                       print'
                       <div class="my_other">
                           <div class="btn listing agentbut listinglink"><a href="' .$link.'"><span class="agent_plus">+ </span>'.__('my listings', 'wpestate').'</a></div>
                       </div>
                      
                   </div>';

               get_template_part ('libs/templates/agentcontactform');
               wp_reset_query();
               endwhile;
               print'   </div>';
       } // end if have posts
}   // end if !=0
else{  
    
        if ( get_the_author_meta('user_level') !=10){
            
       
        $userid                 =   get_the_author_meta('ID') ;
        $first_name             =   get_the_author_meta( 'first_name'  );
        $last_name              =   get_the_author_meta( 'last_name'  );
        $user_email             =   get_the_author_meta( 'user_email'  );
        $user_phone             =   get_the_author_meta( 'phone'  );
        $description            =   get_the_author_meta( 'description'  );
        $user_skype             =   get_the_author_meta( 'skype'  );
        $user_title             =   get_the_author_meta( 'title'  );
        $user_custom_picture    =   get_the_author_meta( 'custom_picture'  );
        if($user_custom_picture==''){
            $user_custom_picture=get_template_directory_uri().'/images/default-user.png';
        }
            
        print '
            <div class="agent_listing-prop agent_bottom_border" >
            
            <div class="featured_agent_image"  style="background-image: url(\'' . $user_custom_picture . '\');">
                <div class="featured_agent_image_hover">
                    <span>'.$first_name.' '.$last_name.'</span>
                </div> 
            </div>
            
            <div class="agent_listing_prop_details" id="prop_inq">
                <h3> '.$first_name.' '.$last_name . '</h3>
                <div class="agent_title">'.$user_title .'</div>';
                if ($user_phone) {
                    print '<div class="agent_detail"><a href="tel:'.$user_phone.'">'.$user_phone.'</a></div>';
                }
               
                if ($user_email) {
                    print '<div class="agent_detail"><a href="mailto:' . $user_email . '">' . $user_email . '</a></div>';
                }

                if ($user_skype) {
                    print '<div class="agent_detail">'.__('Skype','wpestate').': ' . $user_skype . '</div>';
                }
                print'
             </div>';
            print '<input name="prop_id" type="hidden"  id="agent_property_id" value="'.$prop_id.'">';
            get_template_part ('libs/templates/agentcontactform');
            
            print'</div>';
           }
}
}else {
print' <div style="margin-left:-60px;padding-bottom:10px;"><a href="http://54.85.15.66/realestate/?page_id=6902" style="text-decoration:underline;font-weight:bold;">Login first to contact the owner</a></div> ';
}
?>