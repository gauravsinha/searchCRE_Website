<?php
//$did_he_book    = user_booked_this($userID,$prop_id);
global $user_custom_picture;
global $name;
global $agent_posit;
global $agent_phone;
global $agent_mobile;
global $agent_email;
global $agent_skype;
global $content;
global $agent_id;
global $userID;
$args = array(
        'fields' =>'ID',
        'meta_query' => array(

                0 => array(
                        'key'     => 'user_agent_id',
                        'value'   => $agent_id,
                        'compare' => '='
                ),

        )
    );

//print_r($args);
$user_query = new WP_User_Query( $args );
$user_agent_id=$user_query->results[0];
global $current_user;
get_currentuserinfo();
$userID                     =   $current_user->ID;
                   
?>

<div class="agent_listing fullinfo agent_bottom_border" >
                        <div class="featured_agent_image" style="background-image: url('<?php print $user_custom_picture; ?>');">
                            <div class="featured_agent_image_hover">
                                <span><?php echo $name; ?></span>
                             </div> 
                        </div>
                        <div class="agent_listing_details">
                            <?php
                            print '<h3>' .$name. '</h3>
                            <div class="agent_title">'.$agent_posit.'</div>';
                            
                            if ( user_booked_from_agent($userID,$user_agent_id) ) {
                                if ($agent_phone) {
                                    print '<div class="agent_detail"><a href="tel:' . $agent_phone . '">' . $agent_phone . $post->ID.'</a></div>';
                                }
                                if ($agent_mobile) {
                                    print '<div class="agent_detail"><a href="tel:' . $agent_mobile . '">' . $agent_mobile . '</a></div>';
                                }

                                if ($agent_email) {
                                    print '<div class="agent_detail"><a href="mailto:' . $agent_email . '">' . $agent_email . '</a></div>';
                                }

                                if ($agent_skype) {
                                    print '<div class="agent_detail">'.__('Skype','wpestate').': ' . $agent_skype . '</div>';
                                }
                            }else{
                                print '<div>'.__('Detailed contact information will be available after a booking is confirmed!','wpestate').'</div>';
                            }
                            
                           
                            ?>

                        </div> 

                        <?php
                        print '<div class="agent_content">'.$content.'</div>';
                        ?>
                        <hr class="dottedline-agent">
                        
                        <h4 id="show_contact"><?php _e('Contact Me', 'wpestate') ?></h4>
                        
                        <div class="agent_contanct_form" id="booking_form_contact_agent">
                            <p class="full_form" id="booking_contact"></p>

                         
                            <p class="full_form">
                                <label for="booking_mes_subject"><?php _e('Subject','wpestate');?></label>
                                <input type="text" id="booking_mes_subject" size="40" name="booking_mes_subject" value="" style="width:50%;display:block">
                            </p>
                    
                            <p class="full_form">
                               <label for="booking_mes_mess"><?php _e('Message','wpestate');?></label>
                               <textarea id="booking_mes_mess" tabindex="3" name="booking_mes_mess" cols="50" rows="6"></textarea>
                            </p>
                            <input type="hidden" id="agent_id" value="<?php echo $user_agent_id;?>">
                            <button type="submit" id="submit_mess_front" class="btn vernil small" style="margin-bottom: 20px;"><?php _e('Send Message','wpestate');?></button>
                            <?php print wp_nonce_field( 'mess_ajax_nonce_front', 'security-register-mess-front' );?>
                        </div>
                    </div>
