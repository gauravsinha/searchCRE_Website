<?php
// global vars are in map-search-form.php
$custom_image   =   '';
$rev_slider     =   '';
if( isset($post->ID) ){
    $custom_image   =   esc_html( esc_html(get_post_meta($post->ID, 'page_custom_image', true)) );  
    $rev_slider     =   esc_html( esc_html(get_post_meta($post->ID, 'rev_slider', true)) );
}

$hide_class='';
if ((!is_front_page() && $custom_image=='' && $rev_slider=='' ) || (is_front_page() && $home_small_map_status=='yes' ) ) {
    $hide_class= ' geohide advhome ';       
}

$show_over_search   =   get_option('wp_estate_show_adv_search','');
$dropdown_id        =   '';

if(!is_front_page() || (is_front_page() && $home_small_map_status=='yes' ) ){
    $dropdown_id='_internal'; 
}

if ($show_over_search=='yes' && ($custom_image!='' || $rev_slider!='') ){
    $dropdown_id='';
}


$pages = get_pages(array(
 'meta_key' => '_wp_page_template',
 'meta_value' => 'advanced-search-results-booking.php'
     ));

if( $pages ){
    $adv_submit_booking = get_permalink( $pages[0]->ID);
}else{
     $adv_submit_booking='';
}

$dropdown_id        =   '';

if(!is_front_page() || (is_front_page() && $home_small_map_status=='yes' ) ){
    $dropdown_id='_internal'; 
}

if ($show_over_search=='yes' && ($custom_image!='' || $rev_slider!='') ){
    $dropdown_id='';
}

?>




<div class="adv-search-5 <?php echo $hide_class;?>" id="adv-search-5"> 
    <form role="search" method="post"   action="<?php print $adv_submit_booking; ?>" id="advanced_booking_form">
        <div class="adv5_hider">
        <div class="adv_search_internal firstclear booking_where">
            <input type="text" id="booking_location" name="booking_location" placeholder="<?php _e('Where do you want to go?','wpestate');?>"      class="advanced_select">
        </div>

        <div class="adv_search_internal check_in_adv"> 
            <input type="text" id="check_in"  name="check_in"  placeholder="<?php _e('Check In','wpestate');?>"  class="advanced_select">
        </div>

        <div class="adv_search_internal check_out_adv ">
            <input type="text" id="check_out" name="check_out"  class="advanced_select" placeholder="<?php _e('Check Out','wpestate');?>"/>
        </div>

        <div class="adv_search_internal guests_adv">    
            <select id="booking_guest<?php echo $dropdown_id; ?>"  name="booking_guest"  class="cd-select" >
                <option value="1"><?php echo '1 '.__('Guest','wpestate'); ?></option>
                <?php
                for ($i = 2; $i <= 11; $i++) {
                    print ' <option value="'.$i.'">'.$i.' '.__('Guests','wpestate').'</option>';
                }
                
                ?>
            </select>    
        </div>    
           
        <input type="submit" class="btn vernil small" id="advanced_submit_5" value=" <?php _e('Search','wpestate');?>" />
        </div>
        <span class="adv5_label"><?php _E('Advanced Search','wpestate');?></span>
        <div id="adv_5_closer"><i class="fa fa-chevron-up"></i></div>
        
        
    </form>   
</div>  