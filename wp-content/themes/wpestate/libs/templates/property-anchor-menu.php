<?php
if (function_exists('icl_translate') ){
    $floating_property_similar_text         =   icl_translate('wpestate','wp_estate_floating_similar',esc_html( get_option('wp_estate_floating_property_similar_text') ) );
    $floating_property_inquires_text        =   icl_translate('wpestate','wp_estate_floating_inquires',esc_html( get_option('wp_estate_floating_property_inquires_text') ) );
    $floating_property_features_text        =   icl_translate('wpestate','wp_estate_floating_features',esc_html( get_option('wp_estate_floating_property_features_text') ) );
    $floating_property_details_text         =   icl_translate('wpestate','wp_estate_floating_details',esc_html( get_option('wp_estate_floating_property_details_text ') ) );
    $floating_property_description_text     =   icl_translate('wpestate','wp_estate_floating_desc',esc_html( get_option('wp_estate_floating_property_description_text') ) );
}else{
    $floating_property_similar_text         =   esc_html( get_option('wp_estate_floating_property_similar_text') );
    $floating_property_inquires_text        =   esc_html( get_option('wp_estate_floating_property_inquires_text') );
    $floating_property_features_text        =   esc_html( get_option('wp_estate_floating_property_features_text') );
    $floating_property_details_text         =   esc_html( get_option('wp_estate_floating_property_details_text ') );
    $floating_property_description_text     =   esc_html( get_option('wp_estate_floating_property_description_text') );
}

$feature_list_array                     =   array();
$feature_list                           =   esc_html( get_option('wp_estate_feature_list') );
$feature_list_array                     =   explode( ',',$feature_list);

$agent_id = intval( get_post_meta($post->ID, 'property_agent', true) );
        $content = get_the_content();



?>

<div class="anchor-bord">
     <ul>
         <?php   if($content!=''){?>
         <li><a href="#prop_des">
             <?php 
             if($floating_property_description_text ==''){ 
                 _e('Description', 'wpestate');                
             }else{
                 print $floating_property_description_text;                    
             }
             ?></a>
         </li>
         <?php }?>
        
         <li><a href="#prop_det">
             <?php 
             if($floating_property_details_text ==''){ 
                 _e('Details', 'wpestate');                 
             }else{
                    print $floating_property_details_text;                    
             } 
             ?></a>
         </li>
         
         <?php if ( !count( $feature_list_array )==0 ){?>
         <li><a href="#prop_ame">
             <?php if($floating_property_features_text ==''){ 
                 _e('Features', 'wpestate');                  
             }else{
                 print $floating_property_features_text;                    
             } 
             ?></a>
         </li>
         <?php } ?>
         
         <?php if ($agent_id!=0){  ?>
         <li><a href="#prop_inq">
             <?php if($floating_property_inquires_text ==''){ 
                 _e('Inquires', 'wpestate');                  
             }else{
                 print $floating_property_inquires_text;                    
             } ?></a>
         </li>
         <?php }?>
         
         <li><a href="#comment">
             <?php if($floating_property_similar_text ==''){ 
                 _e('Similar Listings', 'wpestate');            
             }else{
                 print $floating_property_similar_text;                    
             } ?></a>
         </li>
     </ul>
 </div>