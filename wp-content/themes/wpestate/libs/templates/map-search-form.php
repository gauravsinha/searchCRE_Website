<!-- SEARCH CODE -->
<?php
$pages = get_pages(array(
    'meta_key' => '_wp_page_template',
    'meta_value' => 'advanced-search-results.php'
        ));

if( $pages ){
    $adv_submit = get_permalink( $pages[0]->ID);
}else{
     $adv_submit='';
}
$currency       = esc_html( get_option('wp_estate_currency_symbol', '') );
$where_currency = esc_html( get_option('wp_estate_where_currency_symbol', '') );

if ($where_currency == 'before') {
    $before = $currency . ' ';
    $after = '';
} else {
    $before = '';
    $after = ' ' . $currency;
}

$args = array(
        'hide_empty'    => true  
        ); 

$show_empty_city_status= esc_html ( get_option('wp_estate_show_empty_city','') );

if ($show_empty_city_status=='yes'){
    $args = array(
        'hide_empty'    => false  
        ); 
}
 

$select_city='';
$taxonomy = 'property_city';
$tax_terms = get_terms($taxonomy,$args);
foreach ($tax_terms as $tax_term) {
   $select_city.= '<option value="' . $tax_term->name . '">' . $tax_term->name . '</option>';
}

if ($select_city==''){
      $select_city.= '<option value="">No Cities</option>';
}

$select_area='';
$taxonomy = 'property_area';
$tax_terms = get_terms($taxonomy,$args);

foreach ($tax_terms as $tax_term) {
    $term_meta=  get_option( "taxonomy_$tax_term->term_id");
    $select_area.= '<option value="' . $tax_term->name . '" data-parentcity="' . $term_meta['cityparent'] . '">' . $tax_term->name . '</option>';
}
$icons          =   array();
$taxonomy       =   'property_action_category';
$tax_terms      =   get_terms($taxonomy,$args);
$taxonomy_cat   =   'property_category';
$categories     =   get_terms($taxonomy_cat,$args);
  

// add only actions
foreach ($tax_terms as $tax_term) {
    $icon_name          =   limit64( 'wp_estate_icon'.$tax_term->slug);
    $limit50            =   limit50( $tax_term->slug);
    $icons[$limit50]    =   esc_html( get_option($icon_name) ); 
}

// add only categories 
foreach ($categories as $categ) {
    $icon_name          =   limit64( 'wp_estate_icon'.$categ->slug);
    $limit50            =   limit50( $categ->slug);
    $icons[$limit50]    =   esc_html( get_option($icon_name) ); 
} 
 


//$args=array( 'hide_empty'    => false );
$taxonomy = 'property_action_category';
$tax_terms = get_terms($taxonomy,$args);

$taxonomy_categ = 'property_category';
$tax_terms_categ = get_terms($taxonomy_categ,$args);

$actions_select     =   '';
$categ_select       =   '';


 if( !empty( $tax_terms ) ){                       
    foreach ($tax_terms as $tax_term) {
        $actions_select.='<option value="'.$tax_term->name.'">'.$tax_term->name.'</option>';      
    } 
}


 if( !empty( $tax_terms_categ ) ){                       
    foreach ($tax_terms_categ as $categ) {
        $categ_select.='<option value="'.$categ->name.'">'.$categ->name.'</option>';      
    }
}



$home_small_map_status  =   esc_html ( get_option('wp_estate_home_small_map','') );
$adv_search_what        =   get_option('wp_estate_adv_search_what','');
$adv_search_label       =   get_option('wp_estate_adv_search_label','');
$adv_search_how         =   get_option('wp_estate_adv_search_how','');
$header_type_status     =   intval ( get_option('wp_estate_header_type',''));   
?>

<div class="search_wrapper search_wrapper_type<?php echo $header_type_status;?>" id="search_wrapper" >
       
       <div class="search_holder" id="adv_search_holder">
        <?php  
        $adv_search                 =   'adv-search1.php'; 
        $adv_search_type_status     =   intval ( get_option('wp_estate_adv_search_type',''));
        if($adv_search_type_status!=''){
            $adv_search             =   'adv-search'.$adv_search_type_status.'.php'; 
        }
        require_once $adv_search;
        
        
        ?>
       
      </div><!-- end search holder--> 
</div><!-- end search wrapper--> 
<!-- END SEARCH CODE --><?php  


 function show_search_field($search_field,$css_value,$dropdown_id,$actions_select,$categ_select,$select_city,$select_area,$adv_search_label,$key,$adv_search_how){
            
          
            
            if($search_field=='none'){
                $return_string=''; 
            }
            else if($search_field=='types'){
                    
                    $return_string='
                    <div class="advanced_action_div adv'.($key+1).'"> 
                        <select id="adv_actions_'.$css_value.$dropdown_id.'"  name="filter_search_action[]"  class="cd-select" >
                            <option value="all">'.__('All Listings','wpestate').'</option>
                            '.$actions_select.'
                        </select>    
                    </div>';
                    
            }else if($search_field=='categories'){
                    
                    $return_string='
                    <div class="advanced_categ_div adv'.($key+1).'"> 
                        <select id="adv_categ_'.$css_value.$dropdown_id.'"  name="filter_search_type[]" class="cd-select" >
                            <option value="all">'.__('All Types','wpestate').'</option>
                            '.$categ_select.'
                        </select>  
                    </div>';

            }  else if($search_field=='cities'){
                    
                    $return_string='
                     <div class="advanced_city_div adv'.($key+1).'">
                        <select id="advanced_city_'.$css_value.$dropdown_id.'" name="advanced_city" class="cd-select" >
                             <option value="all">'. __('All Cities','wpestate').'</option>
                             '.$select_city.'
                        </select>
                     </div>';
                
           }   else if($search_field=='areas'){

                    $return_string='
                     <div class="advanced_area_div adv'.($key+1).'">
                        <select id="advanced_area_'.$css_value.$dropdown_id.'" name="advanced_area"  class="cd-select">
                            <option value="all" data-parentcity="*"  >'.__('All Areas','wpestate').'</option>
                            '.$select_area.'
                        </select>
                    </div>';
                
            }    else {
                    $slug=str_replace(' ','_',$search_field);

                    $label=$adv_search_label[$key];
                    if (function_exists('icl_translate') ){
                      $label     =   icl_translate('wpestate','wp_estate_custom_search_'.$label, $label ) ;
                    }


                    $string =   str_replace(' ','-',$adv_search_label[$key]);
                    //$result =   preg_replace("/[^a-zA-Z0-9]+/", "", $string);
                    $result       =   sanitize_title($string);

                    if ( $adv_search_how[$key]=='date bigger' || $adv_search_how[$key]=='date smaller'){

                      $result_id    =   sanitize_key($result);
                      $return_string='
                      <div class="adv_search_internal"> 
                          <input type="text" id="'.$result_id.'"  name="'.$result.'"  placeholder="'.$label.'"  class="advanced_select">
                      </div>
                      ';
                        print '<script type="text/javascript">
                              //<![CDATA[
                              jQuery(document).ready(function(){
                                      jQuery("#'.$result_id.'").datepicker({
                                              dateFormat : "yy-mm-dd"
                                      });
                              });
                              //]]>
                              </script>';
                    }else{
                        $return_string='
                        <div class="adv_search_internal"> 
                            <input type="text" id="'.$result.'"  name="'.$result.'"  placeholder="'.$label.'"  class="advanced_select">
                        </div>
                        ';
                    }
           

            } 
            print $return_string;
}
   
     
         
         
         
         
         
         
         
         
         
         
         
         
function show_search_field_type1($key,$search_field,$actions_select,$categ_select,$select_city,$select_area,$adv_search_label,$adv_search_how){
            
            $return_string='';
            if($key==0){
                 $return_string.='<div class="adv_search_internal firstcol">';
            }
            
            if($key == '2' || $key == '4' ){
                 $return_string.='<div class="adv_search_internal">';
            }
            
           if($search_field=='types' || $search_field=='categories'){
               //donothing
           }
           else if($search_field=='none'){
                
           }
          else if($search_field=='cities'){
                    
                    $return_string.='
                     <div class="advanced_city_div">
                       <select id="advanced_city" name="advanced_city" class="cd-select" >
                             <option value="all">'. __('All Cities','wpestate').'</option>
                             '.$select_city.'
                        </select>
                     </div>';
                
           }   else if($search_field=='areas'){

                    $return_string.='
                     <div class="advanced_area_div">
                         <select id="advanced_area" name="advanced_area"  class="cd-select">
                            <option value="all" data-parentcity="*"  >'.__('All Areas','wpestate').'</option>
                            '.$select_area.'
                        </select>
                    </div>';
                
            } else {
                
                $slug=str_replace(' ','_',$search_field);

                $label=$adv_search_label[$key];
                if (function_exists('icl_translate') ){
                  $label     =   icl_translate('wpestate','wp_estate_custom_search_'.$label, $label ) ;
                }


                $string =   str_replace(' ','-',$adv_search_label[$key]);
                //$result =   preg_replace("/[^a-zA-Z0-9]+/", "", $string);
                $result       =   sanitize_title($string);

                if ( $adv_search_how[$key]=='date bigger' || $adv_search_how[$key]=='date smaller'){

                     $result_id    =   sanitize_key($result);
                    $return_string.=' <input type="text" id="'.$result_id.'"  name="'.$result.'"  placeholder="'.$label.'"  class="advanced_select">';

                    print '<script type="text/javascript">
                              //<![CDATA[
                              jQuery(document).ready(function(){
                                      jQuery("#'.$result_id.'").datepicker({
                                              dateFormat : "yy-mm-dd"
                                      });
                              });
                              //]]>
                              </script>';
                }else{
                    $return_string.='  <input type="text" id="'.$result.'"  name="'.$result.'"  placeholder="'.$label.'"  class="advanced_select">';

                }
                  
            }  
                
   
            if($key==1 || $key==3 || $key==5){
                 $return_string.='</div>';
            }
            
            print $return_string;
         }
                   
         
?>


