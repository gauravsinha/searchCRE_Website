<?php
add_action('add_meta_boxes', 'estate_sidebar_meta');
add_action('save_post', 'estate_save_postdata', 1, 2);

function estate_sidebar_meta() {
    add_meta_box('wpestate-sidebar-post',       __('Sidebar Settings',  'wpestate'), 'estate_sidebar_box', 'post');
    add_meta_box('wpestate-sidebar-page',       __('Sidebar Settings',  'wpestate'), 'estate_sidebar_box', 'page');
    add_meta_box('wpestate-sidebar-property',   __('Sidebar Settings',  'wpestate'), 'estate_sidebar_box', 'estate_property');
    add_meta_box('wpestate-sidebar-agent',      __('Sidebar Settings',  'wpestate'), 'estate_sidebar_box', 'estate_agent');
    add_meta_box('wpestate-settings-post',      __('Post Settings',     'wpestate'), 'estate_post_options_box', 'post', 'normal', 'default' );
    add_meta_box('wpestate-settings-page',      __('Page Settings',     'wpestate'), 'estate_page_options_box', 'page', 'normal', 'default' );
    add_meta_box('wpestate-settings-page',      __('Page Settings',     'wpestate'), 'estate_page_options_box', 'estate_agent', 'normal', 'default' );
    
    add_meta_box('wpestate-border-page',        __('Border Settings',   'wpestate'), 'estate_page_border_box', 'page', 'normal', 'default' );
    add_meta_box('wpestate-map-page',           __('Map Settings',      'wpestate'), 'estate_page_map_box', 'page', 'normal', 'default' );
    add_meta_box('wpestate-map-post',           __('Map Settings',      'wpestate'), 'estate_page_map_box_agent', 'estate_agent', 'normal', 'default' );
    add_meta_box('wpestate-map-agent',          __('Map Settings',      'wpestate'), 'estate_page_map_box', 'post', 'normal', 'default' );
    add_meta_box('wpestate-slider-set',         __('Slider Settings',   'wpestate'), 'estate_page_slider_box', 'post', 'normal', 'default' );
    add_meta_box('wpestate-slider-set',         __('Slider Settings',   'wpestate'), 'estate_page_slider_box', 'page', 'normal', 'default' );
    add_meta_box('wpestate-slider-set',         __('Slider Settings',   'wpestate'), 'estate_page_slider_box', 'estate_agent', 'normal', 'default' );

    
}





function estate_page_slider_box($post) {
    global $post;
    $rev_slider           = get_post_meta($post->ID, 'rev_slider', true);
    print '  
    <p class="meta-options">	
        <label for="page_custom_lat">'.__('Revolution Slider Name','wpestate').'</label><br />
        <input type="text" id="rev_slider" name="rev_slider" size="40" value="'.$rev_slider.'">
    </p>
    ';

}




function estate_page_map_box($post) {
    global $post;
    $page_lat           = get_post_meta($post->ID, 'page_custom_lat', true);
    $page_long          = get_post_meta($post->ID, 'page_custom_long', true);
    $page_custom_image  = get_post_meta($post->ID, 'page_custom_image', true);
    $page_custom_zoom  = get_post_meta($post->ID, 'page_custom_zoom', true);
    
    if ($page_custom_zoom==''){
        $page_custom_zoom=15;
    }
    
    print '
    <p class="meta-options">
    '.__('  Leave these blank in order to get the general map settings.','wpestate').'
    </p>
    <p class="meta-options">	
        <label for="page_custom_lat">'.__('Map - Center point  Latitudine: ','wpestate').'</label><br />
        <input type="text" id="page_custom_lat" name="page_custom_lat" size="40" value="'.$page_lat.'">
    </p>
    <p class="meta-options">	
        <label for="page_custom_long">'.__('Map - Center point  Longitudine: ','wpestate').'</label><br />
        <input type="text" id="page_custom_long" name="page_custom_long" size="40" value="'.$page_long.'">
    </p>
    
   
    <p class="meta-options">
        <label for="page_custom_image">'.__('Replace Map with this image','wpestate').'</label><br />
        <input id="page_custom_image" type="text" size="36" name="page_custom_image" value="'.$page_custom_image.'" />
	<input id="page_custom_image_button" type="button"   size="40" class="upload_button button" value="'.__('Upload Image','wpestate').'" />
     </p>
     
     <p class="meta-options">
       <label for="page_custom_zoom">'.__('Zoom Level for map (1-20)','wpestate').'</label><br />
       <select name="page_custom_zoom" id="page_custom_zoom">';
      
      for ($i=1;$i<21;$i++){
           print '<option value="'.$i.'"';
           if($page_custom_zoom==$i){
               print ' selected="selected" ';
           }
           print '>'.$i.'</option>';
       }
        
     print'
       </select>
     <p>
    ';
     
     
    
}



function estate_page_map_box_agent($post) {
    global $post;
    $page_lat           = get_post_meta($post->ID, 'page_custom_lat', true);
    $page_long          = get_post_meta($post->ID, 'page_custom_long', true);
    $page_custom_image  = get_post_meta($post->ID, 'page_custom_image', true);
    $page_custom_zoom  = get_post_meta($post->ID, 'page_custom_zoom', true);
    
    if ($page_custom_zoom==''){
        $page_custom_zoom=15;
    }
    
    print '
   
    <p class="meta-options">
        <label for="page_custom_image">'.__('Replace Map with this image','wpestate').'</label><br />
        <input id="page_custom_image" type="text" size="36" name="page_custom_image" value="'.$page_custom_image.'" />
	<input id="page_custom_image_button" type="button"   size="40" class="upload_button button" value="'.__('Upload Image','wpestate').'" />
     </p>
     
     <p class="meta-options">
       <label for="page_custom_zoom">'.__('Zoom Level for map (1-20)','wpestate').'</label><br />
       <select name="page_custom_zoom" id="page_custom_zoom">';
      
      for ($i=1;$i<21;$i++){
           print '<option value="'.$i.'"';
           if($page_custom_zoom==$i){
               print ' selected="selected" ';
           }
           print '>'.$i.'</option>';
       }
        
     print'
       </select>
     <p>
    ';
     
     
    
}



function estate_page_options_box($post) {
    global $post;

    $page_title = get_post_meta($post->ID, 'page_show_title', true);
    $selected_no = $selected_yes = '';

    if ($page_title == 'no') {
        $selected_no = 'selected="selected"';
    } else {
        $selected_yes = 'selected="selected"';
    }

    if ($page_title != '') {
        $page_title_select = '<option value="' . $page_title . '" selected="selected">' . $page_title . '</option>';
    }

    print '
    <p class="meta-options">	
    <label for="page_show_title">'.__('Show Title: ','wpestate').'</label><br />
    <select id="page_show_title" name="page_show_title" style="width: 200px;">
            <option value="yes" ' . $selected_yes . '>yes</optionpage_show_title>
            <option value="no" ' . $selected_no . '>no</option>
    </select></p>';
}






function estate_page_border_box($post) {
    wp_nonce_field(plugin_basename(__FILE__), 'wpestate_page_noncename');
    global $post;

    $page_title = get_post_meta($post->ID, 'page_border', true);
    $selected_no = $selected_yes = '';
    
    
    $border_option = get_post_meta($post->ID, 'border_option', true);
    $option = '';
    $border_values = array('agent border', 'listing border', 'blog border','white border','no border');

    foreach ($border_values as $value) {
        $option.='<option value="' . $value . '"';
        if ($value == $border_option) {
            $option.=' selected="selected"';
        }
        $option.='>' . $value . '</option>';
    }

    print '
    <p class="meta-options">	
    <label for="border_option">'.__('Select Border Type: ','wpestate').' '.$border_option.' </label><br />
    <select id="border_option" name="border_option" style="width: 200px;">
             '.$option.'
    </select></p>';
}





function estate_post_options_box($post) {
    wp_nonce_field(plugin_basename(__FILE__), 'estate_property_noncename');
    global $post;

    $option = '';
    $title_values = array('yes', 'no');
    $post_title = get_post_meta($post->ID, 'post_show_title', true);
    foreach ($title_values as $value) {
        $option.='<option value="' . $value . '"';
        if ($value == $post_title) {
            $option.='selected="selected"';
        }
        $option.='>' . $value . '</option>';
    }

    print '<p class="meta-options">	
                <label for="post_show_title">'.__('Show Title:','wpestate').' </label><br />
                <select id="post_show_title" name="post_show_title" style="width: 200px;">
                        ' . $option . '
                </select><br />
          </p>';

    $option = '';
    $title_values = array('yes', 'no');
    $group_pictures = get_post_meta($post->ID, 'group_pictures', true);
    foreach ($title_values as $value) {
        $option.='<option value="' . $value . '"';
        if ($value == $group_pictures) {
            $option.='selected="selected"';
        }
        $option.='>' . $value . '</option>';
    }

    print'
        <p class="meta-options">	
                <label for="group_pictures">'.__('Group pictures in slider?(*only for blog posts)','wpestate').' </label><br />
                <select id="group_pictures" name="group_pictures" style="width: 200px;">
                        ' . $option . '
                </select><br />
        </p>

     
        

         <p class="meta-options">
                <label for="embed_video_id">'.__('Embed Video id: ','wpestate').'</label><br />     
                <input type="text" id="embed_video_id" name="embed_video_id" value="'.esc_html( get_post_meta($post->ID, 'embed_video_id', true) ).'">
          </p>';
    
    
    
        $option_video='';
        $video_values = array('vimeo', 'youtube');
        $video_type = get_post_meta($post->ID, 'embed_video_type', true);

        foreach ($video_values as $value) {
            $option_video.='<option value="' . $value . '"';
            if ($value == $video_type) {
                $option_video.='selected="selected"';
            }
            $option_video.='>' . $value . '</option>';
        }
      print '
       <p class="meta-options">
                <label for="embed_video_type">'.__('Video from ','wpestate').'</label><br />
                 <select id="embed_video_type" name="embed_video_type" style="width: 200px;">
                        ' . $option_video . '
                </select><br />
   
        </p>
         
        ';
}



/* Prints the box content */

function estate_sidebar_box($post) {

    // Use nonce for verification
    wp_nonce_field(plugin_basename(__FILE__), 'wpestate_sidebar_noncename');
    global $post;
    global $wp_registered_sidebars;
    $sidebar_name   = get_post_meta($post->ID, 'sidebar_select', true);
    $sidebar_option = get_post_meta($post->ID, 'sidebar_option', true);
    $sidebar_values = array('right', 'left', 'none');
    $option         = '';

    foreach ($sidebar_values as $value) {
        $option.='<option value="' . $value . '"';
        if ($value == $sidebar_option) {
            $option.=' selected="selected"';
        }
        $option.='>' . $value . '</option>';
    }

    print '   
    <p class="meta-options"><label for="sidebar_option">'.__('Where to show the sidebar: ','wpestate').' </label><br />
        <select id="sidebar_option" name="sidebar_option" style="width: 200px;">
        ' . $option . '
        </select>
    </p>';
    
    print'
    <p class="meta-options"><label for="sidebar_select">'.__('Select the sidebar: ','wpestate').'</label><br />                  
        <select name="sidebar_select" id="sidebar_select" style="width: 200px;">';
        foreach ($GLOBALS['wp_registered_sidebars'] as $sidebar) {
            print'<option value="' . ($sidebar['id'] ) . '"';
            if ($sidebar_name == $sidebar['id']) {
                print' selected="selected"';
            }
            print' >' . ucwords($sidebar['name']) . '</option>';
        }
        print '
        </select>
    </p>';
}





/* When the post is saved, saves our custom data */

function estate_save_postdata($post_id) {
  
    global $post;
    //$post_ID = $_POST['post_ID'];
   // $post_ID = $post->ID;
    // Skip auto save
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return $post_id;
    }


    // Check permissions
    
    if(isset($_POST['post_type'])){       
            if ('page' == $_POST['post_type'] or 'post' == $_POST['post_type'] or 'estate_property' == $_POST['post_type']) {
                if (!current_user_can('edit_page', $post_id))
                    return;
            }
            else {
                if (!current_user_can('edit_post', $post_id))
                    return;
            }
     }


    foreach ($_POST as $key => $value) {
        //print 'fac update '.$key.' --- '.$value.'</br>';
        if( !is_array ($value) ){
            $postmeta = sanitize_text_field( $value ); 
            update_post_meta($post_id, $key,$postmeta );
        }
  
    }
    
      //////////////////////////////////////////////////////////////////
    //// change listing author id
    //////////////////////////////////////////////////////////////////
    if ( isset($_POST['property_user'])){
        $current_id = wpsestate_get_author($post_id);
        $new_user=intval($_POST['property_user']);
        
        if($current_id != $new_user ){
            // change author
            $post = array(
                'ID'            => $post_id,
                'post_author'   => $new_user
            );

            wp_update_post($post ); 
        }
        
    }
    ///////////////////////////// end change author id
 

    
}



    
?>