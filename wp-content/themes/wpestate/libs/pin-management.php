<?php

   
////////////////////////////////////////////////////////////////////////////////
/// google map functions - contact pin array creation
////////////////////////////////////////////////////////////////////////////////  
function contact_pin(){
        $place_markers=array();
       
        
        $company_name=  stripslashes( esc_html( get_option('wp_estate_company_name','') ) );
        if($company_name==''){
            $company_name='Company Name';
        }

        $place_markers[0]    =   $company_name;
        $place_markers[1]    =   '';
        $place_markers[2]    =   '';
        $place_markers[3]    =   1;
        $place_markers[4]    =   esc_html(get_option('wp_estate_company_contact_image', '') );
        $place_markers[5]    =   '0';
        $place_markers[6]    =   'address';
        $place_markers[7]    =   'none';
        $place_markers[8]    =   '';
       /*  */
        return json_encode($place_markers);
}    






////////////////////////////////////////////////////////////////////////////////
/// google map functions - pin array creation
////////////////////////////////////////////////////////////////////////////////
if (!function_exists('listing_pins')):
function listing_pins(){

    $counter=0;
    $unit                       =   get_option('wp_estate_measure_sys','');
    $currency                   =   get_option('wp_estate_currency_symbol','');
    $where_currency             =   get_option('wp_estate_where_currency_symbol', '');
    $cache                      =   get_option('wp_estate_cache','');
    $place_markers=$markers     =   array();
    $meta_query=array();

    $compare_array['key']        = 'property_latitude';
    $compare_array['value']      = '';
    $compare_array['type']       = 'CHAR';
    $compare_array['compare']    = '!=';
    $meta_query[]                = $compare_array;


    $compare_array['key']        = 'property_longitude';
    $compare_array['value']      = '';
    $compare_array['type']       = 'CHAR';
    $compare_array['compare']    = '!=';
    $meta_query[]                = $compare_array;
        
    if($cache=='yes'){
        if(!get_transient('prop_list_cached')) { 
        $args = array(
                'post_type'         =>  'estate_property',
                'nopaging'          =>  'true',
                 'meta_query'       =>  $meta_query,
                );		

        $prop_selection = new WP_Query($args);
        set_transient('prop_list_cached', $prop_selection, 60 * 60 * 3);//store data for 3h 
        }else{
            $prop_selection =get_transient('prop_list_cached');// retrive cached data
        }
        wp_reset_query(); 
    }
    else{  
        $args = array(
                  'post_type'       =>  'estate_property',
                   'nopaging'       =>  'true',
                   'meta_query'     =>  $meta_query,
                   );		
        $prop_selection = new WP_Query($args);
        wp_reset_query(); 
    }//end cache



        while($prop_selection->have_posts()): $prop_selection->the_post();

               $the_id      =   get_the_ID();
               ////////////////////////////////////// gathering data for markups
               $gmap_lat    =   esc_html(get_post_meta($the_id, 'property_latitude', true));
               $gmap_long   =   esc_html(get_post_meta($the_id, 'property_longitude', true));

               //////////////////////////////////////  get property type
               $slug        =   array();
               $prop_type   =   array();
               $types       =   get_the_terms($the_id,'property_category' );
               $types_act   =   get_the_terms($the_id,'property_action_category' );

              
        
                   if ( $types && ! is_wp_error( $types ) ) { 
                         foreach ($types as $single_type) {
                            $prop_type[] = $single_type->slug;
                            $slug=$single_type->slug;
                           }
                    
                    $single_first_type= $prop_type[0];   
                    }else{
                          $single_first_type='';
                    }
                 //  $single_first_type=urldecode($single_first_type);


                    ////////////////////////////////////// get property action
                    $prop_action=array();
                    if ( $types_act && ! is_wp_error( $types_act ) ) { 
                          foreach ($types_act as $single_type) {
                            $prop_action[] = $single_type->slug;
                            $slug=$single_type->slug;
                           }
                    $single_first_action= $prop_action[0];
                    }else{
                        $single_first_action='';
                    }
                    
                  // $single_first_action=urldecode($single_first_action);
//   $pin=strtolower($single_first_type.$single_first_action);

                    // composing name of the pin
                    if($single_first_type=='' || $single_first_action ==''){
                          $pin                   =   limit54($single_first_type.$single_first_action);
                    }else{
                          $pin                   =   limit27($single_first_type).limit27($single_first_action);
                    }
                    
                    $counter++;

                    //// get price
                    $price          =   intval   ( get_post_meta($the_id, 'property_price', true) );
                    $price_label    =   esc_html ( get_post_meta($the_id, 'property_label', true) );
                    
                    if($price==0){
                        $price='';                        
                    }else{
                        $price=number_format($price);

                        if($where_currency=='before'){
                             $price=' - '.$currency.' '.$price;
                         }else{
                             $price=' - '.$price.' '.$currency;
                         }
                        $price=$price.' '.$price_label;
                    }
                    
                    		
                    
                    $place_markers=array();

                    $place_markers[]    = rawurlencode (get_the_title());
                    $place_markers[]    = $gmap_lat;
                    $place_markers[]    = $gmap_long;
                    $place_markers[]    = $counter;
                    $place_markers[]    = rawurlencode  (get_the_post_thumbnail($the_id,'property_map') );
                    $place_markers[]    = rawurlencode ( $price );
                    $place_markers[]    = rawurlencode ($single_first_type);
                    $place_markers[]    = rawurlencode ($single_first_action);
                    $place_markers[]    = rawurlencode ($pin);
                    $place_markers[]    = rawurlencode (get_permalink());
                    $place_markers[]    = $the_id;
                     $markers[]=$place_markers;
                            

        endwhile; 
        wp_reset_query(); 
  
        return json_encode($markers);
}

endif;

////////////////////////////////////////////////////////////////////////////////
/// google map functions -custom pin array creation - for adv search
////////////////////////////////////////////////////////////////////////////////
if (!function_exists('custom_listing_pins')):
function custom_listing_pins($args){

    $counter=0;
    $unit                       =   get_option('wp_estate_measure_sys','');
    $currency                   =   get_option('wp_estate_currency_symbol','');
    $where_currency             =   get_option('wp_estate_where_currency_symbol', '');
    $cache                      =   get_option('wp_estate_cache','');
    $place_markers=$markers     =   array();

  
     	
        $prop_selection = new WP_Query($args);
      

        while($prop_selection->have_posts()): $prop_selection->the_post();

               $the_id      =   get_the_ID();
               ////////////////////////////////////// gathering data for markups
               $gmap_lat    =   esc_html(get_post_meta($the_id, 'property_latitude', true));
               $gmap_long   =   esc_html(get_post_meta($the_id, 'property_longitude', true));

               //////////////////////////////////////  get property type
               $slug        =   array();
               $prop_type   =   array();
               $types       =   get_the_terms($the_id,'property_category' );
               $types_act   =   get_the_terms($the_id,'property_action_category' );

              
        
                   if ( $types && ! is_wp_error( $types ) ) { 
                         foreach ($types as $single_type) {
                            $prop_type[] = $single_type->slug;
                            $slug=$single_type->slug;
                           }
                    
                    $single_first_type= $prop_type[0];   
                    }else{
                          $single_first_type='';
                    }
                   


                    ////////////////////////////////////// get property action
                    $prop_action=array();
                    if ( $types_act && ! is_wp_error( $types_act ) ) { 
                          foreach ($types_act as $single_type) {
                            $prop_action[] = $single_type->slug;
                            $slug=$single_type->slug;
                           }
                    $single_first_action= $prop_action[0];
                    }else{
                        $single_first_action='';
                    }
                    
                   


                    // composing name of the pin
                   
                    if($single_first_type=='' || $single_first_action ==''){
                          $pin                   =   limit54($single_first_type.$single_first_action);
                    }else{
                          $pin                   =   limit27($single_first_type).limit27($single_first_action);
                    }
                  
            
                    $counter++;

                    //// get price
                    $price          =   intval   ( get_post_meta($the_id, 'property_price', true) );
                    $price_label    =   esc_html ( get_post_meta($the_id, 'property_label', true) );
                    
                    if($price==0){
                        $price='';                        
                    }else{
                        $price=number_format($price);

                        if($where_currency=='before'){
                             $price=' - '.$currency.' '.$price;
                         }else{
                             $price=' - '.$price.' '.$currency;
                         }
                        $price=$price.' '.$price_label;
                    }
                    
                    		
                    
                    $place_markers=array();

                    $place_markers[]    = rawurlencode(get_the_title());
                    $place_markers[]    = $gmap_lat;
                    $place_markers[]    = $gmap_long;
                    $place_markers[]    = $counter;
                    $place_markers[]    = rawurlencode (get_the_post_thumbnail($the_id,'property_map'));
                    $place_markers[]    = rawurlencode ($price);
                    $place_markers[]    = rawurlencode ($single_first_type);
                    $place_markers[]    = rawurlencode ($single_first_action);
                    $place_markers[]    = rawurlencode ($pin);
                    $place_markers[]    = rawurlencode ( get_permalink());
                    $place_markers[]    = $the_id;

                     $markers[]=$place_markers;
                            

        endwhile;  
        wp_reset_query(); 

        return json_encode($markers);
}


endif;
 
////////////////////////////////////////////////////////////////////////////////
/// google map functions - pin Images array creation
////////////////////////////////////////////////////////////////////////////////
function pin_images(){
    $pins=array();
    $taxonomy = 'property_action_category';
    $tax_terms = get_terms($taxonomy);

    $taxonomy_cat = 'property_category';
    $categories = get_terms($taxonomy_cat);
    
     foreach ($tax_terms as $tax_term) {
        $name                    =  limit64('wp_estate_'.$tax_term->slug);
        $limit54                 =  limit54($tax_term->slug);
        $pins[$limit54]          =  esc_html( get_option($name) ); 
    }
    
    foreach ($categories as $categ) {
        $name                           =   limit64('wp_estate_'.$categ->slug);
        $limit54                        =   limit54($categ->slug);
        $pins[$limit54]                 =   esc_html( get_option($name) );
    }
    

    foreach ($tax_terms as $tax_term) {
        foreach ($categories as $categ) {           
            $limit54                    =   limit27($categ->slug).limit27($tax_term->slug);
            $name                       =   'wp_estate_'.$limit54;
            $pins[$limit54]              =   esc_html( get_option($name) ) ;  
        }
    }
    
    $name='wp_estate_idxpin';
    $pins['idxpin']=esc_html( get_option($name) );  
    
    $name='wp_estate_userpin';
    $pins['userpin']=esc_html( get_option($name) );  
    

    return json_encode($pins);
}

function limit64($stringtolimit){
    return substr($stringtolimit,0,64);
}

function limit54($stringtolimit){
    return substr($stringtolimit,0,54);
}

function limit50($stringtolimit){ // 14
    return substr($stringtolimit,0,50);
}

function limit45($stringtolimit){ // 19
    return substr($stringtolimit,0,45);
}                                   

function limit27($stringtolimit){ // 27
    return substr($stringtolimit,0,27);
}    


////////////////////////////////////////////////////////////////////////////////
/// icon functiosn - return array with icons
////////////////////////////////////////////////////////////////////////////////
function get_icons(){
    $icons          =   array();
    $taxonomy       =   'property_action_category';
    $tax_terms      =   get_terms($taxonomy);
    $taxonomy_cat   =   'property_category';
    $categories     =   get_terms($taxonomy_cat);

  
    // add only actions
    foreach ($tax_terms as $tax_term) {
       $icon_name   =   limit64( 'wp_estate_icon'.$tax_term->slug);
       $limit50     =   limit50( $tax_term->slug);
       $value       =    esc_html( get_option($icon_name) );
         
       if ( $value == ''){
          $icons[$limit50]  =    get_template_directory_uri().'/css/css-images/'.$tax_term->slug.'icon.png';  
       }else{
          $icons[$limit50]  =    $value;
       }
       
    }

    // add only categories
    foreach ($categories as $categ) { 
        $icon_name    =    limit64( 'wp_estate_icon'.$categ->slug);
        $value        =    esc_html( get_option($icon_name) );
        $limit50                        =   limit50( $categ->slug);
        
        if ( $value == ''){
          $icons[$limit50]  =    get_template_directory_uri().'/css/css-images/'.$categ->slug.'icon.png';  
        }else{
          $icons[$limit50]  =    $value;
        }
    } 


    return json_encode($icons);
}



////////////////////////////////////////////////////////////////////////////////
/// hover icon functiosn - return array with icons
////////////////////////////////////////////////////////////////////////////////
function get_hover_icons(){
    $icons          =   array();
    $taxonomy       =   'property_action_category';
    $tax_terms      =   get_terms($taxonomy);
    $taxonomy_cat   =   'property_category';
    $categories     =   get_terms($taxonomy_cat);

    // add only actions
    foreach ($tax_terms as $tax_term) {
       $hover_icon_name     =   limit64( 'wp_estate_hovericon'.$tax_term->slug);
       $limit50             =   limit50 ($tax_term->slug);
       $value               =   esc_html( get_option($hover_icon_name) );
       
       if ( $value == ''){
          $icons[$limit50]  =    get_template_directory_uri().'/css/css-images/'.$tax_term->slug.'iconhover.png';  
       }else{
          $icons[$limit50]  =    $value;
       }
       
    }

    //
    // add only categories
    foreach ($categories as $categ) {
       
       $hover_icon_name     =   limit64( 'wp_estate_hovericon'.$categ->slug);
       $limit50             =   limit50( $categ->slug);
       $value               =   esc_html( get_option($hover_icon_name) );
        
       
        if ( $value == ''){
          $icons[$limit50]  =    get_template_directory_uri().'/css/css-images/'.$categ->slug.'iconhover.png';  
        }else{
          $icons[$limit50]  =    $value;
        }
    } 
       

    return json_encode($icons);
}


?>
