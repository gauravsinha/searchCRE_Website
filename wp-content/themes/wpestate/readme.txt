Theme Name: Wp Estate
Theme URI: http://wpestate.org/
Author: Ana Maria - annapx0909@gmail.com
Author URI: 
Description:WP Estate is a premium fully responsive WordPress theme designed for Real Estate companies and independent agents.
Version: 1.1
License: 
License URI:
Text Domain: wpestate
Tags: white, one-column, two-columns,left-sidebar, right-sidebar, flexible-width, custom-menu, theme-options, translation-ready

For help files go here http://help.wpestatetheme.org/