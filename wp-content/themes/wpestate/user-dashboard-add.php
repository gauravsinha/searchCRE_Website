<?php
// Template Name: User Dashboard Submit
// Wp Estate Pack
global $current_user;
$user_roles = $current_user->roles[0];
/*if ( !is_user_logged_in() && $user_roles!='condtributor' ) {   
     //wp_redirect( home_url('url')?$msg='login first' );
	  $login_first="<div style='padding-top:40px;padding-bottom:150px;font-weight:bold;font-size:18px;'>Please login/register first as a Broker</div>";
} elseif(is_user_logged_in() && $user_roles!='condtributor'){
	 $login_first="<div style='padding-top:40px;padding-bottom:150px;font-weight:bold;font-size:18px;'>Please login/register first as a Broker</div>";
	
} */
set_time_limit (600);


global $current_user;
get_currentuserinfo();
$userID                         =   $current_user->ID;
$user_pack                      =   get_the_author_meta( 'package_id' , $userID );
$status_values                  =   esc_html( get_option('wp_estate_status_list') );
$status_values_array            =   explode(",",$status_values);
$feature_list_array             =   array();
$feature_list                   =   esc_html( get_option('wp_estate_feature_list') );
$feature_list_array             =   explode( ',',$feature_list);


if( isset( $_GET['listing_edit'] ) && is_numeric( $_GET['listing_edit'] ) ){
    ///////////////////////////////////////////////////////////////////////////////////////////
    /////// If we have edit
    ///////////////////////////////////////////////////////////////////////////////////////////
    $edit_id                        =   $_GET['listing_edit'];
    $option_slider                  =   '';
    $the_post= get_post( $edit_id); 
    if( $current_user->ID != $the_post->post_author ) {
        exit('You don\'t have the rights to edit this');
    }
    $show_err                       =   '';
    $action                         =   'edit';
    $submit_title                   =   get_the_title($edit_id);
    $submit_description             =   get_post_field('post_content', $edit_id);
    
   

  
    $prop_category_array            =   get_the_terms($edit_id, 'property_category');
    if(isset($prop_category_array[0])){
         $prop_category_selected   =   $prop_category_array[0]->term_id;
    }
    
    $prop_action_category_array     =   get_the_terms($edit_id, 'property_action_category');
    if(isset($prop_action_category_array[0])){
        $prop_action_category_selected           =   $prop_action_category_array[0]->term_id;
    }
   
    
    $property_city_array            =   get_the_terms($edit_id, 'property_city');
    if(isset($property_city_array [0])){
          $property_city                  =   $property_city_array [0]->name;
    }
    
    $property_area_array            =   get_the_terms($edit_id, 'property_area');
    if(isset($property_area_array [0])){
        $property_area                  =   $property_area_array [0]->name;
    }
  
    $property_address               =   esc_html( get_post_meta($edit_id, 'property_address', true) );
    $property_county                =   esc_html( get_post_meta($edit_id, 'property_county', true) );
    $property_state                 =   esc_html( get_post_meta($edit_id, 'property_state', true) );
    $property_zip                   =   esc_html( get_post_meta($edit_id, 'property_zip', true) );
    $country_selected               =   esc_html( get_post_meta($edit_id, 'property_country', true) );
    $prop_stat                      =   esc_html( get_post_meta($edit_id, 'property_status', true) );
    $property_status                =   '';



   foreach ($status_values_array as $key=>$value) {
        $value = trim($value);
        $value_wpml=$value;
        $slug_status=sanitize_title($value);
        if (function_exists('icl_translate') ){
            $value_wpml= icl_translate('wpestate','wp_estate_property_status_front_'.$slug_status,$value );
        }

        $property_status.='<option value="' . $value . '"';
        if ($value == $prop_stat) {
            $property_status.='selected="selected"';
        }
        $property_status.='>' . $value_wpml . '</option>';
    }
    
    $property_price                 =   intval   ( get_post_meta($edit_id, 'property_price', true) );
    $price_per_value                =   esc_html ( get_post_meta($edit_id, 'price_per', true));
    $guest_no                       =   esc_html ( get_post_meta($edit_id, 'guest_no', true));   
    $cleaning_fee                   =   floatval ( get_post_meta($edit_id, 'cleaning_fee', true)) ;
    $city_fee                       =   floatval ( get_post_meta($edit_id, 'city_fee', true)) ;
    if( isset($_POST['property_label']) ){
        $property_label                 =   sanitize_text_field( $_POST['property_label']);    
    }else{
        $property_label='';
    }
    $property_size                  =   intval   ( get_post_meta($edit_id, 'property_size', true) ); 
    $property_lot_size              =   intval   ( get_post_meta($edit_id, 'property_lot_size', true) );
    $property_rooms                 =   intval   ( get_post_meta($edit_id, 'property_rooms', true) );
    $property_bedrooms              =   intval   ( get_post_meta($edit_id, 'property_bedrooms', true) ); 
    $property_bathrooms             =   intval   ( get_post_meta($edit_id, 'property_bathrooms', true) );
    $option_video                   =   '';
    $video_values                   =   array('vimeo', 'youtube');
    $video_type                     =   esc_html ( get_post_meta($edit_id, 'embed_video_type', true) ); 
    $google_camera_angle            =   intval   ( get_post_meta($edit_id, 'google_camera_angle', true) );

    foreach ($video_values as $value) {
        $option_video.='<option value="' . $value . '"';
        if ($value == $video_type) {
            $option_video.='selected="selected"';
        }
        $option_video.='>' . $value . '</option>';
    }
    
    $slider_values = array
                        (   __('full top slider','wpestate')    =>  '1', 
                            __('small slider','wpestate')       =>  '2'
                        );
    
    $slider_type = get_post_meta($edit_id, 'prop_slider_type', true);

   foreach ($slider_values as $key => $value) {
       $label_slider=$key;
       $slug_slider=sanitize_title($key);
       if(function_exists('icl_translate')){
            $label_slider=  icl_translate('wpestate','wp_estate_property_slider'.$slug_slider, $value );
       }

       $option_slider.='<option value="' . $value . '"';
       if ($value == $slider_type) {
           $option_slider.='selected="selected"';
       }
       $option_slider.='>' . $label_slider . '</option>';
   }

    $embed_video_id                 =   esc_html( get_post_meta($edit_id, 'embed_video_id', true) );
    $property_latitude              =   esc_html( get_post_meta($edit_id, 'property_latitude', true)); 
    $property_longitude             =   esc_html( get_post_meta($edit_id, 'property_longitude', true));
    $google_view                    =   intval( get_post_meta($edit_id, 'property_google_view', true) );

    if($google_view==1){
        $google_view_check  =' checked="checked" ';
    }else{
         $google_view_check =' ';
    }
    
    
    $prop_featured                  =   intval( get_post_meta($edit_id, 'prop_featured', true) );
     if($prop_featured==1){
        $prop_featured_check    =' checked="checked" ';
    }else{
         $prop_featured_check   =' ';
    }
    
    $google_camera_angle            =   intval( get_post_meta($edit_id, 'google_camera_angle', true) );; 
   
    //  custom fields
    $custom_fields = get_option( 'wp_estate_custom_fields', true);  
    $custom_fields_array=array();
    $i=0;
    if(!empty($custom_fields)){
        while($i< count($custom_fields) ){
           $name =   $custom_fields[$i][0];
           $type =   $custom_fields[$i][2];
           $slug =   str_replace(' ','_',$name);
           $custom_fields_array[$slug]=esc_html(get_post_meta($edit_id, $slug, true));
           $i++;
        }
    }
  

     

}else{ 
    
    ///////////////////////////////////////////////////////////////////////////////////////////
    /////// If default view
    ///////////////////////////////////////////////////////////////////////////////////////////
    $action                         =   'view';
    $submit_title                   =   ''; 
    $submit_description             =   ''; 
    $prop_category                  =   ''; 
    $property_address               =   ''; 
    $property_county                =   ''; 
    $property_state                 =   ''; 
    $property_zip                   =   ''; 
    $country_selected               =   ''; 
    $prop_stat                      =   ''; 
    $property_status                =   '';
    $property_price                 =   ''; 
    $price_per_value                =   '';
    $guest_no                       =   '';
    $cleaning_fee                   =   '';
    $city_fee                       =   '';
    $property_label                 =   '';   
    $property_size                  =   ''; 
    $property_lot_size              =   ''; 
    $property_rooms                 =   ''; 
    $property_bedrooms              =   ''; 
    $property_bathrooms             =   ''; 
    $option_video                   =   '';
    $option_slider                  =   '';
    $video_type                     =   '';  
    $embed_video_id                 =   ''; 
    $property_latitude              =   ''; 
    $property_longitude             =   '';  
    $google_view                    =   ''; 
    $prop_featured                  =   '';
    $google_camera_angle            =   ''; 
    $prop_category                  =   '';   
    
   $edit_id='';
    $custom_fields = get_option( 'wp_estate_custom_fields', true);    
    $custom_fields_array=array();
    $i=0;
    
    if( !empty($custom_fields) ){
        while($i< count($custom_fields) ){
           $name =   $custom_fields[$i][0];
           $type =   $custom_fields[$i][2];
           $slug =   str_replace(' ','_',$name);
           $custom_fields_array[$slug]='';
           $i++;
        }
    }
        

    




    foreach ($status_values_array as $key=>$value) {
        $value = trim($value);
        $value_wpml=$value;
        $slug_status=sanitize_title($value);
        if (function_exists('icl_translate') ){
            $value_wpml= icl_translate('wpestate','wp_estate_property_status_front_'.$slug_status,$value );
        }

        $property_status.='<option value="' . $value . '"';
        if ($value == $prop_stat) {
            $property_status.='selected="selected"';
        }
        $property_status.='>' . $value_wpml . '</option>';
    }

    $video_values                   =   array('vimeo', 'youtube');
    foreach ($video_values as $value) {
      $option_video.='<option value="' . $value . '"';
      $option_video.='>' . $value . '</option>';
    }    
    
    $option_slider='';
    $slider_values = array
                    (   __('full top slider','wpestate')    =>  '1', 
                        __('small slider','wpestate')       =>  '2'
                    );
    
    $slider_type = get_post_meta($edit_id, 'prop_slider_type', true);

    foreach ($slider_values as $key => $value) {
       $label_slider=$key;
       $slug_slider=sanitize_title($key);
       if(function_exists('icl_translate')){
            $label_slider=  icl_translate('wpestate','wp_estate_property_slider'.$slug_slider, $value );
       }

       $option_slider.='<option value="' . $value . '"';
       if ($value == $slider_type) {
           $option_slider.='selected="selected"';
       }
       $option_slider.='>' . $label_slider . '</option>';
   }
}


 

      

        
    


///////////////////////////////////////////////////////////////////////////////////////////
/////// Submit Code
///////////////////////////////////////////////////////////////////////////////////////////

if( 'POST' == $_SERVER['REQUEST_METHOD'] && $_POST['action']=='view' ) {
     
     $paid_submission_status    = esc_html ( get_option('wp_estate_paid_submission','') );
     
    if ( $paid_submission_status!='membership' || ( $paid_submission_status== 'membership' || get_current_user_listings($userID) > 0)  ){ // if user can submit
        
        if ( !isset($_POST['new_estate']) || !wp_verify_nonce($_POST['new_estate'],'submit_new_estate') ){
           exit('Sorry, your not submiting from site'); 
        }
   
        if( !isset($_POST['prop_category']) ) {
            $prop_category=0;           
        }else{
            $prop_category  =   intval($_POST['prop_category']);
        }
  
        if( !isset($_POST['prop_action_category']) ) {
            $prop_action_category=0;           
        }else{
            $prop_action_category  =   sanitize_text_field($_POST['prop_action_category']);
        }
        
        if( !isset($_POST['property_city']) ) {
            $property_city='';           
        }else{
            $property_city  =   sanitize_text_field($_POST['property_city']);
        }
        
        if( !isset($_POST['property_area']) ) {
            $property_area='';           
        }else{
            $property_area  =   sanitize_text_field($_POST['property_area']);
        }
       
        $show_err                       =   '';
        $post_id                        =   '';
        $submit_title                   =   sanitize_text_field( $_POST['wpestate_title'] ); 
        $submit_description             =   wp_filter_nohtml_kses( $_POST['wpestate_description']);     
        $property_address               =   sanitize_text_field( $_POST['property_address']);
        $property_county                =   sanitize_text_field( $_POST['property_county']);
        $property_state                 =   sanitize_text_field( $_POST['property_state']);
        $property_zip                   =   sanitize_text_field( $_POST['property_zip']);
        $country_selected               =   sanitize_text_field( $_POST['property_country']);     
        $prop_stat                      =   sanitize_text_field( $_POST['property_status']);
        $property_status                =   '';
        
        foreach ($status_values_array as $key=>$value) {
            $value = trim($value);
            $value_wpml=$value;
            $slug_status=sanitize_title($value);
            if (function_exists('icl_translate') ){
                $value_wpml= icl_translate('wpestate','wp_estate_property_status_front_'.$slug_status,$value );
            }

            $property_status.='<option value="' . $value . '"';
            if ($value == $prop_stat) {
                $property_status.='selected="selected"';
            }
            $property_status.='>' . $value_wpml . '</option>';
        }

        if(isset($_POST['property_price'])){
            $property_price                 =   sanitize_text_field( $_POST['property_price']);
        }else{
            $property_price=0;
        }
        
        if(isset($_POST['price_per'])){
            $price_per_value                =   sanitize_text_field( $_POST['price_per']);
        }else{
            $price_per_value='';
        }
        
        if(isset($_POST['guest_no'])){
            $guest_no                       =   sanitize_text_field( $_POST['guest_no']);
        }else{
            $guest_no='';
        }
        
        if(isset($_POST['cleaning_fee'])){
            $cleaning_fee                   =   sanitize_text_field( $_POST['cleaning_fee']);
        }else{
            $cleaning_fee='';
        }
        
        if(isset($_POST['city_fee'])){
            $city_fee                       =   sanitize_text_field( $_POST['city_fee']);
        }else{
            $city_fee='';
        }
        
        if( isset($_POST['property_label']) ){
            $property_label                 =   sanitize_text_field( $_POST['property_label']);    
        }else{
            $property_label='';
        }
        $property_size                  =   sanitize_text_field( $_POST['property_size']);  
        $property_lot_size              =   sanitize_text_field( $_POST['property_lot_size']); 
        //$property_year                  =   sanitize_text_field( $_POST['property_year']); 
        $property_rooms                 =   sanitize_text_field( $_POST['property_rooms']); 
        $property_bedrooms              =   sanitize_text_field( $_POST['property_bedrooms']); 
        $property_bathrooms             =   sanitize_text_field( $_POST['property_bathrooms']); 
 
        $option_video                   =   '';
        $video_values                   =   array('vimeo', 'youtube');
        $video_type                     =   sanitize_text_field( $_POST['embed_video_type']); 
        $google_camera_angle            =   sanitize_text_field( $_POST['google_camera_angle']); 
        $has_errors                     =   false;
        $errors                         =   array();
        
        
        
        $moving_array=array();
        foreach($feature_list_array as $key => $value){
          $post_var_name    =   str_replace(' ','_', trim($value) );
          $feature_value    =   sanitize_text_field( $_POST[$post_var_name] );
          
          if($feature_value==1){
               $moving_array[]=$post_var_name;
          }        
       }
        
      
        foreach ($video_values as $value) {
            $option_video.='<option value="' . $value . '"';
            if ($value == $video_type) {
                $option_video.='selected="selected"';
            }
            $option_video.='>' . $value . '</option>';
        }
        
        $slider_type                    =   intval( $_POST['prop_slider_type']);
        $embed_video_id                 =   sanitize_text_field( $_POST['embed_video_id']); 
        $property_latitude              =   sanitize_text_field( $_POST['property_latitude']); 
        $property_longitude             =   sanitize_text_field( $_POST['property_longitude']); 
        $google_view                    =   sanitize_text_field( $_POST['property_google_view']); 

        if($google_view==1){
            $google_view_check=' checked="checked" ';
        }else{
             $google_view_check=' ';
        }

        if(isset($_POST['prop_featured'])){
            $prop_featured                  =    intval( $_POST['prop_featured']); ;
            if($prop_featured==1){
                 $prop_featured_check    =' checked="checked" ';
            }else{
                 $prop_featured_check   =' ';
            }  
        }
      
        
        $google_camera_angle            =   intval( $_POST['google_camera_angle']); 
        $prop_category                  =   get_term( $prop_category, 'property_category');
        $prop_category_selected         =   $prop_category->term_id;
        $prop_action_category           =   get_term( $prop_action_category, 'property_action_category');     
        $prop_action_category_selected  =   $prop_action_category->term_id;
        
        // save custom fields
     
        $i=0;
        if(!empty($custom_fields)){
           while($i< count($custom_fields) ){
            $name =   $custom_fields[$i][0];
            $type =   $custom_fields[$i][1];
            $slug =   str_replace(' ','_',$name);
            $custom_fields_array[$slug]= sanitize_text_field( $_POST[$slug]); ;
            $i++;
           } 
        }
      
            
            
        if($submit_title==''){
            $has_errors=true;
            $errors[]=__('Please submit a title for your property','wpestate');
        }
        
        if($submit_description==''){
            $has_errors=true;
            $errors[]=__('*Please submit a description for your property','wpestate');
        }
        
        if ($_POST['attachid']==''){
            $has_errors=true;
            $errors[]=__('*Please submit an image for your property','wpestate'); 
        }
        
        
        
        if($property_address==''){
            $has_errors=true;
            $errors[]=__('*Please submit an address for your property','wpestate');
        }

        if($has_errors){
            foreach($errors as $key=>$value){
                $show_err.=$value.'</br>';
            }
            
        }else{
            $paid_submission_status = esc_html ( get_option('wp_estate_paid_submission','') );
            $new_status             = 'pending';
            
            $admin_submission_status= esc_html ( get_option('wp_estate_admin_submission','') );
            if($admin_submission_status=='no' && $paid_submission_status!='per listing'){
               $new_status='publish';  
            }
            
            $post = array(
                'post_title'	=> $submit_title,
                'post_content'	=> $submit_description,
                'post_status'	=> $new_status, 
                'post_type'     => 'estate_property' ,
                'post_author'   => $current_user->ID 
            );
            $post_id =  wp_insert_post($post );  
          
            if( $paid_submission_status == 'membership'){ // update pack status
                update_listing_no($current_user->ID);                
                if($prop_featured==1){
                    update_featured_listing_no($current_user->ID); 
                }
               
            }
           
        }
        
      

        
        
        ////////////////////////////////////////////////////////////////////////
        // Upload images
        ////////////////////////////////////////////////////////////////////////
        if($post_id) {

                
            $attchs=explode(',',$_POST['attachid']);
            $last_id='';
            foreach($attchs as $att_id){
                if( !is_numeric($att_id) ){
                 
                }else{
                    if($last_id==''){
                        $last_id=  $att_id;  
                    }
                    wp_update_post( array(
                                'ID' => $att_id,
                                'post_parent' => $post_id
                            ));
                        
                    
                }
            }
            if( is_numeric($_POST['attachthumb']) && $_POST['attachthumb']!=''  ){
                set_post_thumbnail( $post_id, $_POST['attachthumb'] ); 
            }else{
                set_post_thumbnail( $post_id, $last_id );                
            }
            
            
            if( isset($prop_category->name) ){
                 wp_set_object_terms($post_id,$prop_category->name,'property_category'); 
            }  
            if ( isset ($prop_action_category->name) ){
                 wp_set_object_terms($post_id,$prop_action_category->name,'property_action_category'); 
            }  
            if( isset($property_city) ){
                   wp_set_object_terms($post_id,$property_city,'property_city'); 
            }  
            if( isset($property_area) ){
                   wp_set_object_terms($post_id,$property_area,'property_area'); 
            }  
   
      
            update_post_meta($post_id, 'property_address', $property_address);
            update_post_meta($post_id, 'property_area', $property_area);
            update_post_meta($post_id, 'property_county', $property_county);
            update_post_meta($post_id, 'property_state', $property_state);
            update_post_meta($post_id, 'property_zip', $property_zip);
            update_post_meta($post_id, 'property_country', $country_selected);
            update_post_meta($post_id, 'property_size', $property_size);
            update_post_meta($post_id, 'property_lot_size', $property_lot_size);  
            update_post_meta($post_id, 'property_rooms', $property_rooms);  
            update_post_meta($post_id, 'property_bedrooms', $property_bedrooms);
            update_post_meta($post_id, 'property_bathrooms', $property_bathrooms);
            update_post_meta($post_id, 'property_status', $prop_stat);
            update_post_meta($post_id, 'property_price', $property_price);
              
            update_post_meta($post_id, 'price_per_value', $price_per_value);
            update_post_meta($post_id, 'guest_no', $guest_no);
            update_post_meta($post_id, 'city_fee', $city_fee);
            update_post_meta($post_id, 'cleaning_fee', $cleaning_fee);
    
            update_post_meta($post_id, 'property_label', $property_label);
            update_post_meta($post_id, 'embed_video_type', $video_type);
            update_post_meta($post_id, 'prop_slider_type', $slider_type);
            update_post_meta($post_id, 'embed_video_id',  $embed_video_id );
            update_post_meta($post_id, 'property_latitude', $property_latitude);
            update_post_meta($post_id, 'property_longitude', $property_longitude);
            update_post_meta($post_id, 'prop_featured', $prop_featured);
            update_post_meta($post_id, 'property_google_view',  $google_view);
            update_post_meta($post_id, 'google_camera_angle', $google_camera_angle);
            update_post_meta($post_id, 'pay_status', 'not paid');
            
            if('yes' ==  esc_html ( get_option('wp_estate_user_agent','') )){
                $user_id_agent            =   get_the_author_meta( 'user_agent_id' , $current_user->ID  );
                update_post_meta($post_id, 'property_agent', $user_id_agent);                
            }
           
            // save custom fields
            $custom_fields = get_option( 'wp_estate_custom_fields', true);  
     
            $i=0;
            if(!empty($custom_fields)){
                while($i< count($custom_fields) ){
                   $name =   $custom_fields[$i][0];
                   $type =   $custom_fields[$i][2];
                   $slug =   str_replace(' ','_',$name);

                   if($type=='numeric'){
                       $value_custom    =   floatval(sanitize_text_field( $_POST[$slug] ) );
                       update_post_meta($post_id, $slug, $value_custom);
                   }else{
                       $value_custom    =   esc_html(sanitize_text_field( $_POST[$slug] ) );
                       update_post_meta($post_id, $slug, $value_custom);
                   }
                   $custom_fields_array[$slug]= sanitize_text_field( $_POST[$slug]); ;
                   $i++;
                }
            }
           
            
            
            
            
            foreach($feature_list_array as $key => $value){
                $post_var_name  =   str_replace(' ','_', trim($value) );
                $feature_value  =   sanitize_text_field( $_POST[$post_var_name] );
                update_post_meta($post_id, $post_var_name, $feature_value);
                $moving_array[] =   $post_var_name;
            }
   
            // get user dashboard link
            $redirect = get_dashboard_link();
  
            $headers = 'From: No Reply <noreply@'.$_SERVER['HTTP_HOST'].'>' . "\r\n";
            $message  = __('Hi there,','wpestate') . "\r\n\r\n";
            $message .= sprintf( __("A user has submited a new property on  %s! You should go check it out.This is the property title: %s",'wpestate'), get_option('blogname'),$submit_title) . "\r\n\r\n";
 
            wp_mail(get_option('admin_email'),
		    sprintf(__('[%s] New Listing Submission','wpestate'), get_option('blogname')),
                    $message,
                    $headers);
            
            wp_reset_query();
            wp_redirect( $redirect);
            exit;
        }
        
        }//end if user can submit  
} // end post




///////////////////////////////////////////////////////////////////////////////////////////
/////// Edit Part Code
///////////////////////////////////////////////////////////////////////////////////////////


if( 'POST' == $_SERVER['REQUEST_METHOD'] && $_POST['action']=='edit' ) {
    
        if ( !isset($_POST['new_estate']) || !wp_verify_nonce($_POST['new_estate'],'submit_new_estate') ){
           exit('Sorry, your not submiting from site');
        } 
        
        $has_errors                     =   false;
        $show_err                       =   '';
        $edited                         =   0;
        $edit_id                        =   intval( $_POST['edit_id'] );
        $post                           =   get_post( $edit_id ); 
        $author_id                      =   $post->post_author ;
        if($current_user->ID !=  $author_id){
            exit('you don\'t have the rights to edit');
        }
        
        $images_todelete                =   sanitize_text_field( $_POST['images_todelete'] );
        $images_delete_arr              =   explode(',',$images_todelete);
        foreach ($images_delete_arr as $key=>$value){
             $img                       =   get_post( $value ); 
             $author_id                 =   $img->post_author ;
             if($current_user->ID !=  $author_id){
                exit('you don\'t have the rights to delete images');
             }else{
                  wp_delete_post( $value );   
             }
                      
        }
        
        if( !isset($_POST['prop_category']) ) {
            $prop_category=0;           
        }else{
            $prop_category  =   intval($_POST['prop_category']);
        }
    
        if( !isset($_POST['prop_action_category']) ) {
            $prop_action_category=0;           
        }else{
            $prop_action_category  =   sanitize_text_field($_POST['prop_action_category']);
        }
        
        if( !isset($_POST['property_city']) ) {
            $property_city=0;           
        }else{
            $property_city  =   sanitize_text_field($_POST['property_city']);
        }
        
        if( !isset($_POST['property_area']) ) {
            $property_area=0;           
        }else{
            $property_area  =   sanitize_text_field($_POST['property_area']);
        }
       
        
        
        $submit_title                   =   sanitize_text_field( $_POST['wpestate_title'] ); 
        $submit_description             =   wp_filter_nohtml_kses( $_POST['wpestate_description']);
        //$prop_category                  =   intval($_POST['prop_category']);//
        $property_address               =   sanitize_text_field( $_POST['property_address']);
        $property_county                =   sanitize_text_field( $_POST['property_county']);
        $property_state                 =   sanitize_text_field( $_POST['property_state']);
        $property_zip                   =   sanitize_text_field( $_POST['property_zip']);
        $country_selected               =   sanitize_text_field( $_POST['property_country']);     
        $prop_stat                      =   sanitize_text_field( $_POST['property_status']);
        $property_status                =   '';
        
        foreach ($status_values_array as $key=>$value) {
            $value = trim($value);
            $value_wpml=$value;
            $slug_status=sanitize_title($value);
            if (function_exists('icl_translate') ){
                $value_wpml= icl_translate('wpestate','wp_estate_property_status_front_'.$slug_status,$value );
            }

            $property_status.='<option value="' . $value . '"';
            if ($value == $prop_stat) {
                $property_status.='selected="selected"';
            }
            $property_status.='>' . $value_wpml . '</option>';
        }

        $property_price                 =   sanitize_text_field( $_POST['property_price']);
        if(isset($_POST['price_per'])){
            $price_per_value                =   sanitize_text_field( $_POST['price_per']);
        }else{
            $price_per_value  ='';
        }
        
        if(isset($_POST['guest_no'])){
            $guest_no                       =   sanitize_text_field( $_POST['guest_no']);
        }else{
            $guest_no  ='';
        }
        
        if(isset($_POST['cleaning_fee'])){
            $cleaning_fee                   =   sanitize_text_field( $_POST['cleaning_fee']);
        }else{
            $cleaning_fee  ='';
        }
        
        if(isset($_POST['city_fee'])){
            $city_fee                       =   sanitize_text_field( $_POST['city_fee']);   
        }else{
            $city_fee  ='';
        }
        
        
        if( isset($_POST['property_label']) ){
            $property_label                 =   sanitize_text_field( $_POST['property_label']);    
        }else{
            $property_label='';
        }
        $property_size                  =   sanitize_text_field( $_POST['property_size']);  
        $property_lot_size              =   sanitize_text_field( $_POST['property_lot_size']);  
        $property_rooms                 =   sanitize_text_field( $_POST['property_rooms']); 
        $property_bedrooms              =   sanitize_text_field( $_POST['property_bedrooms']); 
        $property_bathrooms             =   sanitize_text_field( $_POST['property_bathrooms']); 

        $option_video                   =   '';
        $video_values                   =   array('vimeo', 'youtube');
        $video_type                     =   sanitize_text_field( $_POST['embed_video_type']); 
        $google_camera_angle            =   sanitize_text_field( $_POST['google_camera_angle']); 

        foreach ($video_values as $value) {
            $option_video.='<option value="' . $value . '"';
            if ($value == $video_type) {
                $option_video.='selected="selected"';
            }
            $option_video.='>' . $value . '</option>';
        }
        
        $slider_type                    =   intval( $_POST['prop_slider_type']);
     

        $embed_video_id                 =   sanitize_text_field( $_POST['embed_video_id']); 
        $property_latitude              =   sanitize_text_field( $_POST['property_latitude']); 
        $property_longitude             =   sanitize_text_field( $_POST['property_longitude']); 
        $google_view                    =   sanitize_text_field( $_POST['property_google_view']); 

        if($google_view==1){
            $google_view_check=' checked="checked" ';
        }else{
             $google_view_check=' ';
        }
        
        $prop_featured                  =   intval( $_POST['prop_featured'] );
        if($prop_featured==1){
           $prop_featured_check    =' checked="checked" ';
        }else{
            $prop_featured_check   =' ';
        }

        $google_camera_angle            =   intval( $_POST['google_camera_angle']); 
        $prop_category                  =   get_term( $prop_category, 'property_category');
        $prop_action_category           =   get_term( $prop_action_category, 'property_action_category');     

      
     
        
        if($submit_title==''){
            $has_errors=true;
            $errors[]=__('Please submit a title for your property','wpestate');
        }
        
        if($submit_description==''){
            $has_errors=true;
            $errors[]=__('*Please submit a description for your property','wpestate');
        }
        
      
         if ($_POST['attachid']==''){
            $has_errors=true;
            $errors[]=__('*Please submit an image for your property','wpestate'); 
        }
        
        if($property_address==''){
            $has_errors=true;
            $errors[]=__('*Please submit an address for your property','wpestate');
        }
         
     
       if($has_errors){
            foreach($errors as $key=>$value){
                $show_err.=$value.'</br>';
            }
            
        }else{
            $new_status='pending';
            $admin_submission_status = esc_html ( get_option('wp_estate_admin_submission','') );
            $paid_submission_status  = esc_html ( get_option('wp_estate_paid_submission','') );
              
            if($admin_submission_status=='no' ){
               $new_status=get_post_status($edit_id);  
            }
            
            $post = array(
                    'ID'            => $edit_id,
                    'post_title'    => $submit_title,
                    'post_content'  => $submit_description,
                    'post_type'     => 'estate_property',
                    'post_status'   => $new_status
            );

            $post_id =  wp_update_post($post );  
            $edited=1;
        }
        
      
        


        if( $edited==1) {
           
            
            $attchs=explode(',',$_POST['attachid']);
            $last_id='';
         
            // check for deleted images
            $arguments = array(
                'numberposts'   => -1,
                'post_type'     => 'attachment',
                'post_parent'   => $edit_id,
                'post_status'   => null,
                'orderby'       => 'menu_order',
                'order'         => 'ASC'
            );
            $post_attachments = get_posts($arguments);
            $new_thumb=0;
            $curent_thumb = get_post_thumbnail_id($edit_id);
            foreach ($post_attachments as $attachment){
                if ( !in_array ($attachment->ID,$attchs) ){
                    wp_delete_post($attachment->ID);
                    if( $curent_thumb == $attachment->ID ){
                        $new_thumb=1;
                    }
                }
            }
            
            // check for deleted images
                   

             
            foreach($attchs as $att_id){
                if( !is_numeric($att_id) ){
                 
                }else{
                    if($last_id==''){
                        $last_id=  $att_id;  
                    }
                    wp_update_post( array(
                                'ID' => $att_id,
                                'post_parent' => $post_id
                            ));
                        
                    
                }
            }
           
            
            if( is_numeric($_POST['attachthumb']) && $_POST['attachthumb']!=''  ){
                set_post_thumbnail( $post_id, $_POST['attachthumb'] ); 
            } 
            
            
           
            
            if($new_thumb==1 || !has_post_thumbnail($post_id) || $_POST['attachthumb']==''){
                set_post_thumbnail( $post_id, $last_id ); 
            }
           
            if( isset($prop_category->name) ){
                 wp_set_object_terms($post_id,$prop_category->name,'property_category'); 
            }  
            if ( isset ($prop_action_category->name) ){
                 wp_set_object_terms($post_id,$prop_action_category->name,'property_action_category'); 
            }  
            if( isset($property_city) ){
                 wp_set_object_terms($post_id,$property_city,'property_city'); 
            }  
            if( isset($property_area) ){
                 wp_set_object_terms($post_id,$property_area,'property_area'); 
            }  
            
               
      
            update_post_meta($post_id, 'property_address', $property_address);
            update_post_meta($post_id, 'property_area', $property_area);
            update_post_meta($post_id, 'property_county', $property_county);
            update_post_meta($post_id, 'property_state', $property_state);
            update_post_meta($post_id, 'property_zip', $property_zip);
            update_post_meta($post_id, 'property_country', $country_selected);
            update_post_meta($post_id, 'property_size', $property_size);
            update_post_meta($post_id, 'property_lot_size', $property_lot_size);  
            update_post_meta($post_id, 'property_rooms', $property_rooms);  
            update_post_meta($post_id, 'property_bedrooms', $property_bedrooms);
            update_post_meta($post_id, 'property_bathrooms', $property_bathrooms);
            update_post_meta($post_id, 'property_status', $prop_stat);
            update_post_meta($post_id, 'property_price', $property_price);
            update_post_meta($post_id, 'price_per_value', $price_per_value);
            update_post_meta($post_id, 'guest_no', $guest_no);
            update_post_meta($post_id, 'city_fee', $city_fee);
            update_post_meta($post_id, 'cleaning_fee', $cleaning_fee);            
            update_post_meta($post_id, 'property_label', $property_label);
            update_post_meta($post_id, 'embed_video_type', $video_type);
            update_post_meta($post_id, 'embed_video_id', $embed_video_id);
            update_post_meta($post_id, 'prop_slider_type', $slider_type);
            update_post_meta($post_id, 'property_latitude', $property_latitude);
            update_post_meta($post_id, 'property_longitude', $property_longitude);
            update_post_meta($post_id, 'property_google_view', $google_view);
            update_post_meta($post_id, 'prop_featured', $prop_featured);
            
            update_post_meta($post_id, 'google_camera_angle', $google_camera_angle);
         
            foreach($feature_list_array as $key => $value){
                $post_var_name=  str_replace(' ','_', trim($value) );
                $feature_value=sanitize_text_field( $_POST[$post_var_name] );
                update_post_meta($post_id, $post_var_name, $feature_value);
            }
        
    
            // save custom fields
            $i=0;
            if(!empty($custom_fields)){
               while($i< count($custom_fields) ){
                    $name =   $custom_fields[$i][0];
                    $type =   $custom_fields[$i][1];
                    $slug =   str_replace(' ','_',$name);

                    if($type=='numeric'){
                        $value_custom    =   floatval(sanitize_text_field( $_POST[$slug] ) );
                        update_post_meta($post_id, $slug, $value_custom);
                    }else{
                        $value_custom    =   esc_html(sanitize_text_field( $_POST[$slug] ) );
                        update_post_meta($post_id, $slug, $value_custom);
                    }
                        $custom_fields_array[$slug]= sanitize_text_field( $_POST[$slug]); ;
                    $i++;
                }  
            }
           
        
            // get user dashboard link
            $redirect = get_dashboard_link();
            wp_reset_query();
            $headers = 'From: No Reply <noreply@'.$_SERVER['HTTP_HOST'].'>' . "\r\n";
            $message  = __('Hi there,','wpestate') . "\r\n\r\n";
            $message .= sprintf( __("A user has edited one of his listings! You should go check it out.",'wpestate'), get_option('blogname')) . "\r\n\r\n";
            $message .= __('The property name is : ','wpestate').$submit_title;
            @wp_mail(get_option('admin_email'),
		    sprintf(__('[%s] Listing Edited','wpestate'), get_option('blogname')),
                    $message,
                    $headers);
            
            
           wp_redirect( $redirect);
           exit;
        }// end if edited
    
}




get_header();
$options=sidebar_orientation($post->ID);
//get_template_part('libs/templates/map-template'); 


///////////////////////////////////////////////////////////////////////////////////////////
/////// Html Form Code below
///////////////////////////////////////////////////////////////////////////////////////////
?> 

<div id="cover"></div>
    <div id="upload_progress">
     <?php _e('Please wait while we are processing your submission. Thank you!','wpestate');?>
 </div>  
    

<div id="wrapper" class="<?php print $options['fullwhite']; ?>">  
    <div class="<?php print $options['add_back']; ?>"></div>

     <?php
    print breadcrumb_container('yes',$options['bread_align'] )
    ?>
    <div id="main" class="row <?php print $options['sidebar_status']; ?>">
	
<?php if( !is_user_logged_in()){
	echo $login_first;
	$user_roles;
}  /*elseif(is_user_logged_in() && $user_roles!='contributor'){
	echo $login_first;
	//echo $user_roles;
}*/
else {?>

    <?php
    print display_breadcrumbs( 'yes' ,$options['bread_align_internal'] )
    ?>



        <!-- begin content--> 
        <div id="post" class="noborder twelve columns alpha noshadow"> 
            <div class="inside_post inside_no_border submit_area">
                <?php if (esc_html( get_post_meta($post->ID, 'page_show_title', true) ) != 'no') { ?>
                    <h1 class="entry-title-agent-page"><?php the_title(); ?></h1>
                <?php } ?>
                <?php //get_template_part('libs/templates/user_menu');  ?>   
                <?php get_template_part('libs/templates/front-end-submission');  ?>                
            </div> <!-- end inside post-->
        </div>
		
<?php }?>
        <!-- end content-->

       <?php  //include(locate_template('customsidebar.php')); ?>

    </div><!-- #main -->    
</div><!-- #wrapper -->
<?php get_footer();

function insert_attachment($file_handler,$post_id,$setthumb='false') {

    // check to make sure its a successful upload
    if ($_FILES[$file_handler]['error'] !== UPLOAD_ERR_OK) __return_false();

    require_once(ABSPATH . "wp-admin" . '/includes/image.php');
    require_once(ABSPATH . "wp-admin" . '/includes/file.php');
    require_once(ABSPATH . "wp-admin" . '/includes/media.php');

    $attach_id = media_handle_upload( $file_handler, $post_id );

    if ($setthumb) update_post_meta($post_id,'_thumbnail_id',$attach_id);
    return $attach_id;
} 

?>