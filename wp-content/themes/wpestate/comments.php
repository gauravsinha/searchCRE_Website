
<div id="comments">
    <?php if (post_password_required()) : ?>
        <p class="nopassword"><?php _e('This post is password protected. Enter the password to view any comments.', 'wpestate'); ?></p>
    </div><!-- #comments -->
    <?php
    return;
endif;
?>








<?php // You can start editing here -- including this comment!  ?>

<?php if (have_comments()) : ?>
    <h3>
        <?php
        printf(_n('One thought on &ldquo;%2$s&rdquo;', '%1$s thoughts on &ldquo;%2$s&rdquo;', get_comments_number(), 'wpestate'), number_format_i18n(get_comments_number()), '<span>' . get_the_title() . '</span>');
        ?>
    </h3>

    <?php if (get_comment_pages_count() > 1 && get_option('page_comments')) : // are there comments to navigate through  ?>
      
    <?php endif; // check for comment navigation  ?>

    <ul class="commentlist ">
    <?php
    // 
    wp_list_comments(array('callback' => 'wpestate_comment'));
    ?>
    </ul>

    <?php if (get_comment_pages_count() > 1 && get_option('page_comments')) : // are there comments to navigate through  ?>
        <nav id="comment-nav-below">
            <h1 class="assistive-text"><?php _e('Comment navigation', 'wpestate'); ?></h1>
            <div class="nav-previous"><?php previous_comments_link(__('&laquo; Older Comments', 'wpestate')); ?></div>
            <div class="nav-next"><?php next_comments_link(__('Newer Comments &raquo;', 'wpestate')); ?></div>
        </nav>
    <?php endif; // check for comment navigation  ?>

    <?php if (!comments_open() && get_comments_number()) : ?>
        <p class="nocomments"><?php _e('Comments are closed.', 'wpestate'); ?></p>
    <?php endif; ?>

<?php endif; // have_comments()  ?>

<?php comment_form(); ?>

</div><!-- #comments -->
