<form method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
    <input type="text" class="field" name="s" id="s" value="<?php esc_attr_e( 'Search', 'wpestate' ); ?>" />
    <input type="submit" id="submit-form" class="submit-form" value="">
</form>
