<?php
// Template Name: User Dashboard Profile Page
// Wp Estate Pack

global $current_user;
get_currentuserinfo(); 
$dash_profile_link = get_dashboard_profile_link();

   
        
        
//////////////////////////////////////////////////////////////////////////////////////////
// Paypal payments for membeship packages
//////////////////////////////////////////////////////////////////////////////////////////
if (isset($_GET['token']) ){
    $token               =   sanitize_text_field ( $_GET['token'] );
    $token_recursive     =   sanitize_text_field ( $_GET['token'] );
   
       
    // get transfer data
    $save_data=get_option('paypal_pack_transfer');
    $payment_execute_url    =   $save_data[$current_user->ID ]['paypal_execute'];
    $token                  =   $save_data[$current_user->ID ]['paypal_token'];
    $pack_id                =   $save_data[$current_user->ID ]['pack_id'];
    $recursive              =   0;
    if (isset ( $save_data[$current_user->ID ]['recursive']) ){
        $recursive              =   $save_data[$current_user->ID ]['recursive']; 
    }
   
    if($recursive!=1){
        if( isset($_GET['PayerID']) ){
            $payerId             =   sanitize_text_field ( $_GET['PayerID'] );  
        
            $payment_execute = array(
                           'payer_id' => $payerId
                          );
            $json = json_encode($payment_execute);
            $json_resp = make_post_call($payment_execute_url, $json,$token);

            $save_data[$current_user->ID ]=array();
            update_option ('paypal_pack_transfer',$save_data); 

             if($json_resp['state']=='approved' ){

                if( check_downgrade_situation($current_user->ID,$pack_id) ){

                    downgrade_to_pack( $current_user->ID, $pack_id );
                    upgrade_user_membership($current_user->ID,$pack_id,1,'');
                }else{
                   upgrade_user_membership($current_user->ID,$pack_id,1,'');
                }
               wp_redirect( $dash_profile_link ); exit;
            }
        }
    }else{ 
        require('libs/paypalfunctions.php');   
        $billing_period                 =   get_post_meta($pack_id, 'biling_period', true);
        $billing_freq                   =   intval(get_post_meta($pack_id, 'billing_freq', true));
        
        $obj=new paypal_recurring;
        $obj->environment       =   esc_html( get_option('wp_estate_paypal_api','') );
        $obj->paymentType       =   urlencode('Sale');          // or 'Sale' or 'Order'
        $paypal_api_username    =   esc_html( get_option('wp_estate_paypal_api_username','') );
        $paypal_api_password    =   esc_html( get_option('wp_estate_paypal_api_password','') );
        $paypal_api_signature   =   esc_html( get_option('wp_estate_paypal_api_signature','') );    
        $obj->API_UserName      =   urlencode( $paypal_api_username );
        $obj->API_Password      =   urlencode( $paypal_api_password );
        $obj->API_Signature     =   urlencode( $paypal_api_signature );
        $obj->API_Endpoint      =   "https://api-3t.paypal.com/nvp";
        $obj->paymentType       =   urlencode('Sale');  
        $obj->returnURL         =   urlencode($dash_profile_link);
        $obj->cancelURL         =   urlencode($dash_profile_link);
        $obj->paymentAmount     =   get_post_meta($pack_id, 'pack_price', true);
        $obj->currencyID        =   get_option('wp_estate_submission_curency','');
        $date                   =   $save_data[$current_user->ID ]['date'];
        $obj->startDate         =   urlencode($date);
        $obj->billingPeriod     =   urlencode($billing_period);         
        $obj->billingFreq       =   urlencode($billing_freq); 
        $pack_name              =   get_the_title($pack_id);
        $obj->productdesc       =   urlencode($pack_name.__(' package on ','wpestate').get_bloginfo('name') );
        $obj->user_id           =   $current_user->ID;
        $obj->pack_id           =   $pack_id;
        
       if ( $obj->getExpressCheckout($token_recursive) ){
        
             if( check_downgrade_situation($current_user->ID,$pack_id) ){
                 downgrade_to_pack( $current_user->ID, $pack_id );
                 upgrade_user_membership($current_user->ID,$pack_id,2,'');
             }else{
                upgrade_user_membership($current_user->ID,$pack_id,2,'');
             }
            //wp_redirect( $dash_profile_link ); exit; 
        }
        
    }
                             
}


//////////////////////////////////////////////////////////////////////////////////////////
// 3rd party login code
//////////////////////////////////////////////////////////////////////////////////////////
$facebook_status  =   esc_html( get_option('wp_estate_facebook_login','') );

if( ( isset($_GET['code']) && isset($_GET['state']) ) ){
    require 'libs/facebook/facebook.php';
       
    $facebook_api               =   esc_html ( get_option('wp_estate_facebook_api','') );
    $facebook_secret            =   esc_html ( get_option('wp_estate_facebook_secret','') );
    $facebook = new Facebook(array(
        'appId'  => $facebook_api,
        'secret' => $facebook_secret,
        'cookie' => true
     ));
    $secret      =   $facebook_secret;
    $login_url   =   $facebook->getLoginUrl(); 
    $user_id     =   $facebook->getUser();
    
    $params = array(
        'redirect_uri' => get_user_dashboard_url(),
        'scope' => 'email',
        );

  if($user_id==0){
      $login_url = $facebook->getLoginUrl($params); 
      wp_redirect($login_url);exit;
  }else{
        $user_profile = $facebook->api('/me','GET');
        if(isset($user_profile['last_name'])){
            $full_name=$user_profile['first_name']. $user_profile['last_name'];
        }
        
        if(isset($user_profile['name'])){
            $full_name=$user_profile['name'];
        }
        
        if(isset($user_profile['email'])){
            $email=$user_profile['email'];
        }else{
            $email=$full_name.'@facebook.com';
        }
        
        
        $identity_code=$secret.$user_profile['id'];
        
        register_user_via_google($email,$full_name,$identity_code); 
        
        
        
        
        $info                   = array();
        $info['user_login']     = $full_name;
        $info['user_password']  = $identity_code;
        $info['remember']       = true;
       
        $user_signon            = wp_signon( $info, true );
        
        
        if ( is_wp_error($user_signon) ){ 
            wp_redirect( home_url() );  exit;
        }else{
            update_old_users($user_signon->ID);
            wp_redirect(get_user_dashboard_url());exit;
        }
        
                
  }
  
    
}else if(isset($_GET['openid_mode']) && $_GET['openid_mode']=='id_res' ){   
    require 'libs/openid.php';  
    $openid = new LightOpenID( get_domain_openid() );
    if( $openid->validate() ){
        $dashboard_url=get_user_dashboard_url();
        $openid_identity  =  $openid_identity_check      =   $_GET['openid_identity'];
        
        
        if(strrpos  ($openid_identity_check,'google') ){
            $email                  =   $_GET['openid_ext1_value_contact_email'];
            $last_name              =   $_GET['openid_ext1_value_namePerson_last'];
            $first_name             =   $_GET['openid_ext1_value_namePerson_first'];
            $full_name              =   $first_name.$last_name;
            $openid_identity_pos    =   strrpos  ($openid_identity,'id?id=');
            $openid_identity        =   str_split($openid_identity, $openid_identity_pos+6);
            $openid_identity_code   =   $openid_identity[1]; 
        }
        
        if(strrpos  ($openid_identity_check,'yahoo')){
            $email                  =   $_GET['openid_ax_value_email'];
            $full_name              =   str_replace(' ','.',$_GET['openid_ax_value_fullname']);            
            $openid_identity_pos    =   strrpos  ($openid_identity,'/a/.');
            $openid_identity        =   str_split($openid_identity, $openid_identity_pos+4);
            $openid_identity_code   =   $openid_identity[1]; 
        }
       
        register_user_via_google($email,$full_name,$openid_identity_code); 
        $info                   = array();
        $info['user_login']     = $full_name;
        $info['user_password']  = $openid_identity_code;
        $info['remember']       = true;
        $user_signon            = wp_signon( $info, false );
        
     
        
        if ( is_wp_error($user_signon) ){ 
          wp_redirect( home_url() );  exit;
        }else{
    
            update_old_users($user_signon->ID);
            wp_redirect($dashboard_url);exit;
        }
           
    } 
}else if (isset($_GET['code'])){
    estate_google_oauth_login($_GET);
    
}else{
    if ( !is_user_logged_in() ) {   
        wp_redirect(  home_url() );exit;
    }

}



   
$paid_submission_status         =   esc_html ( get_option('wp_estate_paid_submission','') );
$price_submission               =   floatval( get_option('wp_estate_price_submission','') );
$submission_curency_status      =   esc_html( get_option('wp_estate_submission_curency','') );


$pages = get_pages(array(
        'meta_key' => '_wp_page_template',
        'meta_value' => 'user-dashboard-add.php'
            ));

    if( $pages ){
        $edit_link = get_permalink( $pages[0]->ID);
    }else{
        $edit_link=home_url();
    }

  
$processor_link=get_procesor_link();
  
get_header();
$options=sidebar_orientation($post->ID);
//get_template_part('libs/templates/map-template'); 
?> 





<div id="wrapper" class="fullwhite">  
    <div class="<?php print $options['add_back']; ?>"></div>

    <?php    print breadcrumb_container('yes',$options['bread_align'] ); ?>
    <div id="main" class="row">
    <?php     print display_breadcrumbs( 'yes' ,$options['bread_align_internal'] ); ?>




        <!-- begin content--> 
        <div id="post" class=" noborder twelve columns alpha noshadow"> 
            <div class="inside_post inside_no_border submit_area">
               <?php if (esc_html( get_post_meta($post->ID, 'page_show_title', true) ) != 'no') { ?>
                    <h1 class="entry-title-agent-page"><?php the_title(); ?></h1>
               <?php } ?>
                  
               <?php 
               //get_template_part('libs/templates/user_menu'); 
               get_template_part('libs/templates/user_profile'); 
               
               
               ?>            
            </div> <!-- end inside post-->
        </div>
        <!-- end content-->

       <?php //   include(locate_template('customsidebar.php')); ?>

    </div><!-- #main -->    
</div><!-- #wrapper -->
<?php get_footer();




?>