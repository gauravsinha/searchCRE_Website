<?php
// Template Name: Bookings List
// Wp Estate Pack

if ( !is_user_logged_in() ) {   
     wp_redirect(  home_url() );exit;
} 

global $current_user;
global $where_currency;
global $currency;
get_currentuserinfo();
$userID                         =   $current_user->ID;
$user_login                     =   $current_user->user_login;
$user_group                     =   get_the_author_meta( 'user_group' , $userID );
$user_pack                      =   get_the_author_meta( 'package_id' , $userID );
$user_registered                =   get_the_author_meta( 'user_registered' , $userID );
$user_package_activation        =   get_the_author_meta( 'package_activation' , $userID );   
$paid_submission_status         =   esc_html ( get_option('wp_estate_paid_submission','') );
$price_submission               =   floatval( get_option('wp_estate_price_submission','') );
$submission_curency_status      =   esc_html( get_option('wp_estate_submission_curency','') );
$where_currency                 =   esc_html( get_option('wp_estate_where_currency_symbol', '') );
$currency                       =   esc_html( get_option('wp_estate_submission_curency', '') );
    
get_header();
$options=sidebar_orientation($post->ID);
get_template_part('libs/templates/map-template');    
?> 
<!-- Google Map Code -->



<div id="wrapper" class="fullwhite">  
    <div class="<?php print $options['add_back']; ?>"></div>

     <?php
    print breadcrumb_container('yes',$options['bread_align'] )
    ?>
    <div id="main" class="row">
    <?php
    print display_breadcrumbs( 'yes' ,$options['bread_align_internal'] )
    ?>




        <!-- begin content--> 
        <div id="post" class=" noborder twelve columns alpha noshadow"> 
            <div class="inside_post inside_no_border submit_area">
               <?php if (esc_html( get_post_meta($post->ID, 'page_show_title', true) ) != 'no') { ?>
                    <h1 class="entry-title-agent-page"><?php the_title(); ?></h1>
               <?php } ?>
                  
               <?php get_template_part('libs/templates/user_menu');   ?>
                    
             
            
                    <div class="secondary_menu">
                       <div class="action1_booking" id="add_my_booking"><?php _e('Add A Booking','wpestate');?></div> 
                    </div>  
                    
                    <div class="add_booking" id="add_booking_form">
                    <h3><?php _e('Add Booking','wpestate');?></h3>
                    <div class="booking_explaining"> 
                        <?php _e('*use this form to manually add a booking for one of your properties or just to "block" certain dates from being available.','wpestate');?>
                    </div>
                    
                    <div id="booking_form_request_mess"></div>
                    
                    <p class="third_form">
                        <label for="booking_from_date"><?php _e('From Date','wpestate');?></label>
                        <input type="text" id="booking_from_date" size="40" name="booking_from_date" value="">
                    </p>
                    <p class="third_form">
                        <label for="booking_to_date"><?php _e('To Date','wpestate');?></label>
                        <input type="text" id="booking_to_date" size="40" name="booking_to_date" value="">
                    </p>
                     <p class="third_form">
                        <label for="booking_property_name"><?php _e('Property Name','wpestate');?></label>
                        <select id="booking_property_name" name="booking_property_name">
                            <option value="0"><?php _e('select a property','wpestate');?></option>
                            <?php print get_property_list_from_user($userID) ?>
                        </select>   
                      
                    </p>
                    <p class="third_form">
                        <label for="booking_guest_no"><?php _e('Guests','wpestate');?></label>
                        <select id="booking_guest_no"  name="booking_guest_no"  class="cd-select" >
                            <option value="1">1 <?php _e('Guest','wpestate');?></option>
                            <?php
                            for ($i = 2; $i <= 11; $i++) {
                                print '<option value="'.$i.'">'.$i.' '.__('Guests','wpestate').'</option>';
                            }
                            ?>
                    
                        </select> 
                    </p>
                    <p class="full_form">
                       <label for="event_description"><?php _e('Comments','wpestate');?></label>
                       <textarea id="event_description" tabindex="3" name="event_description" cols="50" rows="6"></textarea>
                    </p>
                    <?php print wp_nonce_field( 'booking_ajax_nonce', 'security-register-booking_front' ); ?>
                    <button type="submit" id="submit_booking" class="btn vernil small"><?php _e('Add Booking','wpestate');?></button>
                    </div>  
                  
                    
                <div class="booking-keeper">
 
                    <?php
                    $book_selection='';
                    $all_my_post=array();
                    $args = array(
                        'post_type'         => 'estate_property',
                        'post_status'       => 'publish',
                        'posts_per_page'    => -1,
                        'author'           =>  $userID   
                    );
                    $prop_selection = new WP_Query($args);
                    while ($prop_selection->have_posts()): $prop_selection->the_post(); 
                       $all_my_post[]=$post->ID;
                    endwhile;
                 //   print_r($all_my_post);
                    wp_reset_query();
                    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                    
                    if( !empty($all_my_post) ){

                        $args = array(
                            'post_type'         => 'wpestate_booking',
                            'post_status'       => 'publish',
                            'paged'             => $paged,
                            'posts_per_page'    => 30,
                            'order'             => 'DESC',
                            'meta_query' => array(
                                    array(
                                            'key' => 'booking_id',
                                            'value' => $all_my_post,
                                            'compare' => 'IN'
                                    )
                            )
                         );
                    //    print_r($args);
                        $book_selection = new WP_Query($args);
                        while ($book_selection->have_posts()): $book_selection->the_post(); 
                     //      include(locate_template('booking/book-listing.php'));//
                            get_template_part('booking/book-listing');
                        endwhile;
                        wp_reset_query();
                        }else{
                            print '<h4>'.__('You don\'t have any booking requests yet!','wpestate').'</h4>';
                        }
                    ?>
                </div>
              
               
            </div> <!-- end inside post-->
            <?php  
                if($book_selection!=''){
                    kriesi_pagination($book_selection->max_num_pages, $range =2);
                }  
            ?>
        </div>
        <!-- end content-->

       <?php //   include(locate_template('customsidebar.php')); ?>

    </div><!-- #main -->    
</div><!-- #wrapper -->
<?php get_footer(); ?>