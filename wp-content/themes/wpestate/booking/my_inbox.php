<?php
// Template Name: Inbox Page
// Wp Estate Pack



if ( !is_user_logged_in() ) {   
     wp_redirect(  home_url() );exit;
} 

global $current_user;
get_currentuserinfo();
$userID                         =   $current_user->ID;
$user_login                     =   $current_user->user_login;
$user_group                     =   get_the_author_meta( 'user_group' , $userID );
$user_pack                      =   get_the_author_meta( 'package_id' , $userID );
$user_registered                =   get_the_author_meta( 'user_registered' , $userID );
$user_package_activation        =   get_the_author_meta( 'package_activation' , $userID );   
$paid_submission_status         =   esc_html ( get_option('wp_estate_paid_submission','') );
$price_submission               =   floatval( get_option('wp_estate_price_submission','') );
$submission_curency_status      =   esc_html( get_option('wp_estate_submission_curency','') );

get_header();
$options=sidebar_orientation($post->ID);
get_template_part('libs/templates/map-template');    
?> 
<!-- Google Map Code -->



<div id="wrapper" class="fullwhite">  
    <div class="<?php print $options['add_back']; ?>"></div>

     <?php
    print breadcrumb_container('yes',$options['bread_align'] )
    ?>
    <div id="main" class="row">
    <?php
    print display_breadcrumbs( 'yes' ,$options['bread_align_internal'] )
    ?>




        <!-- begin content--> 
        <div id="post" class=" noborder twelve columns alpha noshadow"> 
            <div class="inside_post inside_no_border submit_area">
               <?php if (esc_html( get_post_meta($post->ID, 'page_show_title', true) ) != 'no') { ?>
                    <h1 class="entry-title-agent-page"><?php the_title(); ?></h1>
               <?php } ?>
                  
               <?php get_template_part('libs/templates/user_menu');   ?>
                    
                <?php 
                /////////////////////////////////////////////////////////////////////////////////////////////
                // starting renter code
                /////////////////////////////////////////////////////////////////////////////////////////////
                
                 
                     $args = array(
                        'post_type'         => 'wpestate_message',
                        'post_status'       => 'publish',
                        'paged'             => $paged,
                        'posts_per_page'    => 30,
                        'order'             => 'DESC',
                        'meta_query' => array(
                                array(
                                        'key'       => 'message_to_user',
                                        'value'     => $userID,
                                        'compare'   => '='
                                ),
                                array(
                                        'key'       => 'delete_destination',
                                        'value'     => 1,
                                        'compare'   => '!='
                                ),
                            
                           
                        )
                     );
                   
                    $message_selection = new WP_Query($args);
                    while ($message_selection->have_posts()): $message_selection->the_post(); 
                          include(locate_template('booking/message-listing.php')); 
                    endwhile;
                    wp_reset_query();
            
                ?>
            </div> <!-- end inside post-->
            <?php   kriesi_pagination($message_selection->max_num_pages, $range =2); ?>
        </div>
        <!-- end content-->

       <?php //   include(locate_template('customsidebar.php')); ?>

    </div><!-- #main -->    
</div><!-- #wrapper -->
<?php get_footer(); ?>
