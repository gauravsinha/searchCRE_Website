<?php
// Search
// Wp Estate Pack
get_header();
$options=sidebar_orientation();
?>

<!-- Google Map Code -->
<?php 
get_template_part('libs/templates/map-template'); 
?> 
<!-- Google Map Code -->


<div id="wrapper" class="<?php print $options['fullwhite']; ?>">  
    <div class="<?php print $options['add_back']; ?>"></div>

    <?php
    print breadcrumb_container($options['full_breadcrumbs'],$options['bread_align'] )
    ?>
    <div id="main" class="row <?php print $options['sidebar_status']; ?>">
    <?php
    print display_breadcrumbs( $options['full_breadcrumbs'] ,$options['bread_align_internal'] )
    ?>

        
 

    <!-- begin content--> 
    <div id="post" class="blogborder <?php print $options['grid'].' ' .$options['shadow']; ?>"> 
        <div class="inside_post no_margin_bottom inside_no_border">
            <h1 class="entry-title"><?php _e( 'Search Results for : ','wpestate');print '<span>' . get_search_query() . '</span>'; ?></h1>

            <?php
            if (have_posts()){
                while (have_posts()) : the_post(); 
                      include(locate_template('bloglisting_search.php')); 
                endwhile;
            }else{
            ?>
                <p>
                <?php _e( 'We didn\'t find any results. Please try again with different search parameters. ', 'wpestate' ); ?>
                </p>
            <?php
            }
            wp_reset_query();
            ?>
        </div> <!-- end inside post-->
        <?php kriesi_pagination('', $range = 2); ?>       


    </div>
    <!-- end content-->


       <?php  include(locate_template('customsidebar.php')); ?>


    </div><!-- #main -->    
</div><!-- #wrapper -->
<?php get_footer();
?>