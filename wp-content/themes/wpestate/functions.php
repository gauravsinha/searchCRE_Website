<?php
show_admin_bar(false);
require_once ('libs/metaboxes.php');
require_once ('libs/property.php');
require_once ('libs/agents.php');
require_once ('libs/membership.php');
require_once ('libs/invoices.php');
require_once ('libs/multiple_sidebars_plugin.php');
require_once ('libs/shortcodes-install.php');
require_once ('libs/shortcodes.php');
require_once ('libs/theme-setup.php');
require_once ('libs/general-settings.php');
require_once ('libs/pin-management.php');
require_once ('libs/widgets.php');
require_once ('libs/breadcrumbs.php');
require_once ('libs/class-tgm-plugin-activation.php');
require_once ('libs/plugins.php');
require_once ('libs/ajax-login.php');
require_once ('libs/ajax_upload.php');

if( esc_html ( get_option('wp_estate_enable_rental_module','') )=='yes'){
    require_once ('libs/booking.php');
    require_once ('libs/messages.php');
    require_once ('libs/calendar.php');
    require_once ('libs/ajax_functions_booking.php');
}



//require get_template_directory() . '/ajax-login-register/ajax-login-register.php';

add_action('after_setup_theme', 'my_theme_setup');
function my_theme_setup(){
    load_theme_textdomain('wpestate', get_template_directory() . '/languages');
}

add_filter( 'wp_nav_menu_items', 'pt_login_link_to_menu', 10, 2 );
function pt_login_link_to_menu ( $items, $args ) {
	if($args->theme_location == 'primary' ){
    if( ! is_user_logged_in()) {
        $items .= '<li class="menu-item login-link"><a href="'.get_permalink(6902).'">'.__( 'Login/Register', 'ptheme' ).'</a></li>';
    } else {
global $current_user;
get_currentuserinfo();
$userID                 =   $current_user->ID;
$user_login             =   $current_user->user_login;

$user_agent    =   get_the_author_meta( 'user_agent_id' , $userID );
if($user_agent!=''){
	
//$preview1    =   get_the_author_meta( 'custom_picture' , $userID );	
$thumb_id = get_post_thumbnail_id($user_agent);
 $preview = wp_get_attachment_image_src($thumb_id, 'agent_picture_thumb_list');
 $preview1=$preview[0];
 //print_r($preview);
//echo "ok";

	//echo "acha";
/*                    $thumb_id = get_post_thumbnail_id($user_agent);
                    $preview = wp_get_attachment_image_src($thumb_id, 'agent_picture_thumb_list');
					$preview1=$preview[0];*/
}			
//echo $preview1;
$items .= '<li class="menu-item login-link" style="margin-top:-20px;margin-left:10px;">
		<div class="featured_agent_image_top" id="profile-image1" style="background:#0b8593 url('.$preview1.') ;"></div>
		<ul class="sub-menu1">
		<li><a href="'.get_permalink(5277).'">Profile</a></li>
		
		<li><a href="'.wp_logout_url( home_url() ).'">Logout</a></li>
		</ul></li>';
	}
    return $items;
	}
}
if ( ! isset( $content_width ) ) $content_width = 1400;



///////////////////////////////////////////////////////////////////////////////////////////
/////// If admin create the menu
///////////////////////////////////////////////////////////////////////////////////////////
if (is_admin()) {
    add_action('admin_menu', 'manage_admin_menu');
}

function manage_admin_menu() {
    global $theme_name;

    add_theme_page('Theme Options', 'Theme Options', 'administrator', 'libs/new-theme-admin.php', 'new_general_set' );
    require 'libs/property-admin.php';
    require 'libs/pin-admin.php';
    require 'libs/new-theme-admin.php';
  
}



///////////////////////////////////////////////////////////////////////////////////////////
/////// Js & Css include on front site 
///////////////////////////////////////////////////////////////////////////////////////////

add_action('wp_enqueue_scripts', 'wpestate_scripts');

function wpestate_scripts() {   
    global $post;
    $custom_image               =   '';
    $rev_slider                 =   '';
    $use_idx_plugins            =   0;
    $idx_status                 =   esc_html ( get_option('wp_estate_idx_enable','') );   
    $adv_search_type_status     =   intval ( get_option('wp_estate_adv_search_type',''));
    $home_small_map_status      =   esc_html ( get_option('wp_estate_home_small_map','') );
 
    $default_map_zoom           =   intval   ( get_option('wp_estate_default_map_zoom','') );
    $page_custom_zoom           =   1;
    if( isset( $post->ID ) ){
           $custom_image        =   esc_html( esc_html(get_post_meta($post->ID, 'page_custom_image', true)) );
           $rev_slider          =   esc_html( esc_html(get_post_meta($post->ID, 'rev_slider', true)) );
           $page_custom_zoom    =   get_post_meta($post->ID, 'page_custom_zoom', true);
           if($page_custom_zoom==''){
               $page_custom_zoom = $default_map_zoom;
           }
    }
 
    if(is_tax() || is_search()){
         $page_custom_zoom = $default_map_zoom;
    }
    
    
    if($idx_status=='yes'){
        $use_idx_plugins=1;
    }
  
   
    
    wp_enqueue_style('wpestate_style',get_stylesheet_uri(), array(), '1.0', 'all');  
    wp_enqueue_style('wpestate_media',get_template_directory_uri().'/css/my_media.css', array(), '1.0', 'all'); 
   // wp_enqueue_style('jquery.fileupload.css',get_template_directory_uri().'/css/jquery.fileupload.css', array(), '1.0', 'all'); 
    

    wp_enqueue_script("jquery");
    wp_enqueue_script("jquery-ui-draggable");
    	
    if( is_page_template('user-dashboard-profile.php')  ){
             wp_enqueue_script('plupload-handlers');
    }
	
    wp_enqueue_script('modernizr', get_template_directory_uri().'/js/modernizr.custom.62456.js',array(), '1.0', false);     
    wp_enqueue_script('dropdown', get_template_directory_uri().'/js/jquery.dropdown.js',array('jquery'), '1.0', true); 
    wp_enqueue_script('gumby', get_template_directory_uri().'/js/gumby.min.js',array('jquery'), '1.0', true); 
    wp_enqueue_script('fitvids', get_template_directory_uri().'/js/jquery.fitvids.js',array('jquery'), '1.0', true); 
    wp_enqueue_script('froogaloop', get_template_directory_uri().'/js/froogaloop.js',array('jquery'), '1.0', true);  
    wp_enqueue_script('jquery.elastislide', get_template_directory_uri().'/js/jquery.elastislide.js',array('jquery'), '1.0', true); 
    wp_enqueue_script('jquery.flexslider', get_template_directory_uri().'/js/jquery.flexslider.js',array('jquery'), '1.0', true); 
    wp_enqueue_script('jquery.prettyphoto', get_template_directory_uri().'/js/jquery.prettyphoto.js',array('jquery'), '1.0', true); 
    wp_enqueue_script('jquery.imagesloaded', get_template_directory_uri().'/js/jquery.imagesloaded.js',array('jquery'), '1.0', true);
    wp_enqueue_script("jquery-ui-datepicker");
    wp_enqueue_script("jquery-ui-autocomplete");
    wp_enqueue_style('jquery.ui.theme', get_template_directory_uri() . '/css/jquery-ui.min.css');
    
    $use_generated_pins=0;    

    

    if($custom_image=='' && $rev_slider==''){    
        $post_type=get_post_type();
        if( is_page_template('advanced-search-results.php') || is_page_template('advanced-search-results-booking.php') || is_tax() || $post_type=='estate_agent' ){    // search results -> pins are added  from template   
            $use_generated_pins=1;
            $json_string=array();
            $json_string=json_encode($json_string);
        }else{
             // google maps pins
             $json_string= listing_pins();
        }
    }

    
    wp_enqueue_script('googlemap', 'http://maps.googleapis.com/maps/api/js?sensor=true&amp;key='.esc_html(get_option('wp_estate_api_key', '') ),array('jquery'), '1.0', false);        
    wp_enqueue_script('infobox',  get_template_directory_uri() .'/js/infobox.js',array('jquery'), '1.0', true);
    
    if(isset($post->ID)){
        $page_lat   = esc_html( get_post_meta($post->ID, 'page_custom_lat', true) );
        $page_long  = esc_html( get_post_meta($post->ID, 'page_custom_long', true) );
    }
   
      
    if( get_post_type() === 'estate_property' && !is_tax() &&!is_search() ){
        $google_camera_angle    =   intval( esc_html(get_post_meta($post->ID, 'google_camera_angle', true)) );
        wp_enqueue_script('googlecode_property', get_template_directory_uri().'/js/google_js/google_map_code_prop.js',array('jquery'), '1.0', true); 
        wp_localize_script('googlecode_property', 'googlecode_property_vars', 
              array(  'general_latitude'  =>  esc_html( get_option('wp_estate_general_latitude','') ),
                      'general_longitude' =>  esc_html( get_option('wp_estate_general_longitude','') ),
                      'path'              =>  get_template_directory_uri().'/css/css-images',
                      'markers'           =>  $json_string,
                      'camera_angle'      =>  $google_camera_angle,
                      'idx_status'        =>  $use_idx_plugins,
                      'page_custom_zoom'  =>  $page_custom_zoom
                   )
          );
      
  
    }else if( is_front_page() && $home_small_map_status=='no' ){
   
        if($custom_image=='' && $rev_slider==''){    
            wp_enqueue_script('googlecode_home', get_template_directory_uri().'/js/google_js/google_map_code_home.js',array('jquery'), '1.0', true);        

            wp_localize_script('googlecode_home', 'googlecode_home_vars', 
                array(  'general_latitude'  =>  esc_html( get_option('wp_estate_general_latitude','') ),
                        'general_longitude' =>  esc_html( get_option('wp_estate_general_longitude','') ),
                        'path'              =>  get_template_directory_uri().'/css/css-images',
                        'markers'           =>  $json_string,
                        'idx_status'        =>  $use_idx_plugins,
                        'page_custom_zoom'  =>  $page_custom_zoom
                     )
            );
        }
        
    }else if( is_page_template('contact-page.php')  ){
            if($custom_image==''  && $rev_slider==''){  
                wp_enqueue_script('googlecode_contact', get_template_directory_uri().'/js/google_js/google_map_code_contact.js',array('jquery'), '1.0', true);        
                $hq_latitude =  esc_html( get_option('wp_estate_hq_latitude','') );
                $hq_longitude=  esc_html( get_option('wp_estate_hq_longitude','') );

                if($hq_latitude==''){
                    $hq_latitude='40.781711';
                }

                if($hq_longitude==''){
                    $hq_longitude='-73.955927';
                }
                $json_string=contact_pin(); 

                wp_localize_script('googlecode_contact', 'googlecode_contact_vars', 
                    array(  'hq_latitude'       =>  $hq_latitude,
                            'hq_longitude'      =>  $hq_longitude,
                            'path'              =>  get_template_directory_uri().'/css/css-images',
                            'markers'           =>  $json_string,
                            'page_custom_zoom'  =>  $page_custom_zoom
                        
                         )
                );
            }
       
    }else{
        if($custom_image==''  && $rev_slider==''){     
           
            
            if($page_lat=='' || $page_long==''){
                $page_lat   = esc_html( get_option('wp_estate_general_latitude','') );
                $page_long  = esc_html( get_option('wp_estate_general_longitude','') );
            }
            
            if(is_front_page()){
                $page_lat   = esc_html( get_option('wp_estate_general_latitude','') );
                $page_long  = esc_html( get_option('wp_estate_general_longitude','') );
            }
            
            wp_enqueue_script('googlecode_regular', get_template_directory_uri().'/js/google_js/google_map_code.js',array('jquery'), '1.0', true); 
            wp_localize_script('googlecode_regular', 'googlecode_regular_vars', 
                array(  'general_latitude'  =>  $page_lat,
                        'general_longitude' =>  $page_long,
                        'path'              =>  get_template_directory_uri().'/css/css-images',
                        'markers'           =>  $json_string,
                        'idx_status'        =>  $use_idx_plugins,
                        'generated_pins'    =>  $use_generated_pins,
                        'page_custom_zoom'  =>  $page_custom_zoom
                     )
            );
        }
    }         
   
 
    
    
    $pin_images=pin_images();
    $geolocation_radius =   esc_html ( get_option('wp_estate_geolocation_radius','') );
    $pin_cluster_status =   esc_html ( get_option('wp_estate_pin_cluster','') );
    $zoom_cluster       =   esc_html ( get_option('wp_estate_zoom_cluster ','') );
    wp_enqueue_script('mapfunctions', get_template_directory_uri().'/js/google_js/mapfunctions.js',array('jquery'), '1.0', true);   
    wp_localize_script('mapfunctions', 'mapfunctions_vars', 
            array(   'path'                 =>  get_template_directory_uri().'/css/css-images',
                     'pin_images'           =>  $pin_images ,
                     'geolocation_radius'   =>  $geolocation_radius,
                     'adv_search'           =>  $adv_search_type_status,
                     'in_text'              =>  __('in','wpestate'),
                     'zoom_cluster'         =>  $zoom_cluster,
                     'user_cluster'         =>  $pin_cluster_status,
                     'radius'               =>  __('radius','wpestate')
                      
                 )
	);
    
    wp_enqueue_script('markerclusterer', get_template_directory_uri().'/js/google_js/markerclusterer.js',array('jquery'), '1.0', true);  
    
    if(is_page_template('user-dashboard-add.php') || is_page_template('user-dashboard.php') ){
            $page_lat   = esc_html( get_option('wp_estate_general_latitude','') );
            $page_long  = esc_html( get_option('wp_estate_general_longitude','') );
            wp_enqueue_script('google_map_submit', get_template_directory_uri().'/js/google_js/google_map_submit.js',array('jquery'), '1.0', true);  
            wp_localize_script('google_map_submit', 'google_map_submit_vars', 
                array(  'general_latitude'  =>  $page_lat,
                        'general_longitude' =>  $page_long                        
                     )
            ); 
          
    }   
         

    $icons          =   get_icons();
    $hover_icons    =   get_hover_icons();
    $login_redirect =   get_dashboard_profile_link();
     
    $max_file_size = 100 * 1000 * 1000;
    global $current_user;
    get_currentuserinfo();
    $userID                     =   $current_user->ID; 
      
    $auto_cities=array();
    $rental_module_status= esc_html ( get_option('wp_estate_enable_rental_module','') );
    if( $rental_module_status=='yes'){
        $auto_cities=wpestate_get_autocomplete_cities();
    }
    
    wp_enqueue_script('control', get_template_directory_uri().'/js/control.js',array('jquery'), '1.0', true);   
    wp_localize_script('control', 'control_vars', 
            array(  'morg1'                 =>   __('Amount Financed:','wpestate'),
                    'morg2'                 =>   __('Mortgage Payments:','wpestate'),
                    'morg3'                 =>   __('Annual cost of Loan:','wpestate'),
                    'searchtext'            =>   __('SEARCH','wpestate'),
                    'searchtext2'           =>   __('Search here...','wpestate'),
                    'icons'                 =>   $icons,
                    'hovericons'            =>   $hover_icons,
                    'path'                  =>   get_template_directory_uri(),
                    'search_room'           =>  __('Type Rooms No.','wpestate'),
                    'search_bath'           =>  __('Type Bathrooms No.','wpestate'),
                    'search_min_price'      =>  __('Type Min. Price','wpestate'),
                    'search_max_price'      =>  __('Type Max. Price','wpestate'),
                    'contact_name'          =>  __('Your Name','wpestate'),
                    'contact_email'         =>  __('Your Email','wpestate'),
                    'contact_phone'         =>  __('Your Phone','wpestate'),
                    'contact_comment'       =>  __('Your Message','wpestate'),
                    'zillow_addres'         =>  __('Your Address','wpestate'),
                    'zillow_city'           =>  __('Your City','wpestate'),
                    'zillow_state'          =>  __('Your State Code (ex CA)','wpestate'),
                    'adv_contact_name'      =>  __('Your Name','wpestate'),
                    'adv_email'             =>  __('Your Email','wpestate'),
                    'adv_phone'             =>  __('Your Phone','wpestate'),
                    'adv_comment'           =>  __('Your Message','wpestate'),
                    'adv_search'            =>  __('Send Message','wpestate'),
                    'admin_url'             =>  get_admin_url(),
                    'login_redirect'        =>  $login_redirect,
                    'login_loading'         =>  __('Sending user info, please wait...','wpestate'), 
                    'userid'                =>  $userID,
                    'favorite_text'         =>  __('favorite','wpestate'),
                    'add_favorite_text'     =>  __('add to favorites','wpestate'),
                    'saving_text'           =>  __('saving..','wpestate'),
                    'autocity'              =>  json_encode($auto_cities),
                    'used_all'              =>  __('You have used all the "Featured" listings in your package.','wpestate')
                    )
     );
    
    
  
     
     if( is_page_template('user-dashboard-profile.php') || is_page_template('user-dashboard-add.php') ){
   
        wp_enqueue_script('ajax-upload', get_template_directory_uri().'/js/ajax-upload.js',array('jquery','plupload-handlers'), '1.0', true);  
        wp_localize_script('ajax-upload', 'ajax_vars', 
            array(  'ajaxurl'           => admin_url('admin-ajax.php'),
                    'nonce'             => wp_create_nonce('aaiu_upload'),
                    'remove'            => wp_create_nonce('aaiu_remove'),
                    'number'            => 1,
                    'upload_enabled'    => true,
                    'confirmMsg'        => __('Are you sure you want to delete this?','wpestate'),
                    'plupload'          => array(
                                            'runtimes' => 'html5,flash,html4',
                                            'browse_button' => 'aaiu-uploader',
                                            'container' => 'aaiu-upload-container',
                                            'file_data_name' => 'aaiu_upload_file',
                                            'max_file_size' => $max_file_size . 'b',
                                            'url' => admin_url('admin-ajax.php') . '?action=me_upload&nonce=' . wp_create_nonce('aaiu_allow'),
                                            'flash_swf_url' => includes_url('js/plupload/plupload.flash.swf'),
                                            'filters' => array(array('title' => __('Allowed Files','wpestate'), 'extensions' => "jpg,gif,png")),
                                            'multipart' => true,
                                            'urlstream_upload' => true,
                                            )
                
                )
                );
    }
     
     
     
    if ( is_singular() && get_option( 'thread_comments' ) ){
        wp_enqueue_script( 'comment-reply' );
    }
    if( get_post_type() === 'estate_property' && !is_tax() ){
        wp_enqueue_script('property',get_template_directory_uri().'/js/property.js',array('jquery'), '1.0', true); 
    }
   
    

    
    /////////////////////// embed custom fonts from admin
  
    $protocol = is_ssl() ? 'https' : 'http';
    $general_font           =   esc_html ( get_option('wp_estate_general_font', '') );
    $headings_font          =   esc_html ( get_option('wp_estate_headings_font', '') );
    $headings_font_subset   =   esc_html ( get_option('wp_estate_headings_font_subset','') );
    $general_font_subset    =   esc_html ( get_option('wp_estate_general_font_subset','') );
  
    if($headings_font_subset!=''){
        $headings_font_subset='&amp;subset='.$headings_font_subset;
    }
    
    if($general_font_subset!=''){
        $general_font_subset='&amp;subset='.$general_font_subset;
    }
    
    
    if($general_font && $general_font!='x'){
        $general_font =  str_replace(' ', '+', $general_font);
        wp_enqueue_style( 'wpestate-custom-font',"$protocol://fonts.googleapis.com/css?family=$general_font:400,500,300$general_font_subset");  
    }else{
        wp_enqueue_style( 'wpestate-roboto', "$protocol://fonts.googleapis.com/css?family=Roboto:400,500,300&amp;subset=latin,latin-ext" );
    }
   
    if($headings_font && $headings_font!='x'){
       $headings_font =  str_replace(' ', '+', $headings_font);
       wp_enqueue_style( 'wpestate-custom-secondary-font', "$protocol://fonts.googleapis.com/css?family=$headings_font:400,500,300$headings_font_subset" );  
    }
    
    //wp_enqueue_style( 'wpestate-custom-awesome-font', "//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" ); 
    wp_enqueue_style( 'font-awesome.min',  get_template_directory_uri() . '/css/fontawesome/css/font-awesome.min.css' );  
  
    
     
     
     
     
     
    ///////////////////////////// load idx placing javascript 
    if($idx_status=='yes'){
       wp_enqueue_script('idx', get_template_directory_uri().'/js/google_js/idx.js',array('jquery'), '1.0', true); 
    }     
    
    $rental_module_status= esc_html ( get_option('wp_estate_enable_rental_module','') );
    if($rental_module_status=='yes'){
       // wp_enqueue_style('booking_css',get_template_directory_uri().'/css/booking_css.css', array(), '1.0', 'all'); 
        wp_enqueue_script('functions_js', get_template_directory_uri().'/js/functions.js',array('jquery'), '1.0', true);  
        wp_enqueue_script('booking_js', get_template_directory_uri().'/js/booking_js.js',array('jquery'), '1.0', true);  
    
        $wp_estate_book_down=get_option('wp_estate_book_down', '');
        if($wp_estate_book_down==''){
            $wp_estate_book_down=10;
        }
        wp_localize_script('booking_js', 'booking_js_vars', 
            array(  'currency_symbol'                 =>    esc_html( get_option('wp_estate_submission_curency', '') ),
                    'where_currency_symbol'           =>    esc_html( get_option('wp_estate_where_currency_symbol', '') ),
                    'book_down'                       =>    $wp_estate_book_down,
                    'discount'                        =>    __('Discount','wpetstate'),
                    'delete_inv'                      =>   __('Delete Invoice','wpestate'),
                    'issue_inv'                       =>   __('Invoice Issued - waiting for payment','wpestate'),
                    'confirmed'                       =>   __('Confirmed','wpestate'),
                    'issue_inv1'                      =>    __('Issue invoice','wpestate'),
                    'sending'                           =>  __('sending message...','wpestate'),
                    'plsfill'                       =>__('Please fill in all the fields','wpestate'),
                    'datesb'                        =>  __('Dates are already booked. Please check the calendar for free days!','wpestate'),
                    'datepast'                      =>  __('You cannot select a date in the past! ','wpestate'),
                    'bookingstart'                              =>  __('Start date cannot be greater than end date !','wpestate'),
                    'selectprop'                    =>  __('Please select a property !','wpestate'),
                ));
    
    }
    
}



///////////////////////////////////////////////////////////////////////////////////////////
/////// Js & Css include on admin area
///////////////////////////////////////////////////////////////////////////////////////////

add_action('admin_enqueue_scripts', 'wpestate_admin');
function wpestate_admin($hook_suffix) {	
    wp_enqueue_script('media-upload');
    wp_enqueue_script('thickbox');
    wp_enqueue_script('my-upload'); 
    wp_enqueue_style('thickbox');
    wp_enqueue_style('adminstyle', get_template_directory_uri() . '/css/admin.css');
    wp_enqueue_style( 'font-awesome.min',  get_template_directory_uri() . '/css/fontawesome/css/font-awesome.min.css' );  
    if($hook_suffix=='post-new.php' || $hook_suffix=='post.php'){
      wp_enqueue_script("jquery-ui-datepicker");
      wp_enqueue_style('jquery.ui.theme', get_template_directory_uri() . '/css/jquery-ui.min.css');
    }

 
    global $pagenow, $typenow;
    if (empty($typenow) && !empty($_GET['post'])) {
        $post = get_post($_GET['post']);
        $typenow = $post->post_type;
    }
    
      if (is_admin() && $pagenow=='post-new.php' || $pagenow=='post.php' && $typenow=='estate_property') {
            wp_enqueue_script('googlemap', 'http://maps.googleapis.com/maps/api/js?key='.esc_html(get_option('wp_estate_api_key', '') ).'&amp;sensor=true',array('jquery'), '1.0', false);
            wp_enqueue_script('infobox',  get_template_directory_uri() .'/js/infobox.js',array('jquery'), '1.0', false);
            wp_enqueue_script('admin_google', get_template_directory_uri().'/js/google_js/admin_google.js',array('jquery'), '1.0', true); 
   
    
            global $post;            
            $wp_estate_general_latitude  = esc_html(get_post_meta($post->ID, 'property_latitude', true));
            $wp_estate_general_longitude = esc_html(get_post_meta($post->ID, 'property_longitude', true));

            if ($wp_estate_general_latitude=='' || $wp_estate_general_longitude=='' ){
             $wp_estate_general_latitude    = esc_html( get_option('wp_estate_general_latitude','') ) ;
             $wp_estate_general_longitude   = esc_html( get_option('wp_estate_general_longitude','') );


             if($wp_estate_general_latitude==''){
                $wp_estate_general_latitude ='40.781711';
             }

             if($wp_estate_general_longitude==''){ 
                $wp_estate_general_longitude='-73.955927';  
             }
            }



            wp_localize_script('admin_google', 'admin_google_vars', 
             array(  'general_latitude'  =>  $wp_estate_general_latitude,
                     'general_longitude' =>  $wp_estate_general_longitude,
                     'postId'=>$post->ID
                  )
            );
      }
      
    
  
   
   wp_enqueue_script('upload', get_template_directory_uri().'/js/adminupload.js',array('jquery'), '1.0', true); 

    $admin_pages = array('appearance_page_libs/new-theme-admin');
    if(in_array($hook_suffix, $admin_pages)) {
        wp_enqueue_script('admin', get_template_directory_uri().'/js/admin.js',array('jquery'), '1.0', true); 
        wp_enqueue_style('co$hook_suffixlorpicker_css', get_template_directory_uri().'/css/colorpicker.css', false, '1.0', 'all');
        wp_enqueue_script('admin_colorpicker', get_template_directory_uri().'/js/admin_colorpicker.js',array('jquery'), '1.0', true);
        wp_enqueue_script('gumby', get_template_directory_uri().'/js/gumby.min.js',array('jquery'), '1.0', true);  
        wp_enqueue_script('config-property', get_template_directory_uri().'/js/config-property.js',array('jquery'), '1.0', true);          
    }
   
}
 
 ///////////////////////////////////////////////////////////////////////////////////////////
/////// generate custom css
///////////////////////////////////////////////////////////////////////////////////////////

add_action('wp_head', 'generate_options_css');
function generate_options_css() {
    $color_scheme   = esc_html( get_option('wp_estate_color_scheme', '') );
    $general_font   = esc_html( get_option('wp_estate_general_font', '') );
    $headings_font  = esc_html( get_option('wp_estate_headings_font', '') );  
    $custom_css     = stripslashes  ( get_option('wp_estate_custom_css')  );

    if ($general_font != '' || $headings_font != '' || $color_scheme == 'yes' || $custom_css != ''){
       echo "<style type='text/css'>" ;
       if ($general_font != '') {
            require_once ('libs/custom_general_font.php');
       }
       if ($headings_font != '') {
           require_once ('libs/custom_heading_font.php');
       }

       if ($color_scheme == 'yes') {
           require_once ('libs/customcss.php');    
       }
        print $custom_css;
       echo "</style>";  
    }
 
}





///////////////////////////////////////////////////////////////////////////////////////////
///////  Get our wp_nav_menu() fallback, wp_page_menu(), to show a home link.
///////////////////////////////////////////////////////////////////////////////////////////

function wp_estate_page_menu_args($args) {
    if (!isset($args['show_home']))
        $args['show_home'] = true;
    return $args;
}

add_filter('wp_page_menu_args', 'wp_estate_page_menu_args');




///////////////////////////////////////////////////////////////////////////////////////////
///////  Display navigation to next/previous pages when applicable
///////////////////////////////////////////////////////////////////////////////////////////

if (!function_exists('wp_estate_content_nav')) :
 
    function wp_estate_content_nav($html_id) {
        global $wp_query;

        if ($wp_query->max_num_pages > 1) :
            ?>
            <nav id="<?php echo esc_attr($html_id); ?>">
                <h3 class="assistive-text"><?php _e('Post navigation', 'wpestate'); ?></h3>
                <div class="nav-previous"><?php next_posts_link(__('<span class="meta-nav">&larr;</span> Older posts', 'wpestate')); ?></div>
                <div class="nav-next"><?php previous_posts_link(__('Newer posts <span class="meta-nav">&rarr;</span>', 'wpestate')); ?></div>
            </nav><!-- #nav-above -->
        <?php
        endif;
    }

endif; // wpestate_content_nav




///////////////////////////////////////////////////////////////////////////////////////////
///////  Comments
///////////////////////////////////////////////////////////////////////////////////////////



if (!function_exists('wpestate_comment')) :

    function wpestate_comment($comment, $args, $depth) {
        $GLOBALS['comment'] = $comment;
        switch ($comment->comment_type) :
            case 'pingback' :
            case 'trackback' :
                ?>
                <li class="post pingback">
                    <p><?php _e('Pingback:', 'wpestate'); ?> <?php comment_author_link(); ?><?php edit_comment_link(__('Edit', 'wpestate'), '<span class="edit-link">', '</span>'); ?></p>
                <?php
                break;
            default :
                ?>

                <li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
                    <div id="comment-<?php comment_ID(); ?>" class="comment">
                        <div class="comment-meta">
                            <div class="comment-author vcard">
                <?php
                $avatar = wpestate_get_avatar_url(get_avatar($comment, 60));
                print '<div class="blog_author_image singlepage" style="background-image: url(' . $avatar . ');"></div>';
                print '<div class="comment_name">' . get_comment_author_link();
                edit_comment_link(__('Edit', 'wpestate'), '<span class="edit-link">', '</span>');
                print '</div>';
                print '<div class="comment_date">' . get_comment_date() . '</div>';
                ?>

                <?php ?>
                            </div><!-- .comment-author .vcard -->

                <?php if ($comment->comment_approved == '0') : ?>
                                <em class="comment-awaiting-moderation"><?php _e('Your comment is awaiting moderation.', 'wpestate'); ?></em>
                                <br />
                <?php endif; ?>

                        </div>

                        <div class="comment-content"><?php comment_text(); ?></div>

                        <div class="reply">
                <?php comment_reply_link(array_merge($args, array('reply_text' => __('Reply', 'wpestate'), 'depth' => $depth, 'max_depth' => $args['max_depth']))); ?>
                        </div><!-- .reply -->
                    </div><!-- #comment-## -->

                <?php
                break;
        endswitch;
    }


endif; // ends check for  wpestate_comment 


////////////////////////////////////////////////////////////////////////////////
/// Twiter API v1.1 functions
////////////////////////////////////////////////////////////////////////////////

 function getConnectionWithAccessToken($consumer_key, $consumer_secret, $oauth_token, $oauth_token_secret) {
    $connection = new TwitterOAuth($consumer_key, $consumer_secret, $oauth_token, $oauth_token_secret);
    return $connection;
} 

                
//convert links to clickable format
function convert_links($status,$targetBlank=true,$linkMaxLen=250){
    // the target
    $target=$targetBlank ? " target=\"_blank\" " : "";

    // convert link to url
    $status = preg_replace("/((http:\/\/|https:\/\/)[^ )]+)/e", "'<a href=\"$1\" title=\"$1\" $target >'. ((strlen('$1')>=$linkMaxLen ? substr('$1',0,$linkMaxLen).'...':'$1')).'</a>'", $status);

    // convert @ to follow
    $status = preg_replace("/(@([_a-z0-9\-]+))/i","<a href=\"http://twitter.com/$2\" title=\"Follow $2\" $target >$1</a>",$status);

    // convert # to search
    $status = preg_replace("/(#([_a-z0-9\-]+))/i","<a href=\"https://twitter.com/search?q=$2\" title=\"Search $1\" $target >$1</a>",$status);

    // return the status
    return $status;
}

                

//convert dates to readable format	
function relative_time($a) {
        //get current timestampt
        $b = strtotime("now"); 
        //get timestamp when tweet created
        $c = strtotime($a);
        //get difference
        $d = $b - $c;
        //calculate different time values
        $minute = 60;
        $hour = $minute * 60;
        $day = $hour * 24;
        $week = $day * 7;

        if(is_numeric($d) && $d > 0) {
                //if less then 3 seconds
                if($d < 3) return "right now";
                //if less then minute
                if($d < $minute) return floor($d) . " seconds ago";
                //if less then 2 minutes
                if($d < $minute * 2) return "about 1 minute ago";
                //if less then hour
                if($d < $hour) return floor($d / $minute) . " minutes ago";
                //if less then 2 hours
                if($d < $hour * 2) return "about 1 hour ago";
                //if less then day
                if($d < $day) return floor($d / $hour) . " hours ago";
                //if more then day, but less then 2 days
                if($d > $day && $d < $day * 2) return "yesterday";
                //if less then year
                if($d < $day * 365) return floor($d / $day) . " days ago";
                //else return more than a year
                return "over a year ago";
        }
    }

 



////////////////////////////////////////////////////////////////////////////////
/// Front end submission
////////////////////////////////////////////////////////////////////////////////
if ( !function_exists('top_bar_front_end_menu')):
function top_bar_front_end_menu(){

    $top_bar_status= esc_html ( get_option('wp_estate_enable_top_bar','') );
    if($top_bar_status=='yes'){
        $current_user = wp_get_current_user();
        $username=$current_user->user_login ;$pages = get_pages(array(
            'meta_key' => '_wp_page_template',
            'meta_value' => 'user-dashboard-add.php'
        ));

        if( $pages ){
            $add_link = get_permalink( $pages[0]->ID);
        }else{
            $add_link='';
        }
    
        $pages = get_pages(array(
            'meta_key' => '_wp_page_template',
            'meta_value' => 'user-dashboard-profile.php'
                ));

        if( $pages ){
            $dash_profile = get_permalink( $pages[0]->ID);
        }else{
            $dash_profile=home_url();
        }   
          
        $dash_link =   get_dashboard_link();
        print'<div class="top-user-menu-wrapper">
            <div class="top-user-menu">
                <div class="left-top-widet">
                    <ul class="xoxo">';
                     dynamic_sidebar('top-bar-left-widget-area');
                    print '</ul>    
                </div>  



                <div class="wellcome-user">';
        
                    $show_user_not_logged_in='';
                    $show_user_logged_in='';
					global $current_user;
						$user_roles = $current_user->roles[0];
                    if ( is_user_logged_in() ) { 
                        $show_user_not_logged_in='geohide' ; 
                    }else{
                         $show_user_logged_in='geohide';
                    }
                    
                     $paid_submission_status    = esc_html ( get_option('wp_estate_paid_submission','') ); 

                     print '
                        <div id="user_logged_in" class="'.$show_user_logged_in.'">';    
if($dash_link!='' && $user_roles=='contributor' || $dash_link!='' && $user_roles=='subscriber' || $user_roles=='administrator'){
                            print ' <a href="'.$dash_link.'">'.__('My Properties','wpestate').'</a> | ';
                        }else{
                            print '<a href="'.$dash_profile.'">' .__('My Profile','wpestate').'</a> | ';
                        }
                        
                        if($add_link!='' && $user_roles=='contributor' || $user_roles=='administrator' || $user_roles=='subscriber'){
                            print '<a href="'.$add_link.'">' .__('List a Property','wpestate').'</a> | ';
                        } 
                        
                        print'
                        <a href="'.wp_logout_url().'">'.__('Log Out','wpestate').'</a>
                        </div>';  
                     
                                            
                  
                        $front_end_register =   esc_html( get_option('wp_estate_front_end_register','') );
                        $front_end_login    =   esc_html( get_option('wp_estate_front_end_login ','') );
                        print '
                        <div id="user_not_logged_in" class="'.$show_user_not_logged_in.'">  
                        <a href="'.$front_end_login.'">'.__('Login','wpestate').'</a> |    
                        <a href="'.$front_end_register.'">'.__('Register','wpestate').'</a> 
                        </div>';
                        
                   
                    print'
                </div>
            </div>
        </div>';
    
    } 
    
}
endif;
////////////////////////////////////////////////////////////////////////////////
/// Ajax adv search contact function
////////////////////////////////////////////////////////////////////////////////
   add_action( 'wp_ajax_nopriv_ajax_contact_form', 'ajax_contact_form' );  
   add_action( 'wp_ajax_ajax_contact_form', 'ajax_contact_form' );  
   
function ajax_contact_form(){
    
        // check for POST vars
        $hasError = false;
        $to_print='';
        if ( !wp_verify_nonce( $_POST['nonce'], 'ajax-contact')) {
            exit(__('No naughty business please','wpestate'));
        }   

        if (trim($_POST['name']) == '') {
            $hasError = true;
            $error[] = __('The name field is empty !','wpestate');
        } else {
            $name = esc_html( trim($_POST['name']) );
        }

        //Check email
        if (trim($_POST['email']) == '') {
            $hasError = true;
            $error[] = __('The email field is empty','wpestate');
        } else if(filter_var($_POST['email'],FILTER_VALIDATE_EMAIL) === false) {
            $hasError = true;
            $error[] = __('The email doesn\'t look right !','wpestate');
        } else {
            $email = esc_html( trim($_POST['email']) );
        }

        $phone = esc_html( trim($_POST['phone']) );
        $subject =__('Contact form from ','wpestate') . home_url() ;

        //Check comments 
        if (trim($_POST['comment']) == '') {
            $hasError = true;
            $error[] = __('Your message is empty !','wpestate');
        } else {
            $comment = esc_html( trim ($_POST['comment'] ) );
        }

         $message='';
            $receiver_email = is_email ( get_bloginfo('admin_email') );
         if (!$hasError) {
            $message .= __("Client Name: " ,"wpestate"). $name . "\n\n ".__("Email:","wpestate")." ". $email . " \n\n ".__('Phone','wpestate').": " . $phone . " \n\n ".__('Subject','wpestate').":" . $subject . " \n\n".__('Message','wpestate').":\n " . $comment;
            $email_headers = "From: " . $email . " \r\n Reply-To:" . $email;

            $mail = wp_mail($receiver_email, $subject, $message, $email_headers);
            $succes = '<span>'.__('The message was sent !','wpestate').'</span>';
                   
            print $succes;
          
        }else{
             foreach ($error as $mes) {
                $to_print.=$mes . '<br />';
             }
             print $to_print;
        }
        die(); 
        
        
}



////////////////////////////////////////////////////////////////////////////////
/// Ajax adv search contact function
////////////////////////////////////////////////////////////////////////////////
   add_action( 'wp_ajax_nopriv_ajax_agent_contact_form', 'ajax_agent_contact_form' );  
   add_action( 'wp_ajax_ajax_agent_contact_form', 'ajax_agent_contact_form' );  
   
   
function ajax_agent_contact_form(){
    
        // check for POST vars
        $hasError = false;
        $to_print='';
        if ( !wp_verify_nonce( $_POST['nonce'], 'ajax-property-contact')) {
            exit(__("No naughty business please" ,"wpestate"));
        }   
       
        
        if ( isset($_POST['name']) ) {
           if( trim($_POST['name']) =='' || trim($_POST['name']) ==__('Your Name','wpestate') ){
               echo json_encode(array('sent'=>false, 'response'=>__('The name field is empty !','wpestate') ));         
               exit(); 
           }else {
               $name = esc_html( trim($_POST['name']) );
           }          
        } 

        //Check email
        if ( isset($_POST['email']) || trim($_POST['name']) ==__('Your Email','wpestate') ) {
              if( trim($_POST['email']) ==''){
                    echo json_encode(array('sent'=>false, 'response'=>__('The email field is empty','wpestate' ) ) );      
                    exit(); 
              } else if( filter_var($_POST['email'],FILTER_VALIDATE_EMAIL) === false) {
                    echo json_encode(array('sent'=>false, 'response'=>__('The email doesn\'t look right !','wpestate') ) ); 
                    exit();
              } else {
                    $email = esc_html( trim($_POST['email']) );
              }
        }

        
        
        $phone = esc_html( trim($_POST['phone']) );
        $subject =__('Contact form from ','wpestate') . home_url() ;

        //Check comments 
        if ( isset($_POST['comment']) ) {
              if( trim($_POST['comment']) =='' || trim($_POST['comment']) ==__('Your Message','wpestate')){
                echo json_encode(array('sent'=>false, 'response'=>__('Your message is empty !','wpestate') ) ); 
                exit();
              }else {
                $comment = esc_html( trim ($_POST['comment'] ) );
              }
        } 

        $message='';
        
        if(isset($_POST['agentemail'] )){
            if( is_email ( $_POST['agentemail'] ) ){
                $receiver_email = $_POST['agentemail'] ;
            }
        }
       
        
        $propid=intval($_POST['propid']);
        if($propid!=0){
            $permalink = get_permalink(  $propid );
        }else{
            $permalink = 'contact page';
        }
        
        $message .= __('Client Name','wpestate').": " . $name . "\n\n ".__('Email','wpestate').": " . $email . " \n\n ".__('Phone','wpestate').": " . $phone . " \n\n ".__('Subject','wpestate').":" . $subject . " \n\n".__('Message','wpestate').":\n " . $comment;
        $message .="\n\n ".__('Message sent from ','wpestate').$permalink;
        $email_headers = "From: " . $email . " \r\n Reply-To:" . $email;
        $headers = 'From:   <noreply@'.$_SERVER['HTTP_HOST'].'>' . "\r\n".
                        'Reply-To:'.$email . "\r\n" .
                        'X-Mailer: PHP/' . phpversion();
        
        $mail = wp_mail($receiver_email, $subject, $message, $headers);
   
        echo json_encode(array('sent'=>true, 'response'=>__('The message was sent !','wpestate') ) ); 

        
      
        die(); 
        
        
}

////////////////////////////////////////////////////////////////////////////////
/// Add new profile fields
////////////////////////////////////////////////////////////////////////////////
     
function modify_contact_methods($profile_fields) {

	// Add new fields
	$profile_fields['phone']                        = __('Phone','wpestate');
	$profile_fields['mobile']                       = __('Mobile','wpestate');
        $profile_fields['skype']                        = __('Skype','wpestate');
		$profile_fields['state']                        = __('State','wpestate');
	$profile_fields['title']                        = __('Title/Position','wpestate');
        $profile_fields['custom_picture']               = __('Picture Url','wpestate');
        $profile_fields['package_id']                   = __('Package Id','wpestate');
        $profile_fields['package_activation']           = __('Package Activation','wpestate');
        $profile_fields['package_listings']             = __('Listings available','wpestate');
        $profile_fields['package_featured_listings']    = __('Featured Listings available','wpestate');
        $profile_fields['profile_id']                   = __('Paypal Recuring Profile','wpestate');
        $profile_fields['user_agent_id']                = __('User Agent Id','wpestate');
	return $profile_fields;
}
add_filter('user_contactmethods', 'modify_contact_methods');



if( !current_user_can('activate_plugins') ) {
    function wpestate_admin_bar_render() {
        global $wp_admin_bar;
        $wp_admin_bar->remove_menu('edit-profile', 'user-actions');
       
}
add_action( 'wp_before_admin_bar_render', 'wpestate_admin_bar_render' );

function stop_access_profile() {
     global $pagenow;
  //   print $pagenow;
   
    if( defined('IS_PROFILE_PAGE') && IS_PROFILE_PAGE === true ) {
        wp_die( __('Please edit your profile page from site interface.','wpestate') );
    }
   // if these are uncomented it generates error on ajax profile update
   // remove_menu_page( 'profile.php' );
   //remove_submenu_page( 'users.php', 'profile.php','user-edit.php' );
    
    if($pagenow=='user-edit.php'){
         wp_die( __('Please edit your profile page from site interface.','wpestate') );
    } 
}
add_action( 'admin_init', 'stop_access_profile' );
}



///////////////////////////////////////////////////////////////////////////////////////////
// prevent changing the author id when admin hit publish
///////////////////////////////////////////////////////////////////////////////////////////


function correct_post_data( $strNewStatus,$strOldStatus,$post) {
    /* Only pay attention to posts (i.e. ignore links, attachments, etc. ) */
    if( $post->post_type !== 'estate_property' )
        return;

    if( $strOldStatus === 'new' ) {
        update_post_meta( $post->ID, 'original_author', $post->post_author );
    }

    
    
    
    
    /* If this post is being published, try to restore the original author */
    if( $strNewStatus === 'publish' && $strOldStatus!='publish') {
    
         // print_r($post);         
         //$originalAuthor = get_post_meta( $post->ID, 'original_author' );
          
            $originalAuthor_id =$post->post_author;
            $user = get_user_by('id',$originalAuthor_id); 
            $user_email=$user->user_email;
            
            if( $user->roles[0]=='subscriber'){
                $headers = 'From: No Reply <noreply@'.$_SERVER['HTTP_HOST'].'>' . "\r\n";
                $message  = __('Hi there,','wpestate') . "\r\n\r\n";
                $message .= sprintf( __("Your listing was approved on  %s! You should go check it out.","wpestate"), get_option('blogname')) . "\r\n\r\n";
                wp_mail($user_email,
		    sprintf(__('[%s] Your listing was approved','wpestate'), get_option('blogname')),
                    $message,
                    $headers);
               
            }
            
 
             
     
    }
}
add_action( 'transition_post_status', 'correct_post_data',10,3 );





///////////////////////////////////////////////////////////////////////////////////////////
// register google user
///////////////////////////////////////////////////////////////////////////////////////////
function register_user_via_google($email,$full_name,$openid_identity_code){


   if ( email_exists( $email ) ){ 
   
           if(username_exists($full_name) ){
               return;
           }else{
               $user_id  = wp_create_user( $full_name, $openid_identity_code,' ' );  
               wpestate_update_profile($user_id); 
               if('yes' ==  esc_html ( get_option('wp_estate_user_agent','') )){
                    wpestate_register_as_user($full_name,$user_id);
               }
           }
          
    }else{
      
          if(username_exists($full_name) ){
                return;
           }else{
                $user_id  = wp_create_user( $full_name, $openid_identity_code, $email ); 
                wpestate_update_profile($user_id);
                if('yes' ==  esc_html ( get_option('wp_estate_user_agent','') )){
                         wpestate_register_as_user($full_name,$user_id);
                }
           }
        
        
           
    }
   
}


function get_user_dashboard_url(){
     $pages = get_pages(array(
        'meta_key' => '_wp_page_template',
        'meta_value' => 'user-dashboard-profile.php'
            ));

    if( $pages ){
        $dash_profile = get_permalink( $pages[0]->ID);
    }else{
        $dash_profile=home_url();
    }
    return $dash_profile;
}


function get_domain_openid(){
    $realm_url = get_home_url();
    $realm_url= str_replace('http://','',$realm_url);
    $realm_url= str_replace('https://','',$realm_url);  
    return $realm_url;
}



///////////////////////////////////////////////////////////////////////////////////////////
// paypal functions
///////////////////////////////////////////////////////////////////////////////////////////

function get_access_token($url, $postdata) {
	$clientId                       =   esc_html( get_option('wp_estate_paypal_client_id','') );
        $clientSecret                   =   esc_html( get_option('wp_estate_paypal_client_secret','') );
       
	$curl = curl_init($url); 
	curl_setopt($curl, CURLOPT_POST, true); 
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($curl, CURLOPT_USERPWD, $clientId . ":" . $clientSecret);
	curl_setopt($curl, CURLOPT_HEADER, false); 
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); 
	curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata); 
#	curl_setopt($curl, CURLOPT_VERBOSE, TRUE);
	$response = curl_exec( $curl );
	if (empty($response)) {
	    // some kind of an error happened
	    die(curl_error($curl));
	    curl_close($curl); // close cURL handler
	} else {
	    $info = curl_getinfo($curl);
		//echo "Time took: " . $info['total_time']*1000 . "ms\n";
	    curl_close($curl); // close cURL handler
		if($info['http_code'] != 200 && $info['http_code'] != 201 ) {
			echo "Received error: " . $info['http_code']. "\n";
			echo "Raw response:".$response."\n";
			die();
	    }
	}

	// Convert the result from JSON format to a PHP array 
	$jsonResponse = json_decode( $response );
	return $jsonResponse->access_token;
}

function make_post_call($url, $postdata,$token) {
	//global $token;
	$curl = curl_init($url); 
	curl_setopt($curl, CURLOPT_POST, true);
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($curl, CURLOPT_HEADER, false);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_HTTPHEADER, array(
				'Authorization: Bearer '.$token,
				'Accept: application/json',
				'Content-Type: application/json'
				));
	
	curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata); 
	#curl_setopt($curl, CURLOPT_VERBOSE, TRUE);
	$response = curl_exec( $curl );
	if (empty($response)) {
	    // some kind of an error happened
	    die(curl_error($curl));
	    curl_close($curl); // close cURL handler
	} else {
	    $info = curl_getinfo($curl);
		//echo "Time took: " . $info['total_time']*1000 . "ms\n";
	    curl_close($curl); // close cURL handler
		if($info['http_code'] != 200 && $info['http_code'] != 201 ) {
			//echo "Received error !xx: " . $info['http_code']. "\n";
			//echo "Raw response:".$response."\n";
                        $dash_profile_link = get_dashboard_profile_link();
                        wp_redirect( $dash_profile_link ); 
			die();
	    }
	}

	// Convert the result from JSON format to a PHP array 
	$jsonResponse = json_decode($response, TRUE);
	return $jsonResponse;
}

///////////////////////////////////////////////////////////////////////////////////////////
// dasboaord link
///////////////////////////////////////////////////////////////////////////////////////////
function get_dashboard_link(){
    $pages = get_pages(array(
            'meta_key' => '_wp_page_template',
            'meta_value' => 'user-dashboard.php'
        ));

    if( $pages ){
        $dash_link = get_permalink( $pages[0]->ID);
    }else{
        $dash_link='';
    }  
    
    return $dash_link;
}
///////////////////////////////////////////////////////////////////////////////////////////
// procesor link
///////////////////////////////////////////////////////////////////////////////////////////

function get_procesor_link(){
    $pages = get_pages(array(
        'meta_key' => '_wp_page_template',
        'meta_value' => 'processor.php'
            ));

    if( $pages ){
        $processor_link = get_permalink( $pages[0]->ID);
    }else{
        $processor_link=home_url();
    }
    
    return $processor_link;
}

///////////////////////////////////////////////////////////////////////////////////////////
// dasboaord profile link
///////////////////////////////////////////////////////////////////////////////////////////
function get_dashboard_profile_link(){
    $pages = get_pages(array(
            'meta_key' => '_wp_page_template',
            'meta_value' => 'user-dashboard-profile.php'
        ));
    
    if( $pages ){
        $dash_link = get_permalink( $pages[0]->ID);
    }else{
        $dash_link=home_url();
    }  
    
    return $dash_link;
}

    
///////////////////////////////////////////////////////////////////////////////////////////
// set 'cron job' - wp_schedule_event seems like a unrecomend option since wp-cron.php is 
// blocked by a lot of hosting providers
///////////////////////////////////////////////////////////////////////////////////////////

if ( get_option('wp_estate_paid_submission','')=='membership' ){
 
    $last_time=get_option('wp_estate_cron_run','');
    $now = time();
    $interval=86400;
    //$interval=60*5; // every 5 min
   
    $last_time=$last_time+$interval;   
    $remain=$now-$last_time;
   
      
    if( ($now > $last_time ) || $last_time==''){
       update_option('wp_estate_cron_run',time());
       check_user_membership_status_function();
    }
} 




/////////////////////////////////////////////////////////////////////////////////
// order by filter featured
///////////////////////////////////////////////////////////////////////////////////
 function my_order($orderby) { 
    global $wpdb; 
    global $table_prefix;
    $orderby = $table_prefix.'postmeta.meta_value DESC, '.$table_prefix.'posts.ID DESC';
    return $orderby;
} 




?>