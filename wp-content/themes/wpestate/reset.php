<?php
/*  Template Name: Reset Page 
*/ 
  get_header();
   
 global $user_ID;
if ($user_ID!='') {
	echo "<script type='text/javascript'>window.location='". get_bloginfo('url') ."'</script>";
}
   $options        =   sidebar_orientation($post->ID);
$custom_image   =   esc_html( esc_html(get_post_meta($post->ID, 'page_custom_image', true)) );   


// get page border if any
$border_option = esc_html( get_post_meta($post->ID, 'border_option', true) );
$border='';

switch ($border_option){
    case 'agent border':
        $border='agentborder';
        break;
    case 'listing border':
        $border='listingborder ';
        break;
    case 'blog border':
       $border='blogborder';
        break;
    case 'white border':
       $border='none';
        break;
    case 'no border':
       $border='';
        break;
     case '':
        $border='none';
        break;
}

?>


<!-- Google Map Code -->
<?php 
//get_template_part('libs/templates/map-template'); 
?> 
   <div class="wrapper"> 
    <div id="main" class="row no sidebar">
    <?php
    //print display_breadcrumbs( $options['full_breadcrumbs'] ,$options['bread_align_internal'] )
    ?>

        
        <!-- begin content--> 
        <div id="post" class="is_page twelve">
   <?php 		global $wpdb; 		
   $error = ''; 	
   $success = ''; 	
   // check if we're in reset form
   if( isset( $_POST['action'] ) && 'reset' == $_POST['action'] )  		{ 
   $email = trim($_POST['user_login']); 			
   if( empty( $email ) ) {
	   $error = 'Enter e-mail address..';
	   } else if( ! is_email( $email )) {
		   $error = 'Invalid username or e-mail address.';
		   } 
		   else if( ! email_exists( $email ) ) {
			   $error = 'There is no user registered with that email address.'; 
			   } else {
				   $random_password = wp_generate_password( 12, false );
				   $user = get_user_by( 'email', $email ); 
				   $update_user = wp_update_user( array (
				   'ID' => $user->ID, 
				   'user_pass' => $random_password 	
				   ) 			
				   ); 
				   // if  update user return true then lets send user an email containing the new password 
				   if( $update_user ) {
						require 'sendgrid-php/vendor/autoload.php';
						require 'sendgrid-php/lib/SendGrid.php';
					   
					   $sendgrid = new SendGrid('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');
						$email1 = new SendGrid\Email();
						$message = 'Your new password is: '.$random_password;
						 $sender = get_option('name');
						 $subject = 'Your new password'; 
					   /*$to = $email; 
					   $subject = 'Your new password'; 
					   $sender = get_option('name'); 
					   $message = 'Your new password is: '.$random_password;
					   $headers[] = 'MIME-Version: 1.0' . "\r\n";
					   $headers[] = 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
					   $headers[] = "X-Mailer: PHP \r\n"; 	
					   $headers[] = 'From: '.$sender.' < '.$email.'>' . "\r\n"; 
					   $mail = wp_mail( $to, $subject, $message, $headers ); 			
					   */
						$email1
						->addTo($email)
						->setFrom('anil.thakur@sanganan.in')
						->setSubject($subject)
						->setText('your new password')
						->setHtml($message)
						;
						try {
	$sendgrid->send($email1);
	$success = 'Check your email address for you new password.'; 	
} catch(\SendGrid\Exception $e) {
	echo $e->getCode();
	$error = 'Oops something went wrong updaing your account.'; 		
	foreach($e->getErrors() as $er) {
		echo $er;
	}
	
}	
					  /* if( $mail ) 	
						   $success = 'Check your email address for you new password.'; 	
					   } else { 	
					   $error = 'Oops something went wrong updaing your account.'; 		
					   }*/ 	
					   }} 
					   if( ! empty( $error ) )
						   echo '<div class="message"><p class="error"><strong></strong> '. $error .'</p></div>';
					   if( ! empty( $success ) )
						   echo '<div class="error_login"><p class="success">'. $success .'</p></div>'; 		} 	?> 
					   <!--html code--> 
					   <form method="post" style="padding-bottom:270px;"> 
                         <h3 style="border-bottom:1px #1791D2 solid;margin-bottom:40px;color:#1791D2;">Forgot Password</h3>					   
					   <fieldset> 			<p>Please enter your email address. You will receive a link to create a new password via email.</p> 
					   <p><label for="user_login">Username or E-mail:</label> 				
					   <?php $user_login = isset( $_POST['user_login'] ) ? $_POST['user_login'] : ''; ?><br/>
					   <input type="text" name="user_login" id="user_login" value="<?php echo $user_login; ?>" style="width:400px;border:1px #ccc solid;margin-top:10px;" />
					   </p> 
					   <p> 			
					   <input type="hidden" name="action" value="reset" />
					  <input type="submit" value="Get New Password" class="button" id="submit" style="width:414px;height:40px;margin-top:10px;background:#005375;color:#fff;border:none;"/>
					   </p> 		</fieldset>
					   </form>
					   </div> 
					   </div>
					   </div>
					   
<style>
.error{color:red !important;}
</style>
					   <?php get_footer() ?>