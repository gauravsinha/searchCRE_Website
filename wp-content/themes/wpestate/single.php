<?php
// Sigle - Blog post
// Wp Estate Pack
get_header();
$options        =   sidebar_orientation($post->ID); 
 

 if ( 'wpestate_message' == get_post_type() || 'wpestate_invoice' == get_post_type() || 'wpestate_booking' == get_post_type() ){
     exit();
 }

?>


<!-- Google Map Code -->
<?php 
get_template_part('libs/templates/map-template'); 
?> 
<!-- Google Map Code -->


<div id="wrapper" class="<?php print $options['fullwhite']; ?>">  
    <div class="<?php print $options['add_back']; ?>"></div>

    <?php
    print breadcrumb_container($options['full_breadcrumbs'],$options['bread_align'] )
    ?>
    <div id="main" class="row <?php print $options['sidebar_status']; ?>">
    <?php
    print display_breadcrumbs( $options['full_breadcrumbs'] ,$options['bread_align_internal'] )
    ?>

    <!-- begin content --> 
    <?php
    $classes = array(
                $options['grid'],
                $options['shadow'],
                'singlepost',
                'blogborder'
                );
    ?>
    <div id="post" <?php post_class($classes);?> > 
        <div class="inside_post  inside_no_border" >

                <?php 
                while ( have_posts() ) : the_post();
                if (esc_html( get_post_meta($post->ID, 'post_show_title', true) ) != 'no') { ?> 
                    <h1 class="entry-title" ><?php the_title(); ?></h1>
                <?php }  ?>

  

          
                <?php
                get_template_part('postslider');   
                ?>
                <div class="meta-info">
                    <?php 
                    $avatar = wpestate_get_avatar_url(get_avatar(get_the_author_meta('email') , 60)); ?>
                    <div class="blog_author_image singlepage" style="background-image: url('<?php print $avatar; ?>');"></div>
                    <div class="blog_author_info"> <?php _e('by ', 'wpestate');
                        print get_the_author(); ?><br>
                        <?php the_date(); ?>
                    </div>
                    <div class="blog_category">
                        <span class="blog_category_mobile"><strong><?php _e('Category: ','wpestate');?> </strong>&nbsp;</span>
                        <?php echo ' ';the_category(', '); ?>
                    </div>

                    <div class="blog_social">
                        <a href="http://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>&amp;t=<?php print urlencode(get_the_title()); ?>" class="social_facebook" target="_blank"></a>
                        <a href="http://twitter.com/home?status=<?php print urlencode(get_the_title().': ');?><?php the_permalink(); ?>" class="social_tweet" target="_blank"></a>
                        <a href="http://google.com/bookmarks/mark?op=edit&amp;bkmk=<?php the_permalink(); ?>&amp;title=<?php print urlencode(get_the_title()); ?>" target="_blank" class="social_google"></a>
                    </div>
                </div> 


                <div class="single-content">
                    <?php 
                     global $more;
                     $more=0;
                     the_content('Continue Reading');                     
                     $args = array(
                                'before'           => '<p>' . __('Pages:','wpestate'),
                                'after'            => '</p>',
                                'link_before'      => '',
                                'link_after'       => '',
                                'next_or_number'   => 'number',
                                'nextpagelink'     => __('Next page','wpestate'),
                                'previouspagelink' => __('Previous page','wpestate'),
                                'pagelink'         => '%',
                                'echo'             => 1
                       ); 
                   wp_link_pages( $args ); ?>                   
                    <div class="navigational_links">
                        <div class="nav-prev"><?php previous_post_link('%link', '&laquo; %title', TRUE); ?></div>
                        <div class="nav-next"><?php next_post_link('%link', '%title &raquo;', TRUE); ?></div>
                    </div> 
                </div>
                
                <hr class="dottedlineblog blog_bottom_border">
                <?php include(locate_template('related-posts.php'));?>
                <!-- #comments start-->
                <?php comments_template('', true);?> 	
                <!-- end comments -->

	<?php endwhile; // end of the loop. ?>

            </div><!-- end inside post-->
        </div>
        <!-- end content-->



       <?php  include(locate_template('customsidebar.php')); ?>

    </div><!-- #main -->    
</div><!-- #wrapper -->
<?php get_footer(); ?>